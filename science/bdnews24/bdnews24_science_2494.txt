অ্যাপল ৭ এর হালনাগাদের তথ্য জানানো ওই ব্যক্তির নাম মার্ক গারম্যান। তিনি ব্লুমবার্গের একজন প্রতিবেদক।
গারম্যান প্রতিবেদনে বলেছেন, আইফোনের পরবর্তী উল্লেখযোগ্য সংস্করণ হল 'প্লাস' মডেল। এই মডেলের বৈশিষ্ট্য হল এতে ডুয়াল সেন্সর ক্যামেরা থাকবে। এর বিশেষত্ব হল, এই ক্যামেরার মাধ্যমে স্বাধীনভাবে ক্যাপচার করা দুইটি ছবির মিলিয়ে উচ্চমানের ছবি পাওয়া যাবে। বিশেষ করে কম আলোয় এই ক্যামেরা বেশি কার্যকর হবে।

এক ব্লগ পোস্টে মার্কিন প্রযুক্তি জায়ান্ট প্রতিষ্ঠানটি জানায়, “আইওএস ১১.৩-তে বাড়তি ফিচার হিসেবে অ্যাডভান্সড মোবাইল লোকেশন আনা হচ্ছে, এর মাধ্যমে গ্রাহক যখন জরুরী সেবায় কল করবেন তখন তার বর্তমান অবস্থান পাঠানো হবে, যেসকল দেশে এএমএল সমর্থন করে।”
ভারতীয় সংবাদমাধ্যম আইএএনএস-এর প্রতিবেদনে বলা হয়, এই ফিচারের মাধ্যমে গ্রাহক যখন জরুরী নাম্বারে কল করবেন তখন জিপিএস ও ওয়াই-ফাই সেবা চালু হয় এবং উত্তরদাতাকে গ্রাহকের নিখুঁত অবস্থান জানিয়ে একটি বার্তা পাঠানো হবে।
বর্তমানে মার্কিন যুক্তরাষ্ট্রে এএমএল সমর্থন নেই বলে প্রতিবেদনে উল্লেখ করা হয়েছে। তবে, যুক্তরাজ্য, নিউ জিল্যান্ড, সুইডেন, লিথুয়ানিয়া এবং লোয়ার অস্ট্রিয়ার কিছু অংশে আইওএস গ্রাহকরা এই সুবিধা নিতে পারবেন।
ইতোমধ্যেই অ্যান্ড্রয়েড জিঞ্জারব্রেড এবং এর পরবর্তী সংস্করণে এএমএল সমর্থন রয়েছে বলে প্রতিবেদনে জানানো হয়।
এএমএল-এর পাশাপাশি নতুন আইওএস ১১.৩ সংস্করণে ব্যাটারি নিয়ন্ত্রণ ক্ষমতা, অগমেন্টেড রিয়ালিটি এবং অ্যানিমোজি আপগ্রেড আনা হবে।

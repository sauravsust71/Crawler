‘ইয়ার ইন রিভিউ’ নামে বছরজুড়ে ব্যবহারকারীদের ‘গুরুত্বপূর্ণ’ মূহুর্তগুলো নিয়ে বানানো একটি স্লাইড শো চালু করেছে ফেইসবুক। এর পরই এমন অভিযোগ আসা শুরু করে বলে মার্কিন দৈনিক বস্টন গ্লোব-এ শুক্রবারের এক প্রতিবেদনের বরাতে জানিয়েছে আইএএনএস ।
পিসি ম্যাগাজিন-এর প্রতিবেদক স্টিফেন মাইলট-এর উদ্ধৃতি দিয়ে ওই প্রতিবেদনে বলা হয়, শুক্রবার তার চারটি পুরানো ছবি নতুন করে পোস্ট করা হয়।
এই ছবিগুলো ব্যবহারকারীদের টাইমলাইনে নতুন করে পোস্ট না হলেও, হোম পেইজে নতুন তারিখ আর সময় দিয়ে পোস্ট হয়। এটি দেখে মনে হয় ব্যবহারকারী নিজেই ছবিগুলো পোস্ট করছেন, যা ব্যবহারকারীদের বিভ্রান্তির মধ্যে ফেলে দেয়।  
এ নিয়ে ফেইসবুক জানিয়েছে, তারা এই সমস্যা নিয়ে জানে ও বর্তমানে এ বিষয়ে তদন্ত চলছে।
এ বিষয় নিয়ে ফেইসবুকের ‘হেল্প ফোরাম’-এ অনেক প্রশ্ন জমা হতে থাকে। অনেক ব্যবহারকারী এই ঘটনায় ভেবেছিলেন তাদের অ্যাকাউন্ট হ্যাক করা হয়েছে।
এক ব্যবহারকারী তার অভিযোগে বলেন, “আজ রাতে আমার পেইজে নতুন পোস্ট হিসাবে আমি দুটি পোস্ট দেখেছি, কিন্তু এগুলো ছয় মাসের আগের ও পুরানো। এটি কেন হচ্ছে? আমি কি হ্যাকড?”

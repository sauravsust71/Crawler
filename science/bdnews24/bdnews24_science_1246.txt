বৃহস্পতিবার ইয়াহু জানায়, ২০১৪ সালে হ্যাকাররা ৫০ কোটিরও বেশি গ্রাহকের এনক্রিপটেড পাসওয়ার্ড আর ব্যক্তিগত তথ্য হাতিয়ে নিয়েছে। ওই ঘটনার খবর জানাতে ইয়াহু দুই বছর অপেক্ষা করায় হাজার হাজার ব্যবহারকারী সামাজিক মাধ্যমে তাদের ক্ষোভ প্রকাশ করেছেন বলে জানিয়েছে রয়টার্স।
অনেক ব্যবহারকারী তাদের অ্যাকাউন্ট বন্ধ করে দেওয়ার কথা জানিয়েছেন।
৫৬ বছর বয়সী রিক হলিস্টার ফ্লোরিডার একটি প্রাইভেট তদন্তকারী প্রতিষ্ঠানে কাজ করেন। তিনি বলেন, "আমরা হয়তো সব মিলিয়ে ইয়াহুকে ফেলে দিতে যাচ্ছি।"
ইয়াহু লঙ্ঘনের মাত্রা, আর ব্যবহারকারীরা প্রায়ই তাদের পাসওয়ার্ড পরিবর্তন করায় ও অনেক সার্ভিসের মাধ্যমে নিরাপত্তা প্রশ্নের উত্তরের ব্যবস্থা থাকায়, ইন্টারনেটে এই হ্যাকের প্রভাব প্রতিহত করা সম্ভব বলে জানিয়েছেন সাইবার নিরাপত্তা বিশেষজ্ঞরা।
অনেক ব্যবহারকারী তাদের লগইন তথ্য বদলাতে উঠেপরে লেগেছেন। এক্ষেত্রে শুধু ইয়াহু-ই মাথাব্যথা নয়, অনেকেই একাধিক ইন্টারনেট অ্যাকাউণ্টে একই পাসওয়ার্ড ব্যবহার করে থাকেন। এর ফলে অনেকেরই ব্যাংক অ্যাকাউন্টের মতো গুরুত্বপূর্ণ অ্যাকাউন্ট লঙ্ঘন হতে পারে।
৪৭ বছর বয়সী স্কট ব্রান বলেন, "আমি মনে করেছিলাম, কোনো হ্যাকার আমার ইয়াহু আর জিমেইল অ্যাকাউন্ট যুক্ত করে দিয়েছে। তারা দুই ক্ষেত্রেই আমার প্রথম ও শেষ নাম ব্যবহার করে। একজন হ্যাকার না হওয়ায়, আমি জানি না তাদের সক্ষমতা কতটুকু।"
উদ্বেগ শুধু ব্যবহারকারীদের মধ্যেই নয়, রয়েছে মার্কিন সরকারেরও। বৃহস্পতিবার ডেমোক্রেট সিনেটর মার্ক ওয়ার্নার বলেন, "ইয়াহু হ্যাকিংয়ের ভয়াবহতা বিশাল।" প্রতিষ্ঠানটি সামনের সপ্তাহে এই আক্রমণের বিষয়ে তাকে অবহিত করবে বলেও জানান তিনি।
ইয়াহু জানিয়েছে, তারা বিশ্বাস করে, এই আক্রমণ কোনো রাষ্ট্রীয় পৃষ্ঠপোষকতায় চালানো হয়ছে। 

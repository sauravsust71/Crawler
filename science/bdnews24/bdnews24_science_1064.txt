যুক্তরাজ্যের ইউনিভার্সিটি অফ কেমব্রিজ-এর এক গবেষণায় সংবাদ বিকৃতকরণ দূর করতে মনস্তাত্ত্বিক টুল উদ্ভাবন করা হয়েছে।
গবেষকরা রলছেন, পাঠকের কাছে পরিকল্পনামাফিক খানিকটা ভুল খবর ধরিয়ে দিলে পাঠক ওই ধরনের খবর সম্পর্কে সচেতন হয়ে ওঠেন।
২০১৬ সালের মার্কিন যুক্তরাষ্ট্রের প্রেসিডেন্ট নির্বাচন এবং সিরিয়াবিষয়ক ভুয়া সংবাদগুলো এমন উদ্বেগ সৃষ্টি করেছে।
এই গবেষণার প্রধান গবেষক ড. স্যান্ডার ফন ডার লিন্ডেন বলেন, “ভুল সংবাদ ভাইরাসের মতো ছড়ায়।” তিনি আরও বলেন, “পরিকল্পনাটি ছিল স্ববুদ্ধিমত্তা তৈরি করা যেন তা ভুল সংবাদের বাধা হিসেবে কাজ করতে পারে।” এর ফলে পরবর্তীবার যখন কেউ অল্প পরিমাণেও সন্দেহ দেখবে ওই সংবাদ কাটিয়ে আসতে পারবে বলে জানান তিনি।

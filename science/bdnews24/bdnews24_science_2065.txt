ব্রিটিশ দৈনিক টেলিগ্রাফ-এর বরাতে আইএএনএস জানায়, ভুয়া সংবাদ ‘মানুষের মন হত্যা’ করছে বলে মন্তব্য করেছেন কুক। উন্নত সাংবাদিকতা করছেন এমন প্রতিষ্ঠানগুলোকে সহায়তায় কর্তৃপক্ষ ও প্রযুক্তি প্রতিষ্ঠানগুলোর কঠোর পদক্ষেপ নেওয়া অপরিহার্য হয়ে উঠেছে।
কুক বলেন, “এর ফলে সত্যবাদী, নির্ভরযোগ্য, রঙ চড়ানো নয় এমন সংবাদ সরবরাহকারীরা জিতবে।”
অনলাইনে প্রকাশিত বিকৃত খবরগুলোর হুমকি সম্পর্কে মানুষকে জানাতে কুক একটি প্রচারণা চালানোর জন্য বলেন।
তিনি বললেন, “আমরা এমন একটি সময় পার করছি যখন দূর্ভাগ্যবশত সে লোকজনই জয়ী হচ্ছে যারা তাদের ক্লিক বাড়ানোর চেষ্টায় সময় ব্যয় করছে, সত্যটা বলার জন্য নয়। একদিক থেকে এটি মানুষের মনকে হত্যা করছে।”
যেসব প্রযুক্তি প্রতিষ্ঠান ভুয়া সংবাদ ঠেকাতে খুব বেশি অবদান রাখেনি সেসব প্রতিষ্ঠানকে এ ধরনের সংবাদ কমানোর টুল বানাতে আহ্বান জানান তিনি।

রয়টার্স-এর প্রতিবেদনে বলা হয়, ব্রাজিল ও রাশিয়া থেকে শুরু হয়ে ভিয়েতনাম আর মিয়ানমার পর্যন্ত দেশগুলোতে ব্যবহারকারীরা হোয়াটসঅ্যাপ ব্যবহারে সমস্যার কথা জানিয়ে সামাজিক মাধ্যমগুলোতে অভিযোগ করছেন। কেন এই সমস্যা হলো তা এখনও জানা যায়নি।
এ সময় ভারতে টুইটারের সবচেয়ে বেশি আলোচনার বিষয় ছিল ‘হোয়াটসঅ্যাপডাউন’। ভারত এই অ্যাপের সবচেয়ে বড় বাজার, দেশটিতে প্রায় ২০ কোটি ব্যবহারকারী রয়েছে। ভারত ছাড়াও, পাকিস্তান, ব্রিটেন, জার্মানি ও আরও অনেক দেশে এই সমস্যার বিষয়টি আলোচনায় উঠে আসে।
সামাজিক মাধ্যমগুলোতে অভিযোগ তোলার প্রায় ৩০ মিনিট পর অ্যাপটি আবারও কাজ করা শুরু করে বলে জানিয়েছেন ব্যবহারকারীরা।
মালয়েশিয়া আর সিঙ্গাপুরেও এ নিয়ে অভিযোগ উঠেছে।
সিঙ্গাপুরে হোয়াটসঅ্যাপ-এর মালিক প্রতিষ্ঠান ফেইসবুক-এর এক মুখপাত্র বলেন, প্রতিষ্ঠানটি এখনও এ বিষয় নিয়ে তদন্ত চালাচ্ছে।
চলতি বছর হোয়াটসঅ্যাপে আরও কয়েকবার এমন সমস্যা দেখা গেছে। এর মধ্যে মে মাসে কয়েকঘণ্টা ধরে এমন সমস্যা ছিল।

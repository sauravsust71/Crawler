শুক্রবার মার্কিন আপিল বিভাগের এক আদালতে দেওয়া এক রায়ে জানানো হয়, কিম যুক্তরাষ্ট্রের বাইরে থাকায় তিনি 'পলাতক' হিসেবে গণ্য, এবং সে কারণে এই মামলা লড়ার অধিকার রাখেন না, আর এ কারণে তিনি তার সম্পদ পুনরুদ্ধারও করতে পারবেন না।  
অন্যদিকের, কিম-এর আইনজীবী ইরা পি. রথকেন জানান, তার মক্কেল পুরো বেঞ্চের সামনে এই রায় রিভিউয়ের আবেদন করবে। যদি দরকার হয়, সুপ্রিম কোর্টেও পিটিশন দাখিল করা হবে বলে জানান তিনি, খবর রয়টার্স-এর।
২০১৫ সালের নভেম্বরে অনলাইন ফাইল শেয়ারিং সাইট ‘মেগাআপলোড’-এর প্রধান কিম ডটকম ও তার তিন সহকর্মীকে বিচারের জন্য নিউ জিল্যান্ড থেকে যুক্তরাষ্ট্র সরকারের হাতে হস্তান্তর ইসু নিয়ে আদালতে শুনানি শেষ হয়।
সে সময় ডটকমের আইনজীবী রন ম্যানসফিল্ড বলেছিলেন, এই ইসুতে যদি শেষ পর্যন্ত মার্কিন সরকারের জয় হয় তবে এর প্রভাব ফেইসবুক থেকে ইউটিউব পর্যন্ত ইন্টারনেটের প্রায় সব ক্ষেত্রেই পড়বে।
২০১২ সালে কপিরাইট আইন লঙ্ঘনের অভিযোগে ‘মেগাআপলোড’ বন্ধ করে দেয় মার্কিন আদালত। অবৈধভাবে মুভি ডাউনলোড করার প্ল্যাটফর্ম হিসেবে সাইটটি ব্যবহার করা হচ্ছে এবং এতে বিপুল পরিমাণ আর্থিক ক্ষতির অভিযোগ করেছে মার্কিন কর্তৃপক্ষ।

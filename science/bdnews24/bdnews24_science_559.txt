প্রযুক্তি খাতে যুক্তরাজ্যের উন্নয়ন করতে হলে বাইরের দেশের মেধাবী অভিবাসীদের প্রযুক্তি প্রতিষ্ঠানগুলোতে সুযোগ দেওয়া উচিত বলে বিবিসি-কে বলেন আইভ।
সম্প্রতি রয়াল কলেজ অফ আর্ট -এর চ্যান্সেলর হিসেবে নিয়োগ দেওয়া হয়েছে তাকে।
“সিলিকন ভ্যালি’র মতো প্রযুক্তি হাবগুলোতেও অসাধারণ সাংস্কৃতিক বৈচিত্র্য রয়েছে,” বলেন আইভ।
ব্রেক্সিট প্রভাবের ফলে যুক্তরাজ্যের প্রযুক্তি প্রতিষ্ঠানগুলোতে অভিবাসীদের চাকুরির সুযোগ বন্ধ হতে পারে বলে আশংকা করা হচ্ছে। সেটি করা হলে তা প্রতিষ্ঠানগুলোর জন্য ভালো হবে না বলেই জানিয়েছেন এই আইফোন নকশাকারী।
“কোনো প্রতিষ্ঠানে নতুন পণ্য তৈরি, নতুন পণ্যের ধরন এবং বৃদ্ধির ক্ষেত্রে অনেকের জন্য প্ল্যাটফর্ম তৈরি করাটা মৌলিকভাবে গুরুত্বপূর্ণ,” বলেন স্যার জনাথন।
১৯৯৬ সাল থেকে অ্যাপলের নকশাকারী দলের নেতৃত্ব দিয়ে আসছেন ব্রিটিশ নাগরিক আইভ। আইফোন, আইপ্যাড-সহ প্রতিষ্ঠানের অন্যান্য পণ্য নকশার নেপথ্যে রয়েছেন তিনি।
তিনি আরও বলেন, “নকশা শিক্ষায় যুক্তরাজ্যের দারুণ রীতি রয়েছে, কিন্তু ক্যালিফোর্নিয়ার সিলিকন ভ্যালির মতো অবস্থানে যেতে হলে তাদের আরও অনেক কিছু করার আছে।”
“আমি মনে করি সিলিকন ভ্যালির স্টার্ট-আপ প্রতিষ্ঠানগুলোকে সমর্থনের মতো কাঠামো রয়েছে, প্রযুক্তিগত সহায়তাসহ তহবিল পর্যন্ত তারা দিয়ে থাকে।”
ব্রিটেনের ইউরোপিয়ান ইউনিয়ন থেকে সরে আসার পর যুক্তরাজ্যের কিছু প্রযুক্তি প্রতিষ্ঠানকে সতর্ক করা হয় যে তারা আন্তর্জাতিক মেধাবীদের হারাতে পারে। বার্লিনের মতো শহরগুলোও লন্ডন থেকে প্রযুক্তি প্রতিষ্ঠান সরিয়ে নেওয়ার পরিকল্পনা করছে বলে উল্লেখ করা হয়। ব্রেক্সিটের পর একেই ইউরোপের শীর্ষ প্রযুক্তি হাব বিবেচনা করা হয়ে থাকে।

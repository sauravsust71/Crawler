বুধবার ঢাকার বঙ্গবন্ধু আন্তর্জাতিক সম্মেলন কেন্দ্রে শুরু হওয়া ‘ডিজিটাল ওয়ার্ল্ড বাংলাদেশ’ এর প্রথম দিন ‘সাইবার সিকিউরিটি : ইন কনটেক্সট অব বাংলাদেশ’ শীর্ষ সেমিনারে সাইবার নিরাপত্তা ঝুঁকি, নিরাপত্তা ও করণীয় সম্পর্কে  আলোচনা করেন বিশেষজ্ঞরা।
অনুষ্ঠানে দেন রাজশাহী বিশ্ববিদ্যালয়ের আইনের অধ্যাপক জুলফিকার আহমেদ বলেন, রাজনীতি, অর্থনীতি ও শিক্ষার মতো গুরুত্বপূর্ণ নানা ক্ষেত্রে  নীতি-নির্ধারকরা সাইবার নিরাপত্তার আইনটি নিয়ে ‘ওয়াকিবহাল নন’।
“তারা সচেতন নন বলেই আমাদের দেশে প্রায়ই নানাভাবে সাইবার অ্যাটাক হচ্ছে। অথচ একটু সচেতন হলে, প্রযুক্তিবিষয়ক জ্ঞান থাকলে এসব বিষয় সহজেই এড়ানো যেত।”
এখন এই বিষয়টিতে সর্বোচ্চ নজর দেওয়ার পরামর্শ দিয়ে তিনি বলেন, “ডিজিটাল বাংলাদেশের বাস্তবায়ন করতে হলে এ বিষয়টি এড়িয়ে গেলে একেবারেই চলবে না।”
বাংলাদেশে সাইবার নিরাপত্তা ঝুঁকির কথা বলা হচ্ছে বেশ কিছু দিন ধরে; হ্যাকিংয়ের মাধ্যমে বাংলাদেশ ব্যাংকের রিজার্ভ খোয়া যাওয়ার পর তা প্রকটভাবে প্রকাশিত হয়। 
অনুষ্ঠানে এক প্রবন্ধে বিইউবিটির কম্পিউটার সায়েন্স অ্যান্ড ইঞ্জিনিয়ারিং বিভাগের অধ্যাপক ড. আমের আলী বলেন, যুক্তরাষ্ট্র, যুক্তরাজ্য, জার্মানি ও চীনসহ বিশ্বের উন্নত দেশগুলো সাইবার নিরাপত্তা খাতের সুরক্ষায় ১৪৪৫ বিলিয়ন ডলার ব্যয় করেছে।
অনুষ্ঠানে পূ্বালী ব্যাংকের কর্মকর্তা মো. আলী বলেন, রিজার্ভ চুরির আগে দেশের ব্যাংকগুলোর নিরাপত্তা ব্যবস্থা ‘যথেষ্ট নিরাপদ’ ছিল না’। তবে এখন তারা ‘ সর্বোচ্চ নিরাপত্তা ব্যবস্থা নিশ্চিতের চেষ্টা করছেন’।
বিইউবিটির উপাচার্য অধ্যাপক আবু সালেহ সাইবার নিরাপত্তা ইস্যুতে কাজ করতে তরুণদের সর্বোচ্চ অবদান রাখতে অনুরোধ জানান।
তথ্যপ্রযুক্তি বিষয়ক আইনের ৫৭ ধারার প্রয়োগ নিয়ে সমালোচনার জবাবে এই আইন প্রণয়ণের সঙ্গে যুক্ত অধ্যাপক জুলফিকার বলেন, “এই আইনটি নিয়ে আসলে বিভ্রান্তি ছড়ানো হয়েছে। এই ধারাটির প্রয়োজন ছিল।”
ডিজিটাল নিরাপত্তা আইন হলে বিতর্কের অবসান ঘটবে বলে আশা প্রকাশ করেন তিনি।

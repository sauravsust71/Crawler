ওয়াল স্ট্রিট বিশ্লেষকদের প্রত্যাশিত অংক ছাড়িয়ে যাওয়ার মাধ্যমে প্রতিষ্ঠানটির শেয়ার মূল্য বেড়েছে বলে জানিয়েছে ব্রিটিশ ট্যাবলয়েড মিরর। ক্রমাগত আইফোন বিক্রি কমে যাওয়ায়, ২৫ জুন শেষ হওয়া প্রতিষ্ঠানটির তৃতীয় প্রান্তিকে আয় কমেছে আগের চেয়ে ১৫ শতাংশ।
আগের বছরের এই প্রান্তিকের তুলনায় চলতি বছর আইফোন বিক্রি ১৫ শতাংশ কমে চার কোটি চার লাখে এসে দাড়িঁয়েছে। তবে, বিশ্লেষকদের প্রত্যাশিত হিসাবের গড়ে এ সংখ্যাটা ছিল ৩৯৯০০০০০।
প্রতিষ্ঠানটি ব্যবসায়ের পতন সবচেয়ে বেশি হয়েছে চীনে। এই দেশটি থেকে অ্যাপলের মোট আয়ের ২০ শতাংশ আসে।
আইপ্যাড-এর ক্ষেত্রে এবার চমক দেখেছে প্রতিষ্ঠানটি। ১০টি প্রান্তিকের মধ্যে এই প্রথমবার অ্যাপল তাদের আইপ্যাড থেকে আয় বাড়িয়েছে। আইপ্যাড থেকে প্রতিষ্ঠানটির আয় এসেছে ৪৯০ কোটি ডলার, এক বছর আগে যা ছিল ৪৫০ কোটি ডলার।   

মূলত বাইকটি দিয়ে ভবিষ্যৎ প্রজন্মের যানবাহন কেমন হতে পারে সেটিরই কিছুটা ধারণা দেওয়ার চেষ্টা করেছে বিএমডাব্লিউ। প্রতিষ্ঠানটির ১০০ বছর পূর্তি উপলক্ষে এই বাইকের কনসেপ্ট উন্মোচন করা হয়, জানিয়েছে ব্যবসা-বাণিজ্যবিষয়ক প্রকাশনা বিজনেস ইনসাইডার।
এখন পর্যন্ত তিনটি স্বচালিত গাড়ির মডেল উন্মোচন করেছে বিএমডাব্লিউ, যা তাদের ভিশন নেক্সট ১০০ লাইন প্রকল্পের একটি অংশ। সম্প্রতি উন্মোচন করা মোটরসাইকেলটি এই প্রকল্পের প্রথম মডেল।
মোটরসাইকেলটির আকার এমন যে এটির সামনে থেকে পিছনের চাকা পর্যন্ত ক্রমান্বয়ে ঢালু হয়ে গেছে। এই ফ্লেক্সফ্রেম গঠনের কারণে বিভিন্ন প্রতিকূল সড়কে এটি বিশেষ সুবিধা পাবে বলে জানানো হয়।
বর্তমান সময়ের অন্যান্য মোটরসাইকেলের মতো এটিতে এক অংশের সঙ্গে আরেক অংশের জোড়া নেই। যার ফলে হ্যাণ্ডেলবার বাঁকানোর সঙ্গে সঙ্গে সম্পূর্ণ মোটরসাইকেলটিও বেঁকে যাবে।
মোটরসাইকেলটির চাকা দুটি বেশ বড় আকৃতির এবং সেগুলো এতই মজবুত যে চালক এর উপর স্থির হয়ে বসে থাকলেও এটি কোনো দিকেই হেলে পরবে না। সব সময় সোজা অবস্থায় থাকবে বাইকটি। দুই চাকার উপর ভর করে সোজা হয়ে ভারসাম্য বজায় রাখার সুবিধা চালক শুধু স্থির অবস্থায় না চলন্ত অবস্থায়ও পাবেন।
ওজন কমানোর জন্য মোটরসাইকেলটির ফেন্ডার, আসন ও ফ্লেক্সফ্রেম সবকিছুই কার্বন ফাইবার দিয়ে তৈরি করা হয়েছে।
এর ইঞ্জিনটি পুরনো আমলের ফ্লাট-টুইন-এর মতো হলেও বিএমডাব্লিউ এর মতে এর শূন্য নির্গমন ব্যবস্থা ভবিষ্যৎ প্রজন্মের সঙ্গে মিল রেখেই তৈরি করা হয়েছে।
মোটরসাইকেলটির সবচেয়ে উল্লেখযোগ্য বৈশিষ্ট্য হচ্ছে এর অগমেন্টেড রিয়ালিটি চশমা, যা চালকের সামনে সরাসরি মোটরসাইকেল সংক্রান্ত সকল তথ্য প্রদর্শন করে। এমনকি প্রয়োজন অনুসারে চালক এই তথ্য প্রদর্শন ব্যবস্থাকে নিয়ন্ত্রণ করতে পারে। এটি সব সময় যতটা সম্ভব কম তথ্য প্রদানের চেষ্টা করে, যদিও চালক চাইলে আরও সুনির্দিষ্ট তথ্য পেতে পারেন।

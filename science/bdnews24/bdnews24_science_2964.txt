মেইট ১০ ও মেইট ১০ প্রো-এর সঙ্গে বিশেষ সংস্করণের পোর্শ ডিজাইন মেইট ১০ উন্মোচন করেছে স্মার্টফোন নির্মাতা চীনা প্রতিষ্ঠানটি।
মেইট ১০ ও মেইট ১০ প্রো-এর আকারে রাখা হয়েছে  ভিন্নতা। মেইট ১০-এর পর্দার মাপ ৫.৯ ইঞ্চি। আর এর চেয়ে কিছুটা লম্বাটে মেইট ১০ প্রো-এর পর্দার মাপ ৬ ইঞ্চি। অন্যদিকে বিশেষ সংস্করণে ‘পোর্শ ডিজাইন ব্র্যান্ডিং’ ছাড়া মেইট ১০ প্রো-এর সঙ্গে অভ্যন্তরীন কোনো পরিবর্তন রাখেনি হুয়াওয়ে। এখানে শুধু পার্থক্য দামের। পোর্শ ডিজাইন ব্র্যান্ডিংয়ের কারণে দাম অন্য ডিভাইসের চেয়ে অনেক বেশি রাখা হয়েছে ডিভাইসটির।
মেইট ১০-এ হেডফোন জ্যাক রাখা হলেও মেইট ১০ প্রো থেকে হেডফোন জ্যাক বাদ দিয়েছে হুয়াওয়ে। এর সঙ্গে ডিভাইসগুলোকে করা হয়েছে পানি নীরোধী।
নতুন ডিভাইসগুলোতে ৪০০০ মিলিঅ্যাম্পিয়ার ব্যাটারি ব্যবহার করেছে হুয়াওয়ে। আর উন্নত ডুয়াল লেন্স লাইকা ক্যামেরা রয়েছে স্মার্টফোনগুলোতে।
ডিভাইসগুলোর নকশাতেও ব্যতিক্রম দেখা গেছে। এবার অ্যালুমিনিয়ামের পরিবর্তে গ্লাস ব্যবহার করা হয়েছে স্মার্টফোনগুলোতে। আর এতে বেজেলের আকারও কমিয়ে এনেছে চীনা প্রতিষ্ঠানটি।
মেইট ১০ হার্ডওয়্যারের মূল আকর্ষণ বলা হয়েছে এর চিপ। এতে ব্যবহার করা হয়েছে হুয়াওয়ে’র নিজস্ব কিরিন ৯৭০ প্রসেসর।
হুয়াওয়ের দাবি তাদের নতুন এই চিপটি কৃত্রিম বুদ্ধিমত্তা প্রযুক্তির ওপর ভিত্তি করে তৈরি করা হয়েছে। ফলে সময়ের সঙ্গে ডিভাইসগুলো আরও দ্রুতগতির হবে বলে উল্লেখ করা হয়। এক বছর ব্যবহারের পর ডিভাইসটি প্রায় ৮০ শতাংশ দ্রুত গতির হবে বলে দাবি করেছে হুয়াওয়ে, বলা হয়েছে মার্কিন সংবাদমাধ্যম সিএনবিসি’র প্রতিবেদনে।
প্রতিষ্ঠানের এআই প্রতি মিনিটে ২০০৫টি ছবি শণাক্ত করতে পারে বলে জানিয়েছে হুয়াওয়ে।  ফলে ছবি তোলার সময় কোনো বস্তু শণাক্ত করে স্বয়ংক্রিয়ভাবে এর আলো এবং অন্যান্য বিষয়গুলো ঠিক করে নেবে ডিভাইসটি।
নতুন মেইট ১০-এর নেটওয়ার্ক গতিতেও আনা হয়েছে পরিবর্তন। ডিভাইসটি প্রতি সেকেন্ডে ১.২ গিগাবিট ডেটা ডাউনলোড করতে পারে। হুয়াওয়ের দাবি এমন গতিতে ডেটা ডাউনলোড করতে পারে এটিই এমন প্রথম ডিভাইস।
অক্টোবরের শেষ নাগাদ স্পেন, সিঙ্গাপুর এবং অস্ট্রেলিয়াসহ ১৫টি দেশের বাজারে আনা হয়েছে মেইট ১০। অন্যদিকে জার্মানি এবং ইতালিসহ ২৪টির বেশি দেশে নভেম্বরের মাঝামাঝি পাওয়া যাবে মেইট ১০ প্রো।

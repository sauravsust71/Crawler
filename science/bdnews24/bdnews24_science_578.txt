গ্যালাক্সি এস৮ আর এস৮+ নামের এই দুই স্মার্টফোনের আকার ২০১৬ সালে আনা এস৭ আর এস৭ এজ-এর সমান হলেও, নতুন ফোনদুটির স্ক্রিন বড় করা হয়েছে। এবার দুই মডেলের স্মার্টফোনেই রাখা হয়েছে পাশ দিয়ে বাকানো ডিসপ্লে।
২০১৬ সালে নিজেদের ফ্ল্যাগশিপ স্মার্টফোন গ্যালাক্সি নোট ৭ -এর বিস্ফোরণ আর অগ্নিঝুঁকির কারণে তা বাজার থেকে তুলে নিতে বাধ্য হয় স্যামসাং। এরপর এ নিয়ে নানা নিরাপত্তাবিষয়ক পদক্ষেপ নিয়েছে প্রতিষ্ঠানটি। অন্যদিকে, নিজ দেশে প্রতিষ্ঠানটি দুর্নীতির অভিযোগের মুখে পড়েছে, সব মিলিয়ে নতুন এই ফ্ল্যাগশিপ স্মার্টফোনদুটি আনার সময়টা প্রতিষ্ঠানটির জন্য খুব একটা ভালো নয় বলাই যায়।
এস৮-এর দাম ধরা হয়েছে ৬৯০ পাউন্ড আর এস৮+ এর দাম রাখা হয়েছে ৭৮০ পাউন্ড। ২০১৬ সালে এস৭-এর দাম রাখা হয়েছিল ৫৬৯ পাউন্ড আর এস৭ এজ-এর ৬৩৯ পাউন্ড। চলতি বছর ২১ এপ্রিল নতুন স্মার্টফোনদুটি বাজারে ছাড়া হবে।
মার্কিন প্রযুক্তি জায়ান্ট অ্যাপলের সিরি’র মূল নির্মাতাদের মধ্যে কয়েকজনের কাছ থেকে নেওয়া প্রযুক্তির উপর ভিত্তি করে বানানো ভার্চুয়াল অ্যাসিস্ট্যান্ট বিক্সবি আনা হয়েছে নতুন এই দুই স্মার্টফোনে। ভার্চুয়াল অ্যাসিস্ট্যান্টটিকে চালু করতে হ্যান্ডসেটদুটির পাশে রাখা হয়েছে একটি বাটন। নতুন এই ভার্চুয়াল অ্যাসিস্ট্যান্ট দিয়ে ফটো গ্যালারি, মেসেজ আর ওয়েদারসহ ১০টি বিল্ট-ইন অ্যাপ কন্ঠের মাধ্যমেই চালানো যাবে।
বিক্সবি গুগল প্লে মিউজিক-এর সঙ্গেও কাজ করবে। অদূর ভবিষ্যতে একে তৃতীয় পক্ষের অ্যাপগুলোর জন্যও উন্মুক্ত করার ইচ্ছা প্রকাশ করলেও কবে করা হবে তা নিয়ে কিছু জানায়নি স্যামসাং। বর্তমানে বিক্সবি শুধু মার্কিন ও কোরীয় কন্ঠ বুঝতে সক্ষম।
১। দ্রুত অটোফোকাস ক্ষমতার আট মেগাপিক্সেল সামনের ক্যামেরা।
২। পেছনের ক্যামেরা আগের মতো ১২ মেগাপিক্সেল রাখা হলেও, আরও উন্নত করা হয়েছে বলে জানানো হয়।
৩। সেন্ট্রাল প্রসেসর ইউনিট (সিপিইউ) আগের চেয়ে ১০ শতাংশ বেশি ও গ্রাফিক্স প্রসেসিং ইউনিট (জিপিইউ) ২১ শতাংশ শক্তিশালী বলে দাবি নির্মাতাদের।
৪। রাখা হয়েছে আইরিস স্ক্যানার, এতে আঙ্গুলের ছাপের পরিবর্তে পরিচয় শনাক্তকরণের বিকল্প পন্থা হিসেবে ‘আই-প্রিন্টস’ ব্যবহার করা হবে।

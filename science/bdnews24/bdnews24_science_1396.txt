রয়টার্স-এর তথ্যমতে, সাবস্ক্রিপশন বাড়ার ফলে মোবাইল ডেটার ব্যবহার আটগুণ হবে বলেও জানিয়েছে প্রতিষ্ঠানটি। ২০২২ সালের শেষে স্মার্টফোন সাবস্ক্রিপশনের সংখ্যা ৬৮০ কোটি হবে বলে আশা করছে এরিকসন, ২০১৬ সালে যার সংখ্যা ৩৯০ কোটি।
এর আগে চলতি বছরের জুনে প্রতিষ্ঠানটির পক্ষ থেকে বলা হয়েছিল, ২০২১ সালের মধ্যে সাবস্ক্রিপশন হবে ৬৩০ কোটি।
২০২১ সালে ৫জি সাবস্ক্রিপশন ১৫ কোটি থেকে বেড়ে ২০২২ সালে ৫৫ কোটি হবে বলে জানিয়েছেন এরিকসন-এর কৌশল ও প্রযুক্তি প্রধান উল ইওয়াডসন।
তিনি বলেন, "আমারা ধারণা করছি এর মধ্যে ২৫ শতাংশ উত্তর আমেরিকা এবং ১০ শতাংশ এশিয়া থেকে আসবে।"

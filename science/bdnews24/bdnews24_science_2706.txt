ডিসেম্বরের শুরতেই এই অ্যাপের তালিকা তৈরি শেষ করায় এতে জায়গা পায়নি সম্প্রতি আইওএস প্লাটফর্মে ছাড়া নিনটেনডো’র আলোচিত মোবাইল গেইম সুপার মারিও রান, জানিয়েছে প্রযুক্তি সাইট বিজনেস ইনসাইডার।
অ্যাপলের তালিকায় জায়গা পাওয়া বিনামূল্যে ডাউনলোড করা যায় এমন অ্যাপগুলোর মধ্যে ২০১৬ সালে সবচেয়ে বেশি ডাউনলোড করা ১০টি অ্যাপ হচ্ছে-
১. ছবি শেয়ারিং অ্যাপ স্ন্যাপচ্যাট
২.  ফেইসবুকের মেসেজিং অ্যাপ ফেইসবুক মেসেঞ্জার
৩. অগমেন্টেড রিয়ালিটি গেইম পোকিমন গো
৪. ছবি শেয়ারিং অ্যাপ ইনস্টাগ্রাম
৫. সামাজিক মাধ্যম ফেইসবুকের অ্যাপ
৬. ভিডিও স্ট্রিমিং অ্যাপ ইউটিউব
৭. ম্যাপিং সেবাদাতা গুগল ম্যাপস
৮. মিউজিক স্ট্রিমিং সাইট প্যান্ডোরা
৯. ভিডিও স্ট্রিমিং অ্যাপ নেটফ্লিক্স

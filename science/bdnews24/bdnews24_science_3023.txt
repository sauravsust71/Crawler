“স্ট্রিম প্লাস একটি লয়ালটি পয়েন্ট সিস্টেম যেখানে খেলোয়াড়রা স্ট্রিম দেখার মাধ্যমে পয়েন্ট অর্জন করতে পারবেন", বলেন অ্যামাজন গেইম স্টুডিওজ-এর  প্যাট্রিক গিলমোর। তিনি জানান, পয়েন্ট 'পোল এবং বাজি' খেলায় ব্যবহার হতে পারে এবং গেইমের আইটেম কেনায় এই পয়েন্ট ব্যবহার করা যেতে পারে।
অ্যামাজন স্টুডিওজ-এর আসন্ন মাল্টিপ্লেয়ার গেইম ব্রেকওয়ে-তে প্রথম স্ট্রিম প্লাস ব্যবহার করা হচ্ছে।
‘ব্লিজার্ড’, ‘ভালভ’ এবং ‘রায়ট’-এর মতো প্রতিদ্বন্দ্বী প্রতিষ্ঠানগুলো দলীয় যুদ্ধের গেইম তৈরি করে থাকে। অ্যামাজনের ব্রেকওয়ে প্রতিদ্বন্দ্বীদের জন্য জবাব হিসেবে কাজ করবে।
যুক্তরাজ্যভিত্তিক ডিজিটাল ডেটা গবেষক প্রতিষ্ঠান আইএইচএস মার্কিট-এর পিয়ের হার্ডিং রোলস বলেন, “শেষ কয়েক বছরে অ্যামাজন নিজেদের গেইমের ক্ষমতার সংস্করণে অনেক বিনিয়োগ করেছে কিন্তু বাজারে তার ব্যাবসায়িকভাবে কোনো প্রভাব আসেনি।“
“২০১৪ সালে অ্যামাজনের টুইচ কিনে ফেলা তাদের নিজেদের গেইমের সংস্করণের জন্য একটি নতুন দৃঢ়তা এনেছে, গেইমগুলো একদম এমনভাবে তৈরি যেন সব টুইচ ভিউয়াররা আরও আগ্রহী হন", যোগ করেন তিনি।

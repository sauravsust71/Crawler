চলতি বছরের ৭ সেপ্টেম্বর এক ইভেন্টে গ্রাহকের জন্য আইওএস ১০ উন্মুক্ত করে অ্যাপল। নতুন এই আপডেটেই দেখা দিয়েছে বাগ। কিছু গ্রাহক অভিযোগ জানিয়েছেন একবার 'এয়ারপ্লেন মোড' অন করে সিগনাল ব্লক করা হলে পরে মোড অফ করা হলেও সিগনাল ফিরে পাওয়া যায়নি এবং সেখানে 'নো সার্ভিস' লেখা দেখানো হচ্ছে বলে জানানো হয়।
কিছু ক্ষেত্রে দেখা গেছে আইফোনটি পুনরায় চালু করা হলে ফোনে সিগনাল ফিরে এসেছে। তবে, কিছু গ্রাহককে অ্যাপল স্টোর-এ গিয়ে আইফোন পরিবর্তন করার কথা জানানো হয়েছে।
ঠিক কি কারণে এই সমস্যা দেখা গিয়েছে সেটি এখনও স্পষ্ট নয়। আর সমস্যাটি শুধু আইফোন ৭ এবং আইফোন ৭ প্লাসে দেখা গিয়েছে নাকি আইওএস ১০ এর কারণে দেখা গিয়েছে সেটিও পরিষ্কার নয়।
ইয়াসির এল-হেগান নামের এক ইউটিউবার বলেন, "আজ হঠাৎ করেই আমার আইফোন ৭ জেট ব্ল্যাক মডেলে আমি ইন্টারনেট সংযোগ পাচ্ছিলাম না, এমনকি কোনো কল করতেও পারছিলাম না, যদিও সেখানে চার দাগ নেটওয়ার্ক দেখাচ্ছিল।
তিনি আরও বলেন,"আমি তখন ফোনটিতে এয়ারপ্লেন মোড অন করি এবং সেটি বন্ধ করি, তখন ফোনটিতে 'নো সার্ভিস' লেখা দেখায়। ফোনটির উপরের ডান কোণায় বেশ গরম হয়ে ওঠে।"

মার্কিন সংবাদমাধ্যম সিএনবিসি জানিয়েছে, গেইমটি দিয়ে প্রথম মাসে ৭১ মিলিয়ন মার্কিন ডলার আয় হবে বলে ধারণা করা হচ্ছে যা প্রতিষ্ঠানের অগমেন্টেড রিয়ালিটি গেইম ‘পোকিমন গো’ এর প্রথম মাসের আয়ের অর্ধেক।
চলতি বছরের ৭ সেপ্টেম্বর অ্যাপল ইভেন্টে গেইমটির ডেমো দেখানো হয়। অ্যাপ ডেটা বিশ্লেষণা প্রতিষ্ঠান ‘সেন্সর টাওয়ার’ তাদের এক গবেষণায় প্রকাশ করেছে উন্মোচনের দিনই দুই কোটি গ্রাহক গেইমটি ডাউনলোডের জন্য প্রস্তুত রয়েছেন।
প্রাথমিকভাবে গেইমটি বিনামূল্যে ডাউনলোডের সুযোগ দেওয়া হলেও সম্পূর্ণ সংস্করণ ডাউনলোড করতে গ্রাহককে ৯.৯৯ মার্কিন ডলার পরিশোধ করতে হবে।
এর মাধ্যমে প্রথম ৩০ দিনে ৭১ মিলিয়ন আয় করবে নিনটেনডো। যেখানে ‘পোকিমন গো’-এর প্রথম মাসের আয় ছিল ১৪৩ মিলিয়ন। আর ‘ক্ল্যাশ রয়াল’ গেইমের এক মাসের আয় ছিল ১০৭ মিলিয়ন ডলার।
বিশ্লেষকরা ধারণা করছেন ‘সুপার মারিও রান’ ‘পোকিমন গো’-এর আয়কে ছাড়িয়ে যাবে।
সেন্সর টাওয়ার-এর মোবাইল ইনসাইট প্রধান র‍্যান্ডি নেলসন বলেন, “আমাদের কোনো সন্দেহ নেই প্রথম মাসের ‘সুপার মারিও রান’-এর ডাউনলোড ঐতিহাসিক হবে, হয়তো পোকিমন গো-এর চেয়ে বেশি হবে। ”

রয়টার্স জানিয়েছে, দুই মাস বিলম্বের পর এবার ‘এয়ারপডস’ বাজারে আনতে যাচ্ছে অ্যাপল। তবে, বিলম্বের কারণ ব্যাখ্যা করেনি নির্মাতা প্রতিষ্ঠানটি।
এর আগে ধারণা করা হয়েছিল বড়দিনের ছুটির আগে বাজারে আসবে না এয়ারপডস। এবার প্রতিষ্ঠানটির পক্ষ থেকেই জানানো হয়েছে সামনের সপ্তাহ থেকে ডিভাইসটির সরবরাহ শুরু হবে।
অ্যাপল স্টোর এবং প্রতিষ্ঠানের অনুমোদিত বিক্রেতার কাছে পাওয়া যাবে হেডফোনটি। চলতি বছরের ৭ সেপ্টেম্বর অ্যাপল ইভেন্টে আইফোন ৭-এর সঙ্গে উন্মোচন করা হয় নতুন এয়ারপডস। সে সময় বলা হয়েছিল অক্টোবরে বাজারে আসবে হেডফোনটি।
আগের মাসে প্রতিষ্ঠানটির পক্ষ থেকে জানানো হয় ডিভাইসটি প্রস্তুত করতে বিলম্ব হচ্ছে। তবে, বিলম্বের কারণ এবং কবে নাগাদ তা বাজারে আসবে তা জানানো হয়নি।
সে সময় প্রতিষ্ঠানের এক মুখপাত্র বলেন, “আমরা কোনো পণ্য পুরোপুরি তৈরি না করে বাজারে আনায় বিশ্বাস করি না।”
প্রতিষ্ঠানটির পক্ষ থেকে আবারও জানানো হয়, এয়ারপডস-এর সমস্যাগুলো সমাধান করতে “আরও বেশি সময়” লাগবে। তাই ধারণা করা হয়েছিল প্রতিষ্ঠানের নতুন ওয়্যারলেস হেডফোন ‘এয়ারপডস’ বড়দিনের জন্য সময়মত প্রস্তুত হবে না।
গ্যাজেটসহ বিভিন্ন পশ্চিমা নির্মাতার লক্ষ্য থাকে বড়দিনের আগেই দোকানের শেলফে নিজেদের পণ্য পৌঁছে দেওয়া। বড়দিনের ছুটির কারণে বছরের শেষ তিন মাসে গ্যাজেটসহ বিভিন্ন উপহারসামগ্রী বিক্রি হয় সবচেয়ে বেশি।

১৩ বছর ধরে স্যান্টা ট্র্যাকার চালিয়ে আসছে মার্কিন ওয়েব জায়ান্ট প্রতিষ্ঠানটি। এর মাধ্যমে স্যান্টা ক্লজ বর্তমানে কোথায় অবস্থান করছেন এবং পরবর্তীতে কোথায় উপহার দিতে যাবেন তার অবস্থান জানিয়ে থাকে গুগল, বলা হয়েছে ভারতীয় সংবাদমাধ্ম আইএএনএস-এর প্রতিবেদনে।
ওয়েব ব্রাউজার, মোবাইল ওয়েব ব্রাউজার, অ্যান্ড্রয়েড অ্যাপ, অ্যান্ড্রয়েড টিভি এবং ক্রোমকাস্ট-এর মাধ্যমে স্যান্টার অবস্থান জানা যাবে। এ ছাড়া গুগল পিক্সেল স্মার্টফোন ও গুগল হোম ডিভাইসেও আপডেট দেওয়া হবে।
স্যান্টা’র অবস্থান জানার পাশাপাশি এতে গেইমস এবং বড়দিন সম্পর্কে জানার সুযোগ রয়েছে। এর মাধ্যমে শিক্ষার্থীদের জন্য পাঠ পরিকল্পনা এবং ভিডিও নির্দেশিকা ডাউনলোড করতে পারবেন শিক্ষকরা।
স্যান্টা ট্র্যাকার-এর মাধ্যমে বিশ্বের বিভিন্ন দেশের বড়দিনের প্রথাগুলোও জানা যাবে বলে প্রতিবেদনে উল্লেখ করা হয়েছে।

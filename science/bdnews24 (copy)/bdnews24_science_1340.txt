রক্ষণশীল রাজতন্ত্রে থাকা দেশটিতে প্রযুক্তি প্রসার বাড়াতে এটি যুবরাজ মোহাম্মদ বিন সালমান-এর প্রচেষ্টার অংশ বলে সংবাদমাধ্যমটির প্রতিবেদনে উল্লেখ করা হয়েছে।  
সৌদি আরবের বিদেশি বিনিয়োগবিষয়ক কর্তৃপক্ষ সৌদি অ্যারাবিয়ান জেনারেল ইনভেস্টমেন্ট অথরিটি’র সঙ্গে অ্যাপলের আলোচনার কথা তৃতীয় একটি সূত্রও নিশ্চিত করেছে বলে প্রতিবেদনে জানানো হয়।
অ্যাপল আর অ্যামাজন- দুই প্রতিষ্ঠানই বর্তমানে তৃতীয় পক্ষের মাধ্যমে সৌদি আরবে পণ্য বিক্রি করে থাকে। কিন্তু দেশটিতে এই দুই প্রতিষ্ঠান আর অন্যান্য বৈশ্বিক প্রযুক্তি জায়ান্টগুলোর এখনও সরাসরি কোনো উপস্থিতি নেই।  
অ্যামাজনের আলোচনায় নেতৃত্ব দিচ্ছে প্রতিষ্ঠানটির ক্লাউড কম্পিউটিং বিভাগ অ্যামাজন ওয়েব সাইর্ভিসেস বা এডাব্লিউএস। সৌদি আরবের এ খাতে বর্তমানে বাজারের আধিপত্য এসটিসি আর মোবাইলি’র মতো অপেক্ষাকৃত ছোট ও স্থানীয় প্রতিষ্ঠানগুলোর হাতে, অ্যামাজন এলে এখানে শক্ত প্রতিদ্বন্দ্বীতা শুরু হবে।
শেষ দুই বছর ধরে সৌদি সরকার তাদের নীতিমালাবিষয়ক প্রতিবন্ধকতা শিথিল করছে। অপরিশোধিত তেলের দাম কমে যাওয়ার পর তেলনির্ভর এ অর্থনীতির জন্য বৈচিত্র্য আনা দরকারি হয়ে পড়েছে।  
অ্যাপল আর অ্যামাজনকে এ দেশে আসতে প্রলুব্ধ করা যুবরাজের পুনর্গঠন পরিকল্পনার অংশ হতে পারে। এর মাধ্যমে প্রতিষ্ঠানগুলোও নতুন ও অপেক্ষাকৃত সমৃদ্ধ একটি বাজারে প্রবেশ করতে পারছে।
দেশটির মোট জনসংখ্যার প্রায় ৭০ শতাংশই ৩০ বছরের কমবয়সী আর তারা অধিকাংশই সামাজিক মাধ্যমের সঙ্গে যুক্ত।  
২০১৮ সালের ফেব্রুয়ারিতে দেশটির বিদেশি বিনিয়োগবিষয়ক কর্তৃপক্ষের সঙ্গে অ্যাপল স্টোরের একটি লাইসেন্সিং চুক্তি হবে বলে আশা করা হচ্ছে। এর সঙ্গে অ্যাপল ২০১৯ সালের জন্য দেশটিতে প্রাথমিকভাবে একটি স্টোর স্থাপনের লক্ষ্য নিয়েছে বলে খবরে বলা হয়।  
অ্যামাজনের সঙ্গে আলোচনা এখনও শুরুর দিকে আছে আর এ নিয়ে বিনিয়োগ পরিকল্পনায় কোনো নির্দিষ্ট তারিখ ঠিক করা হয়নি। 
বাজার গবেষণা প্রতিষ্ঠান ইউরোমনিটর-এর তথ্যমতে, সৌদি আরবের স্মার্টফোন বাজারে স্যামসাংয়ের পরেই দ্বিতীয় শীর্ষস্থানে রয়েছে অ্যাপল। 
চলতি বছর শুরুরে দেশটিরে খুচরা পণ্য বিক্রির পুরো অ্যাকসেস পেতে দুবাইভিত্তিক অনলাইন খুচরা বিক্রেতা প্রতিষ্ঠান সোউক ডটকম-কে কিনে নেয় অ্যামাজন।  

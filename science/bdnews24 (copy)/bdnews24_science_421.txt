জরিমানার আদেশের ক্ষেত্রে নীতিনির্ধারকদের দাবি, গুগল তাদের সার্চ ইঞ্জিনের ফলাফল প্রদর্শনের সময় নিজস্ব কেনাকাটা তুলনার সেবাগুলো উপরের দিকে দেখিয়ে ক্ষমতার অপব্যবহার করছে। এই মামলায় নীতিনির্ধারকদের ধার্য করে দেওয়া জরিমানার অংকের হিসাবে এটি এখন পর্যন্ত সর্বোচ্চ। সেই সঙ্গে প্রতিষ্ঠানটি এই ধরনের কাজ অব্যাহত রাখলে জরিমানার অংক আরও বাড়তে পারে বলেও জানায় কমিশন।
এ নিয়ে আর কোনো মন্তব্য নেই বলে জানিয়েছে গুগল, খবর বিবিসি’র।
এই জরিমানা যখন ধার্য করা হয় তখন ইউরোপীয় ইউনিয়ন-এর প্রতিযোগিতাবিষয়ক কমিশনার মারগ্রেথ ভেস্টেগার বলেছিলেন, গুগলের কার্যক্রম “ইইউ অ্যান্টিট্রাস্ট নীতিমালা অনুযায়ী অবৈধ।”
এরপর গুগলের একজন মুখপাত্র বলেন, প্রতিষ্ঠানটি এই আদেশ মানতে ‘সম্মানের সঙ্গে অসম্মতি’ প্রকাশ করেছে। 
ওই সময় গুগলকে ‘প্রতিযোগিতাবিরোধী’ চর্চা বন্ধে ৯০ দিন সময় দেওয়া হয়েছিল। তা না হলে গুগলের মূল প্রতিষ্ঠান অ্যালফাবেট-এর গড় দৈনিক আয়ের পাঁচ শতাংশ করে প্রতিদিন জরিমানা বাড়তে থাকবে বলেও উল্লেখ করা হয়েছিল। 

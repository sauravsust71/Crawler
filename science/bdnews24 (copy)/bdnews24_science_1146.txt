উন্মোচনকে সামনে রেখে কর্মীরা ডিভাইস আনার সময় একটি লকআউট কৌশল চালু হয়ে যাওয়ায় এই ফেইস আইডি ত্রুটি ঘটে বলে দাবি প্রতিষ্ঠানটির। 
ওই সময় কোনোভাবে সামাল দিয়েছিলেন অ্যাপলের সফটওয়্যার প্রধান ক্রেইগ ফেডেরিঘি। তবে এ ঘটনার খবর ছড়িয়ে পড়ে বলে উল্লেখ করা হয়েছে বিবিসি’র প্রতিবেদনে।
নাম প্রকাশ না করা প্রতিষ্ঠানের একজন প্রতিনিধি বলেন, “কর্মীরা মঞ্চে প্রদর্শনের জন্য ডিভাইসটি নিয়ে আসছিল কিন্তু তারা বুঝতে পারেনি যে ফেইস আইডি তাদের চেহারা শনাক্ত করার চেষ্টা করছে।”
“তাদের মধ্যে কেউ ক্রেইগ ফেডেরিঘি ছিলেন না, তাই কয়েকবার ব্যর্থ হওয়ার পর, আইফোনটিকে যেভাবে বানানো হয়েছে এটি তাই করেছে, পাসকোড দিতে হয়েছে।”
এই কথা বিবিসি-কে নিশ্চিত করেছে অ্যাপল।
ফেইস আইডি ব্যবস্থায় ব্যবহারকারী তার আইফোন X-এর দিকে তাকালেই তার চেহারা শনাক্ত করা হবে ও ডিভাইসটই আনলক করা হবে।  এই ফিচার দেখাতে ১২ সেপ্টেম্বরের ইভেন্টে মঞ্চে ছিলেন অ্যাপলের জেষ্ঠ্য ভাইস প্রেসিডেন্ট ক্রেইগ ফেডেরিঘি। মঞ্চে ফিচার প্রদর্শনের সময় কাজ করছিল না ফেইসআইডি, এক পর্যায়ে পাসকোড ব্যবহার করে লগ ইন করতে বাধ্য হন তিনি- বলা হয়েছে ব্যবসা-বাণিজ্যবিষয়ক মার্কিন সাইট বিজনেস ইনসাইডার-এর প্রতিবেদনে।  

টেসলা সেমি’র মূল্য বলা হয়েছে দেড় লাখ মার্কিন ডলার। যেমনটা ধারণা করা হয়েছিল তার চেয়ে কম এই মূল্য।
একবার পূর্ণ চার্জে মালামাল নিয়ে মহাসড়কে ৩০০ মাইল চলবে টেসলা সেমি। আর ৫০০ মাইল চলবে এমন টেসলা সেমি’র মূল্য বলা হয়েছে ১,৮০,০০০ মার্কিন ডলার-- খবর মার্কিন সংবাদমাধ্যম সিএনবিসি’র।
অনেক বিশ্লেষক যেমনটা ধারণা করেছিলেন তার চেয়ে সস্তা টেসলা সেমি’র এই মূল্য। আর এতে জ্বালানী খরচ বা ডিজেল ট্রাকের মতো মেরামত খরচও দিতে হবে না ট্রাক পরিচালনাকারী প্রতিষ্ঠানগুলোকে।
ইতোমধ্যে ট্রাকটি প্রি-অর্ডার করেছে বেশ কিছু প্রতিষ্ঠান। এর মধ্যে জে. বি. হান্ট, মেইজার এবং ওয়াল-মার্ট-এর মতো প্রতিষ্ঠান রয়েছে। এবার প্রিঅর্ডার মূল্য পাঁচ হাজার মার্কিন ডলার থেকে চারগুণ বাড়িয়ে ২০ হাজার মার্কিন ডলার করেছে টেসলা।
টেসলা সেমি’ নিয়ে এখনো অনেক প্রশ্ন রয়ে গেছে। এখন পর্যন্ত এটির ফিচার তালিকা প্রকাশ করেনি প্রতিষ্ঠানটি।

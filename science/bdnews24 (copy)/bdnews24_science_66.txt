মঙ্গলবার প্রথমবারের মতো মাইক্রোসফট স্টোরে ক্রোম ব্রাউজার যোগ করা হয়। এখানে পুরো অ্যাপের ইনস্টলার ফাইলের পরিবর্তে ব্রাউজারটির ডাউনলোড লিংক দেওয়া হয়েছে শুধু।
এক বিবৃতিতে মাইক্রোসফট-এর এক মুখপাত্র বলেন, “মাইক্রোসফট স্টোর থেকে আমরা গুগল ক্রোম ইনস্টলার অ্যাপ সরিয়ে নিয়েছি কারণ এটি মাইক্রোসফট স্টোরের নীতিমালা ভঙ্গ করেছে।”
ওই মুখপাত্র আরও বলেন, “মাইক্রোসফট স্টোর-এর নীতিমালা মেনে ব্রাউজার অ্যাপ তৈরি করতে আমরা গুগলকে স্বাগত জানাই।”
মাইক্রোসফট-এর পক্ষ থেকে প্রস্তাব দেওয়া হলেও গুগল তা মেনে নেবে না বলে ধারণা করা হচ্ছে। মাইক্রোসফট স্টোরে গুগল ক্রোম না আনার পেছনে অনেক কারণও রয়েছে গুগলের। এর মধ্যে প্রাথমিক কারণ হতে পারে উইন্ডোজ ১০ এস-এর সীমাবদ্ধতা, বলা হয়েছে প্রযুক্তি সাইট ভার্জ-এর প্রতিবেদনে।
মাইক্রোসফট স্টোরের নীতিমালা অনুযায়ী ব্রাউজার অ্যাপগুলোকে অবশ্যই উইন্ডোজ ১০-এর দেওয়া এইচটিএমএল ও জাভাস্ক্রিপ্ট ইঞ্জিন ব্যবহার করতে হবে। কিন্তু গুগল ক্রোম তার নিজের ব্লিঙ্ক রেন্ডারিং ইঞ্জিন ব্যবহার করে।
খুব বেশি সংখ্যক উইন্ডোজ ১০ পিসিতে উইন্ডোজ ১০ এস সংস্করণ চালানো হয় না। তাই এর জন্য আলাদা ক্রোম বানানোর বিষয়টিও চিন্তা করেনি গুগল।

রাবি: রাজশাহী বিশ্ববিদ্যালয়ের (রাবি) প্রাণ রসায়ন ও অনুপ্রাণ বিজ্ঞান বিভাগের উদ্যোগে আর্সেনিক গবেষণা বিষয়ক সিম্পোজিয়াম অনুষ্ঠিত হয়েছে।
বৃহস্পতিবার (০১ ফেব্রুয়ারি) বিজ্ঞান অনুষদের কনফারেন্স রুমে ‘দূরারোগ্য আর্সেনিক ব্যাধির স্বাস্থ্যগত সমস্যা’ শীর্ষক সিম্পোজিয়ামটি সকালে শুরু হয়ে দুপুর পর্যন্ত চলে।
আর্সেনিক বিষয়ে বিশ্ববিদ্যালয়ের প্রাণ রসায়ন ও অনুপ্রাণ বিজ্ঞান বিভাগ ও জাপানের টকুশিমা বুনরি বিশ্ববিদ্যালয়ের মলিক্যুলার নিউট্রিয়েশন অ্যান্ড টক্সিকোলজি বিভাগের যৌথ গবেষণার এক দশক পূর্তি উদযাপন উপলক্ষে এ অনুষ্ঠানের আয়োজন করা হয়।   জাপানের টকুশিমা বুনরি বিশ্ববিদ্যালয়ের অধ্যাপক ড. সেইসিরো হিমনুরের সঙ্গে বিশ্ববিদ্যালয়ের প্রাণ রসায়ন ও অনুপ্রাণ বিজ্ঞান বিভাগের অধ্যাপক ও পরিবেশ স্বাস্থ্য বিজ্ঞান গবেষণাগারের প্রধান গবেষক ড. খালেদ হোসেন আর্সেনিকের স্বাস্থ্য ঝুঁকি নিয়ে যৌথ গবেষণা করে আসছেন।
বিভাগের অধ্যাপক ড. রেজাউল করিমের সভাপতিত্বে অনুষ্ঠানে স্বাগত বক্তব্য রাখেন অধ্যাপক খালেদ হোসেন।
সিম্পোজিয়াম বক্তা হিসেবে বক্তব্য রাখেন জাপানি অধ্যাপক ড. হিমনু। 
এসময় তিনি আর্সেনিক গবেষণায় প্রাপ্ত তথ্য, সাফল্য, মানবদেহে আর্সেনিক বিষক্রিয়ার প্রক্রিয়া ও প্রভাবসহ যৌথ গবেষণার বিভিন্ন দিক তুলে ধরেন।
অনুষ্ঠানে প্রধান অতিথি হিসেবে বক্তব্য রাখেন বিশ্ববিদ্যালয় উপ-উপাচার্য অধ্যাপক ড. আনন্দ কুমার সাহা।
বিশেষ অতিথি হিসেবে বক্তব্য রাখেন বিশ্ববিদ্যালয়ের পদার্থবিজ্ঞান বিভাগের ইমেরিটাস অধ্যাপক ড. অরুণ কুমার বসাক, বিজ্ঞান অনুষদের ডীন অধ্যাপক ড. আখতার ফারুক প্রমুখ।

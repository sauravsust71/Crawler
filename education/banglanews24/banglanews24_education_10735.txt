ঢাকা: স্বীকৃতিপ্রাপ্ত নন-এমপিও শিক্ষা প্রতিষ্ঠান এমপিওভুক্তির দাবিতে লাগাতার কর্মসূচির দ্বিতীয় দিন বুধবার (২৭ ডিসেম্বর) জাতীয় প্রেসক্লাবের সামনে অবস্থান নিয়েছেন এসব শিক্ষা প্রতিষ্ঠানের শিক্ষক-কর্মচারীরা।
নন-এমপিও শিক্ষা প্রতিষ্ঠান শিক্ষক-কর্মচারী ফেডারেশন আয়োজিত সংগঠনটির ভারপ্রাপ্ত সভাপতি অধ্যক্ষ গোলাম মাহমুদুন্নবী ডলার ও সাধারণ সম্পাদক বিনয় ভূষণ রায়ের নেতৃত্বে এ লাগাতার অবস্থান কর্মসূচিতে কয়েক হাজার শিক্ষক-কর্মচারী প্রেসক্লাবের সামনে অবস্থান করছেন।
নন-এমপিও শিক্ষা প্রতিষ্ঠান শিক্ষক-কর্মচারী ফেডারেশনের নেতারা বলেন, দাবি আদায় না হওয়া পর্যন্ত এ লাগাতার অবস্থান কর্মসূচি চলবে। আমরা রাজপথ ছাড়বো না।
ফেডারেশন থেকে জানানো হয়, দেশে স্বীকৃতিপ্রাপ্ত পাঁচ হাজারের মতো নন-এমপিও শিক্ষা প্রতিষ্ঠান রয়েছে। যাতে শিক্ষক-কর্মচারীর সংখ্যা এক লাখের বেশি। এসব শিক্ষা প্রতিষ্ঠানে ২৫ লাখের বেশি শিক্ষার্থী শিক্ষা নিচ্ছে।
সংগঠনের নেতারা বলেন, এসব শিক্ষক-কর্মচারী বিনা বেতনে শিক্ষাদানের মহান পেশায় নিয়োজিত। অনেকে ১৫-২০ বছর বিনা বেতনে কাজ করে যাচ্ছেন। এমপিওভুক্তি না হলে আমাদের অর্থনৈতিক মুক্তি আসবে না।

বেরোবি (রংপুর): বেগম রোকেয়া বিশ্ববিদ্যালয় (বেরোবি) অফিসার্স অ্যাসোসিয়েশন নির্বাচনে এটিজিএম গোলাম ফিরোজ সভাপতি ও মো. মোর্শেদ উল আলম রনি সাধারণ সম্পাদক নির্বাচিত হয়েছেন।
রোববার (৩১ ডিসেম্বর) সকাল ১০টা থেকে ৩ টা পর্যন্ত বিশ্ববিদ্যালয়ের প্রশাসনিক ভবনে অনুষ্ঠিত নির্বাচনের ফলাফল রাতে প্রকাশ করা হয়।
এতে বিশ্ববিদ্যালয়ের ডেপুটি রেজিস্ট্রার মো. জাহিদুর রহমান প্রধান নির্বাচন কমিশনার হিসেবে নির্বাচন পরিচালনা করেন।
নির্বাচিত অন্য কর্মকর্তারা হলেন- সহ-সভাপতি মো. রেজাউল করিম শাহ্, কোষাধ্যক্ষ মো. আহসানুল কবীর, যুগ্ম সম্পাদক মোছা. মাহাবুবা আক্তার, দফতর ও প্রচার সম্পাদক মো. মাসুদার রহমান, ক্রিয়া ও সাংস্কৃতিক সম্পাদক মো. আল- আমিন, সমাজকল্যাণ সম্পাদক মো. ওবায়েদুর রহমান, সদস্য তাপস কুমার গোস্বামী, মো. ফিরোজুল ইসলাম, মো. জিয়াউল হক, এ কে এম রাহিমুল ইসলাম, এস এম আব্দুর রহিম।

নারায়ণগঞ্জের প্রধান শিক্ষক শ্যামল কান্তি ভক্তকে লাঞ্ছনার ঘটনায় মাধ্যমিক ও উচ্চ শিক্ষা অধিদফতর (মাউশি) গঠিত তদন্ত কমিটি প্রতিবেদন জমা দিয়েছে বলে জানিয়েছেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ।  
ঢাকা: নারায়ণগঞ্জের প্রধান শিক্ষক শ্যামল কান্তি ভক্তকে লাঞ্ছনার ঘটনায় মাধ্যমিক ও উচ্চ শিক্ষা অধিদফতর (মাউশি) গঠিত তদন্ত কমিটি প্রতিবেদন জমা দিয়েছে বলে জানিয়েছেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ।  
বুধবার (১৮ মে) সন্ধ্যায় শিক্ষামন্ত্রী বাংলানিউজকে বলেন, বিকেলে প্রতিবেদন জমা হয়েছে। বৃহস্পতিবার (১৯ মে) সকালে প্রতিবেদন দেখে জানাবো।
ধর্মীয় অনুভূতিতে আঘাত হানার গুজব ছড়িয়ে গত ১৩ মে নারায়ণগঞ্জের বন্দর উপজেলার পিয়ার সাত্তার লতিফ উচ্চ বিদ্যালয়ের প্রধান শিক্ষক শ্যামল কান্তি ভক্তকে প্রকাশ্যে কান ধরে ওঠ-বস করা হয়।
শিক্ষামন্ত্রীর নির্দেশে গত সোমবার (১৬ মে) দুই সদস্যের একটি তদন্ত কমিটি গঠন করে মাউশি।
শিক্ষামন্ত্রী জানান, কমিটি সরেজমিনে কাজ করে প্রতিবেদন জমা দিয়েছে, কাল জানতে পারবেন। বৃহস্পতিবার সকালে রাজধানীর সেগুনবাগিচায় আন্তর্জাতিক মাতৃভাষা ইন্সটিটিউট মিলনায়তনে এ বিষয়ে জানানো হবে বলেও জানান মন্ত্রী।
দুপুরে সচিবালয়ে এক অনুষ্ঠানে শিক্ষামন্ত্রী সাংবাদিকদের বলেন, প্রতিবেদন পেলে দোষীদের বিরুদ্ধে ‘পোক্ত’ ব্যবস্থা নেওয়া হবে। তিনি প্রতিবেদনের জন্য অপেক্ষায় রয়েছেন।
তদন্ত কমিটির সদস্য মাউশির ঢাকা অঞ্চলের পরিচালক (সেসিপ) এবং নারায়ণগঞ্জ জেলা শিক্ষা কর্মকর্তাকে তিন কর্মদিবসের মধ্যে প্রতিবেদন দাখিল করতে বলা হয়েছিল।

ঢাকা: জাতীয় বিশ্ববিদ্যালয়ের ২০১৭-২০১৮ শিক্ষাবর্ষে প্রথম বর্ষ স্নাতক (সম্মান) ভর্তি কার্যক্রমে বিষয়ভিত্তিক প্রথম মেধা তালিকা (০২ অক্টোবর) প্রকাশ করা হবে।
বৃহস্পতিবার (২৮ সেপ্টেম্বর) বিকেলে বিশ্ববিদ্যালয়ে থেকে পাঠানো এক সংবাদ বিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
সংবাদ বিজ্ঞপ্তিতে জানানো হয়, ভর্তির ফলাফল SMS এর মাধ্যমে বিকেল ৪টা থেকে যেকোনো মোবাইল মেসেজ অপশনে গিয়ে- (nu<space>athn<space>roll no.  লিখে ১৬২২২ নম্বরে মেসেজ Send করলে ফল জানা যাবে এবং রাত ৯টায় ওয়েব সাইট (www.nu.edu.bd/admissions অথবা admissions.nu.edu.bd) থেকে ফল পাওয়া যাবে।
এ শিক্ষাবর্ষে ভর্তিচ্ছুকদের ক্লাস ১৫ অক্টোবর থেকে শুরু হবে।

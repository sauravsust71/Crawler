ইউনিভার্সিটি করেসপন্ডেন্ট: জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের (জাবি) রেজিস্ট্রার ভবন বোমা মেরে উড়িয়ে দেওয়ার হুমকি দিয়ে বিশ্ববিদ্যালয় উপাচার্য  অধ্যাপক ফারজানা ইসলামকে বেনামি একটি চিঠি দেওয়া হয়েছে।
শুক্রবার (১০ নভেম্বর) বিশ্ববিদ্যালয় রেজিস্ট্রার আবু বকর সিদ্দিক বাংলানিউজকে এ তথ্য নিশ্চিত করেন।  এ বিষয়ে আশুলিয়া থানায় একটি সাধারণ ডায়েরি (জিডি) করা হয়েছে বলেও জানান তিনি। 
বৃহস্পতিবার (০৯ নভেম্বর) বিকেলে চিঠিটি রেজিস্ট্রার দফতরে আসে বলে জানানো হয়। 
আশুলিয়া থানার ভারপ্রাপ্ত কর্মকর্তা (ওসি)আব্দুল আওয়ালের কাছে জিডি সম্পর্কে জানতে চাইলে তিনি বলেন- বৃহস্পতিবার সন্ধ্যায় বিশ্ববিদ্যালয়ের ভিসি অধ্যাপক ফারজানা ইসলাম  এ বিষয়ে একটি সাধারণ ডায়েরি (জিডি) করেছেন।    
ভিসি অধ্যাপক ফারজানা ইসলাম বলেন, আমরা আতঙ্কিত নই। তারপরও দায়িত্বের জায়গা থেকে পুলিশকে জানিয়েছি। এখন অনেক কিছুই হতে থাকবে। সামনে যেহেতু সিনেট নির্বাচন রয়েছে। 
তিনি জানান, চিঠিতে লেখা হয়েছে, ৫ থেকে ৯ নভেম্বরের মধ্যে বিশ্ববিদ্যালয় প্রশাসনে রদবদল না করলে টাইম বোমা মেরে রেজিস্ট্রার ভবন উড়িয়ে দেওয়া হবে। রেজিস্ট্রার ভবন পুরাতন ও নতুন দুই ভবনে দুটি টাইম বোমা সেট করে রাখা হয়েছে, তাও চিঠিতে উল্লেখ রয়েছে। 
চিঠি পাওয়ার পরপরই আমরা পুলিশকে বিষয়টি জানিয়েছি। থানায় জিডি করেছি। পুলিশ ভবন দু’টি তল্লাশি করার কথা জানিয়েছে।

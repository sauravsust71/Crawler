জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের সেন্টার অব এক্সিলেন্স ইন টিচিং অ্যান্ড লার্নিং (সিইটিএল-জেইউ) আয়োজিত তিন দিনব্যাপী শিক্ষক প্রশিক্ষণ কর্মশালা শুরু হয়েছে।
মঙ্গলবার (৩ জানুয়ারি) সকালে বিশ্ববিদ্যালয়ের জহির রায়হান মিলনায়তনে এ প্রশিক্ষণ কর্মশালার উদ্বোধন করেন উপাচার্য অধ্যাপক ফারজানা ইসলাম।
উদ্বোধনী বক্তব্যে তিনি বলেন, পেশাগত সম্মান ও মর্যাদা বৃদ্ধির জন্য প্রশিক্ষণ গ্রহণ আবশ্যক। শিক্ষকের শেখার ইচ্ছে থাকতে হবে। শেখার ইচ্ছাই শিক্ষককে জাগ্রত করে রাখে।
প্রশিক্ষণ কর্মশালার মাধ্যমে শিক্ষকদের গুণগত মান বাড়বে বলে মন্তব্য করেন তিনি।
সিইটিএল-জেইউ’র পরিচালক অধ্যাপক শেখ মনজুরুল হকের সভাপতিত্বে অনুষ্ঠানে বিশেষ অতিথি হিসেবে বক্তব্য রাখেন উপ-উপাচার্য অধ্যাপক মো. আবুল হোসেন, ইটিএল এর উপ-পরিচালক অধ্যাপক সৈয়দ হাফিজুর রহমানসহ প্রমুখ।
কর্মশালার প্রথম দিনে প্রশিক্ষণ দেন জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের ভূগোল ও পরিবেশ বিভাগের অবসরপ্রাপ্ত শিক্ষক অধ্যাপক দারা শামসুদ্দিন।
কর্মশালায় বিশ্ববিদ্যালয়ের বিভিন্ন বিভাগের ১৫ জন তরুণ শিক্ষক অংশ নেন।

নতুন হল নির্মাণ ও পরিত্যক্ত ঢাকা কেন্দ্রীয় কারাগারের জমি জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) কাছে হস্তান্তরের দাবিতে প্রতিষ্ঠানটির আন্দোলনরত শিক্ষার্থীদের দ্বিতীয় দিনের ধর্মঘট চলছে। 
জগন্নাথ বিশ্ববিদ্যালয়ের (জবি): নতুন হল নির্মাণ ও পরিত্যক্ত ঢাকা কেন্দ্রীয় কারাগারের জমি জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) কাছে হস্তান্তরের দাবিতে প্রতিষ্ঠানটির আন্দোলনরত শিক্ষার্থীদের দ্বিতীয় দিনের ধর্মঘট চলছে। 
পূর্বঘোষিত কর্মসূচি অনুযায়ী রোববার (২১ আগস্ট) সকাল ৮টা থেকে বিশ্ববিদ্যালয়ের প্রধান ফটকে তালা ঝু‍লিয়ে দেন আন্দোলনরত শিক্ষার্থীরা। এর ফলে কার্যত অচল হয়ে পড়ে বিশ্ববিদ্যালয়ের সার্বিক কার্যক্রম। 
এ কারণে ক্যাম্পাসের ভেতরে ঢুকতে পারছে না শিক্ষক-শিক্ষার্থীদের পরিবহনকারী কোনো বাস বা পরিবহন। সকাল থেকে হচ্ছে না কোনো বিভাগের ক্লাস-পরীক্ষাও।
তবে ২১ আগস্ট গ্রেনেড হামলায় নিহতদের স্মরণে আন্দোলন সকাল থেকেই কিছুটা শিথিল ছিল। এরমধ্যে বৃষ্টি শুরু হলে আন্দোলনকারীরা প্রধান ফটক খুলে দিয়ে সব ভবন ও বিভাগের মূল ফটকে তালা ঝুলিয়ে দেন।
দুপুর ১২টার কিছুক্ষণ আগে ক্যাম্পাসে প্রবেশ করেন উপাচার্য (ভিসি) অধ্যাপক ড. মীজানুর রহমান ও ট্রেজারার। সেসময় নতুন একাডেমিক ভবনের নিচে অবস্থান নিয়ে স্লোগান দিচ্ছিলেন আন্দোলনরত শিক্ষার্থীরা।
বিশৃঙ্খলা এড়াতে সকাল থেকেই ক্যাম্পাসে বিপুলসংখ্যক পুলিশ মোতায়েন রয়েছে।
এর আগে, গত বৃহস্পতিবার (১৮ আগস্ট) ক্যাম্পাসে টানা পাঁচ ঘণ্টা ধর্মঘট পালন শেষে আন্দোলনরত শিক্ষার্থীরা জানান, রোববারের মধ্যে সরকার বা সংশ্লিষ্ট কর্তৃপক্ষের তরফ থেকে কোনো সুস্পষ্ট বক্তব্য না এলে সোমবার (২২ আগস্ট) প্রধানমন্ত্রীর কার্যালয় অভিমুখে মিছিল করবে শিক্ষার্থীরা।
কলেজ থেকে ২০০৫ সালে যাত্রা শুরু করা জগন্নাথ বিশ্ববিদ্যালয়ের ১১টি হল ছিল প্রভাবশালীদের দখলে।
বিভিন্ন সময়ে সেসব হল উদ্ধারের দাবি উঠলেও ২০০৯ সালে শিক্ষার্থীদের জোরালো আন্দোলনে সরকারের উচ্চ মহলের টনক নড়ে। ওই সময় একাধিক হল উদ্ধার করে বিশ্ববিদ্যালয়কে দিতে ভূমি মন্ত্রণালয়ের সুপারিশ করে। কিন্তু তা কার্যকর করেনি ঢাকা জেলা প্রশাসন।
এরপর ২০১১ ও ২০১৪ সালে আবারও জোরালো আন্দোলন হলে দু’টি হল পুনরুদ্ধার হয়। কিন্তু তা এখনও ব্যবহার উপযোগী করতে পারেনি বিশ্ববিদ্যালয় কর্তৃপক্ষ। আবার আরেকটি হল আগে থেকেই বিশ্ববিদ্যালয়ের নিয়ন্ত্রণে থাকলেও তা নিয়ে দেখা যায়নি কোনো পরিকল্পনা। দীর্ঘসূত্রিতার অভিযোগ রয়েছে নতুন দু’টি হল নির্মাণের উদ্যোগেও।

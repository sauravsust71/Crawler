ঢাকা: বাংলাদেশে প্রথমবারের মতো অনুষ্ঠিত হতে যাচ্ছে ইংলিশ অলিম্পিয়াড। ইতোমধ্যে ১০ হাজারের মতো শিক্ষার্থী অগ্রিম রেজিস্ট্রেশন সম্পন্ন করেছেন।
সোমবার ( ০৩ এপ্রিল) সন্ধ্যায় এ ব্যাপারে ডেইলি সান এবং ওয়ার্ল্ড অরফান সেন্টারের (ডব্লিউওসি) মধ্যে এক সমঝোতা স্মারক স্বাক্ষর হয়েছে। 
ডেইলি সানের ভারপ্রাপ্ত সম্পাদক শিয়াবুর রহমান ও ডব্লিউওসির প্রতিষ্ঠাতা মো. আমানুল্লাহ ডেইলি সান অফিসে এ সমঝোতা স্মারকে স্বাক্ষর করেন।
শিক্ষার্থীরা ইংলিশ অলিম্পিয়াডে অংশগ্রহণের জন্য অনলাইনে রেজিস্ট্রেশন করতে পারবে। ডেইলি সানের ওয়েবসাইটে ইংলিশ অলিম্পিয়াডের ফেসবুক পেজেও রেজিস্ট্রেশন ফরম পাওয়া যাবে। আগামী ২০ মে পর্যন্ত রেজিস্ট্রেশন চলবে।
সমঝোতা স্মারক স্বাক্ষর অনুষ্ঠানে ডব্লিউওসির প্রতিষ্ঠাতা মো. আমানুল্লাহ বলেন, এই ইংলিশ অলিম্পিয়াডের মাধ্যমে এমন দক্ষ নাগরিক তৈরি করা হবে যারা ‌এক সময় বহু আন্তর্জাতিক সংগঠনের নেতৃত্ব দেবে এবং আমাদের দেশের প্রতিনিধিত্ব করবে।
এছাড়া দ্বিতীয় ভাষা হিসেবে ইংরেজি চর্চা খুবই জরুরি এবং উচ্চ শিক্ষার জন্য ইংরেজি ব্যাপক ভূমিকা পালন করে থাকে।
এ সময় উপস্থিত ছিলেন ডেইলি সানের বিপণন ও বিক্রয় বিভাগের নির্বাহী পরিচালক জেডএম আহমেদ প্রিন্স।

যশোর: বাংলাদেশ বিশ্ববিদ্যালয় মঞ্জুরি কমিশনের (ইউজিসি) চেয়ারম্যান প্রফেসর আবদুল মান্নানের মা ছালেমা খাতুনের (৯৩) মৃত্যুতে গভীর শোক প্রকাশ করেছেন যশোর বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ের (যবিপ্রবি) উপাচার্য প্রফেসর মো. আনোয়ার হোসেন।
বৃহস্পতিবার (২৩ নভেম্বর) সন্ধ্যায় একবার্তায় যবিপ্রবি উপাচার্য এ শোক প্রকাশ করেন।
শোকবার্তায় যবিপ্রবি উপাচার্য মরহুমার বিদেহী আত্মার মাগফিরাত কামনা করে শোকসন্তপ্ত পরিবারের প্রতি সমবেদনা জ্ঞাপন করেন।

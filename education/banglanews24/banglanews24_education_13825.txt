ইবি: কনজুমার ইয়ুথ বাংলাদেশ (সিওয়াইবি)-এর ইসলামী বিশ্ববিদ্যালয় (ইবি) শাখার ১ম কার্যনির্বাহী কমিটি অনুমোদন দিয়েছে কেন্দ্রীয় কমিটি। 
কমিটিতে আরবি ভাষা ও সাহিত্য বিভাগের ইমরান শুভ্রকে সভাপতি এবং হিসাব বিজ্ঞান বিভাগের ওসমান গনীকে সাধারণ সম্পাদক পদে মনোনীত করা হয়েছে। 
কনজুমার ইয়ুথ বাংলাদেশ (সিওয়াইবি)-এর প্রচার সম্পাদক জয়ন্ত কৃষ্ণ জয় স্বাক্ষরিত এক প্রেস বার্তায় এ কমিটির অনুমোদন দেওয়া হয়। আগামী এক বছরের জন্য এ কমিটি গঠন করা হয়েছে বলে প্রেস বার্তায় উল্লেখ আছে।
মঙ্গলবার (১৮ জুলাই) বিকেল সাড়ে ৪টার দিকে বিশ্ববিদ্যালয়ের বীরশ্রেষ্ট হামিদুর রহমান মিলনায়তনে আনুষ্ঠানিকভাবে ২৩ সদস্যের এ কমিটি ঘোষণা করে ইংরেজি বিভাগের শিক্ষক এবং ইবি শাখা কনজুমার ইয়ুথের উপদেষ্টা অধ্যাপক ড. মেহের আলী।    এসময় আরও উপস্থিত ছিলেন কেন্দ্রীয় কমিটির সদস্য মোস্তফা যুবাইর আলম, ইবি প্রেস ক্লাবের দফতর বিষয়ক সম্পাদক ইকবাল হোসাইন রুদ্র।

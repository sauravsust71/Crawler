সারা দেশে প্রাথমিক সমাপনী (পিইসি) ও ইবতেদায়ী পরীক্ষার ফল প্রকাশ হয়েছে। পিইসি পরীক্ষায় এবার রাজশাহী জেলায় পাসের হার ৯৮ দশমিক ৩৬ শতাংশ এবং জিপিএ-৫ পেয়েছে চার হাজার ৩০১ জন পরীক্ষার্থী।
রাজশাহী: সারা দেশে প্রাথমিক সমাপনী (পিইসি) ও ইবতেদায়ী পরীক্ষার ফল প্রকাশ হয়েছে। পিইসি পরীক্ষায় এবার রাজশাহী জেলায় পাসের হার ৯৮ দশমিক ৩৬ শতাংশ এবং জিপিএ-৫ পেয়েছে চার হাজার ৩০১ জন পরীক্ষার্থী।
জিপিএ-৫ বাড়লেও রাজশাহীতে গতবারের চেয়ে এবার পাসের হার কমেছে। গতবার জেলায় পিইসিতে পাসের হার ছিল ৯৯ শতাংশ। যা ছিল রেকর্ড। তবে এবার জিপিএ-৫ প্রাপ্তির সংখ্যা বেড়েছে। গতবার মোট জিপিএ-৫ পেয়েছিল চার হাজার ২৬৮ জন। এবার জিপিএ-৫ পেয়েছে চার হাজার ৩০১ জন। ফলে জিপিএ-৫ প্রাপ্ত শিক্ষার্থীর সংখ্যা বেড়ে দাঁড়িয়েছে ৩৩ জন।
রাজশাহীর সহকারী জেলা প্রাথমিক শিক্ষা অফিসার সাইফুল ইসলাম বৃহস্পতিবার (২৯ ডিসেম্বর) এ তথ্য জানিয়েছেন।
এদিকে, রাজশাহী সহকারি জেলা প্রাথমিক শিক্ষা কার্যালয় সূত্রে পাওয়া ফলাফল থেকে জানা গেছে জেলায় এবার ৪৭ হাজার ৪৩ শিক্ষার্থী পরীক্ষায় অংশ নিয়েছিল। এতে পাস করেছে ৪৫ হাজার ৯০৭ জন।
পাসের হার ৯৮ দশমিক ৩৬ শতাংশ। মেয়েদের পাসের হার ৯৮ দশমিক ৩৬ শতাংশ ও ছেলেদের ৯৭ দশমিক ৮৪ শতাংশ।
পিইসিতে ছেলেদের চেয়ে এবার মেয়েরা ভালো ফল করেছে।

ময়মনসিংহের ত্রিশালে প্রতিষ্ঠিত জাতীয় কবি কাজী নজরুল ইসলাম বিশ্ববিদ্যালয়ের শিক্ষক-কর্মকর্তাদের আবাসনের জন্য নবনির্মিত ডরমিটরি ভবনের উদ্বোধন হয়েছে।
ময়মনসিংহ: ময়মনসিংহের ত্রিশালে প্রতিষ্ঠিত জাতীয় কবি কাজী নজরুল ইসলাম বিশ্ববিদ্যালয়ের শিক্ষক-কর্মকর্তাদের আবাসনের জন্য নবনির্মিত ডরমিটরি ভবনের উদ্বোধন হয়েছে।
বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ড. মোহীত উল আলম এ ডরমিটরির উদ্বোধন করেন।
বুধবার (০৫ অক্টোবর) দুপুরে বিশ্ববিদ্যালয়ের উপাচার্য দপ্তরের উপ-পরিচালক (জনসংযোগ) এস এম হাফিজুর রহমান স্বাক্ষরিত এক প্রেস বিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
প্রেস বিজ্ঞপ্তিতে বলা হয়, নবনির্মিত ৫তলা বিশিষ্ট এ ডরমিটরি ভবনে ৬০ জন শিক্ষক-কর্মকর্তাকে আবাসন সুবিধা দেওয়া হয়েছে। এর আগে ২০১৪ সালের ৯ নভেম্বর নবনির্মিত এ ডরমিটরি ভবনের ভিত্তি প্রস্তর স্থাপন করেছিলেন বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ড. মোহীত উল আলম।
এদিকে, গত ৪ অক্টোবর উপাচার্যের সভাপতিত্বে তার অফিস কক্ষে ১৯তম একাডেমিক কাউন্সিলের সভায় নবনির্মিত এবং নির্মাণাধীন বিভিন্ন ভবনের নামকরণ করা হয়।
নবনির্মিত উপাচার্যের বাংলোকে দুখু মিয়া বাংলো, নবনির্মিত শিক্ষক-কর্মকর্তা ডরমিটরিকে ব্রহ্মপুত্র নিকেতন, নির্মাণাধীন ১০তলা বিশিষ্ট ছাত্র হলের নাম জাতির জনক বঙ্গবন্ধু শেখ মুজিবুর রহমান হল এবং নির্মাণাধীন ১০তলা বিশিষ্ট ছাত্রী হলের নাম বঙ্গমাতা শেখ ফজিলাতুন্নেসা মুজিব হল নির্ধারণ করা হয়।
বিশ্ববিদ্যালয়ের পরবর্তী সিন্ডিকেট সভার অনুমোদনক্রমে এসব ভবনের নির্ধারিত নামগুলো চূড়ান্ত করা হবে বলে প্রেস বিজ্ঞপ্তিতে উল্লেখ করা হয়।

জবি: জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) ভুগোল ও পরিবেশ বিদ্যা বিভাগের ২০১৬-২০১৭ শিক্ষাবর্ষের শিক্ষার্থী হেলাল উদ্দিন আবিদের বিরুদ্ধে করা মামলা প্রত্যাহারের দাবিতে ও তাকে হয়রানির প্রতিবাদে মানববন্ধন করেছে সাধারণ শিক্ষার্থীরা।
সোমবার (৭ আগস্ট) দুপুরে বিশ্ববিদ্যালয়ের শহীদ মিনারের সামনে এ মানববন্ধন কর্মসূচির আয়োজন করা হয়। 
গত শনিবার আবিদকে বালিয়াকান্দি থেকে আটক করে স্থানীয় থানার পুলিশ। জগন্নাথ বিশ্ববিদ্যালয় ছাত্রলীগ কর্মী আবিদের গ্রামের বাড়ি বালিয়াকান্দির নবাবপুর গ্রামে।
মানববন্ধনে অংশ নেয়া জবির ভুগোল ও পরিবেশ বিদ্যা বিভাগের শিক্ষার্থী রমিজ বলেন, আবিদ জগন্নাথ বিশ্ববিদ্যালয় ছাত্রলীগের একজন সক্রিয় কর্মী। স্থানীয় রাজনীতিতে সে ঢাকা বিশ্ববিদ্যালয় শাখা ছাত্রলীগের সাবেক সভাপতি শেখ সোহেল রানা টিপুর অনুসারী।
তিনি অভিযোগ করে বলেন, শেখ সোহেল রানার টিপুর ফেসবুকের ছবিতে লাইক কমেন্ট শেয়ার করার কারণে স্থানীয় সংসদ সদস্য জিল্লুল হাকিমের মদদে হেলাল উদ্দিন আবিদকে আটক করে পুলিশ। কিন্তু কোন মামলায় আবিদকে গ্রেফতার করা হয়েছে তা জানায়নি পুলিশ। 
উল্লেখ্য, বালিয়াকান্দির বতর্মান এমপি জিল্লুল হাকিম। আগামী নির্বাচনে ঢাবি ছাত্রলীগের সাবেক সভাপতি সোহেল রানা টিপুও বালিয়াকান্দি থেকে মনোনয়ন প্রত্যাশী। এ নিয়ে স্থানীয় রাজনীতিতে উভয় পক্ষের মধ্যে শীতল সম্পর্ক বিরাজ করছে।
এ ব্যাপারে বালিয়াকান্দি থানার ভারপ্রাপ্ত কর্মকর্তা হাসিনা বেগম বাংলানিউজকে বলেন, গত পরশু (শনিবার) সন্ধ্যায় মারামারির প্রস্তুতিকালে হেলাল উদ্দিন আবিদকে আমরা আটক করে রোববার কোর্টে প্রেরণ করি। 
আবিদের নামে কোন মামলা ছিল কি না এমন প্রশ্নের জবাবে তিনি বলেন, তার নামে কোন মামলা ছিল না। আমরা তাকে মারামারির প্রাক্কালে আটক করে কোর্টে প্রেরণ করি। 

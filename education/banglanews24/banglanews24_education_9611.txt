প্রাথমিক শিক্ষা সমাপনী পরীক্ষায় ময়মনসিংহে পাসের হার ৯৭ দশমিক ৬৫ শতাংশ। আর ইবতেদায়িতে পাসের হার ৯৪ দশমিক ০৮ শতাংশ।
ময়মনসিংহ: প্রাথমিক শিক্ষা সমাপনী পরীক্ষায় ময়মনসিংহে পাসের হার ৯৭ দশমিক ৬৫ শতাংশ। আর ইবতেদায়িতে পাসের হার ৯৪ দশমিক ০৮ শতাংশ।
বৃহস্পতিবার (২৯ ডিসেম্বর) দুপুরে জেলা প্রাথমিক শিক্ষা কর্মকর্তা শফিউল হক বাংলানিউজকে এ তথ্য জানান।
তিনি বলেন, প্রাথমিক শিক্ষা সমাপনী পরীক্ষায় ময়মনসিংহে মোট পরীক্ষার্থী ছিল এক লাখ ছয় হাজার ৫৮ জন। পরীক্ষায় অংশ নিয়েছে এক লাখ ৮শ ১৫ জন শিক্ষার্থী। এর মধ্যে পাস করেছে ৯৮ হাজার ৪শ ৫৩ শিক্ষার্থী। জিপিএ-৫ পেয়েছে সাত হাজার ২শ ৪২ জন শিক্ষার্থী।
ইবতেদায়িতে মোট পরীক্ষার্থী ছিলো সাত হাজার ৫শ ৪০ জন। পাস করেছে সাত হাজার ১শ ৩৭ জন। জিপিএ-৫ পেয়েছে ৬৩ জন শিক্ষার্থী।

১০ দফা দাবি আদায়ে বরিশালে ইনস্টিটিউট অব হেলথ টেকনোলজির (আইএইচটি) শিক্ষার্থীরা মানববন্ধন করেছেন।
বরিশাল: ১০ দফা দাবি আদায়ে বরিশালে ইনস্টিটিউট অব হেলথ টেকনোলজির (আইএইচটি) শিক্ষার্থীরা মানববন্ধন করেছেন।
শনিবার (১৪ মে) বেলা ১১টায় নগরের অশ্বিনী কুমার হলের সামনে সদর রোডে এ মানববন্ধন করেন তারা।
এতে বক্তব্য রাখেন মো. আরিফুর রহমান, সঞ্জিব মজুমদার, ওবায়দুর রহমান, সোয়াইব হোসেন, সৌরভ, শেখর, মিল্টন, ও মো. হোসেন প্রমুখ।
মানববন্ধন থেকে রোববার জেলা সিভিল সার্জনের মাধ্যমে স্বাস্থ্যমন্ত্রী বরাবর স্বারকলিপি প্রদান, ১৬ মে সকাল ১০টায় শের-ই-বাংলা মেডিকেল কলেজ হাসপাতালের সামনে  অবস্থান কর্মসূচির ঘোষণা দেওয়া হয়। গত বৃহস্পতিবার (১২ মে) থেকে ১০ দফা দাবিতে ক্লাস ও পরীক্ষা বর্জন করে ধর্মঘট পালন করছেন আইএইচটির শিক্ষার্থীরা।

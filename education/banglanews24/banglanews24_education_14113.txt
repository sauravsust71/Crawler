কুষ্টিয়া: কুষ্টিয়ার ইসলামী বিশ্ববিদ্যালয়ের (ইবি) সাদ্দাম হোসেন হলে তল্লাশি চালিয়ে ৫টি পেট্রোল বোমা, ১১টি ককটেল, রড, চাপাতি, হকিস্টিক ও দেশীয় উদ্ধার করা হয়েছে। এসময় শিবির সন্দেহে আবু হাসনাত ও গোলম আজম নামে দুই শিক্ষার্থীকে আটক করা হয়।
রোববার (১৩ আগস্ট) বিকেল সাড়ে ৩টা থেকে রাত ৯টা পর্যন্ত হলে তল্লাশি চালায় বিশ্ববিদ্যালয় প্রশাসন, হল প্রশাসন ও পুলিশ।
কুষ্টিয়ার পুলিশ সুপার এসএম মেহেদী হাসান বাংলানিউজকে জানান, বিকেলে সাদ্দাম হোসেন হল প্রশাসনের সহায়তায় পুলিশ অস্ত্রবহনের অভিযোগের ভিত্তিতে হলের ৪২৩ নম্বর কক্ষে তল্লাশি চালায়। এসময় ওই কক্ষ থেকে দু'টি রড, হকস্টিক ও চারটি ২৩২ লেখা ক্রিকেট স্ট্যাম্প উদ্ধার করা হয়।
পরে রাতে ইবি প্রশাসন, পুলিশ ও ডিবির সমন্বয়ে সাদ্দাম হোসেন হলের সব রুমে তল্লাশি চালানো হয়। এসময় সাদ্দাম হোসেন হল থেকে ৫টি পেট্রোল বোমা, ১১টি ককটেল, রড, চাপাতি, হকিস্টিক ও দেশীয় অস্ত্র উদ্ধার করা হয়।
এসময় শিবির সন্দেহে হাসনাত ও আজম নামে দুই শিক্ষার্থীকে আটক করা হয়। বর্তমানে ওই দুই শিক্ষার্থী পুলিশি হেফাজতে রয়েছে।

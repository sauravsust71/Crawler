জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি) সায়েন্স ক্লাবের আয়োজনে শুরু হচ্ছে দুই দিনব্যাপী বিজ্ঞান মেলা।
বৃহস্পতিবার (৮ ফেব্রুয়ারি) বিকাল ৫টায় বিশ্ববিদ্যালয়ের সাংবাদিক সমিতির কার্যালয়ে অনুষ্ঠিত এক সংবাদ সম্মেলনে এ তথ্য জানান সংগঠনটির সভাপতি শাহরিয়ার কবির সোহাগ।
লিখিত বক্তব্যে তিনি বলেন,  মেলায় বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষার্থীদের ১০টি দল, কলেজ শিক্ষার্থীদের ২০টি দল এবং স্কুল পর্যায়ের শিক্ষার্থীদের ২০টি দল অংশ নেবে।
বিশ্ববিদ্যালয় থেকে অংশগ্রহণকারী শিক্ষার্থীরা পোস্টার প্রেজেন্টেশন, প্রোজেক্ট শোকেসিং এবং কুইজ প্রতিযোগিতায় অংশ নেবে। এছাড়া স্কুল এবং কলেজ পর্যায়ের শিক্ষার্থীরা কুইজ কন্টেস্ট, আইডিয়া কন্টেস্ট, প্রোজেক্ট প্রেজেন্টেশন এবং রোবটিক্স ওয়ার্কশপে অংশ নেবে।
বিজ্ঞান বিষয়ক এসব প্রতিযোগিতায় অংশগ্রহণকারী বিজয়ী শিক্ষার্থীরা পাবে বিশেষ পুরস্কার ও প্রাইজ মানি।
শুক্রবার (৯ ফেব্রুয়ারি) সকাল ৯টায় বিশ্ববিদ্যালয়ের জহির রায়হান মিলনায়তনের সেমিনার কক্ষে প্রধান অতিথি হিসেবে এ মেলার উদ্বোধন করবেন টেকসই উন্নয়ন প্রকল্পের (এসডিজি) সমন্বয়ক আবুল কালাম আজাদ। এ সময় বিশেষ অতিথি হিসেবে উপস্থিত থাকবেন-বিশ্ববিদ্যালয়ের উপ-উপাচার্য অধ্যাপক মো. আমির হোসেন, কোষাধ্যক্ষ অধ্যাপক শেখ মো. মনজুরুল হক।
সংবাদ সম্মেলনে বিজ্ঞান মেলার আহ্বায়ক তাযীম মো. নিয়ামত উল্লাহ্ আখন্দ, সংগঠনটির কোষাধ্যক্ষ মোস্তফা বিন বশির সায়েম, মিডিয়া সম্পাদক তারেক আজিজ শ্রাবন, প্রকাশনা সম্পাদক রাকিবুল ইসলাম প্রমুখ উপস্থিত ছিলেন।

বরিশাল বিশ্ববিদ্যালয়ে (ববি) শুরু হয়েছে আন্তঃবিভাগ ফুটবল প্রতিযোগিতা ২০১৬-১৭।
বরিশাল: বরিশাল বিশ্ববিদ্যালয়ে (ববি) শুরু হয়েছে আন্তঃবিভাগ ফুটবল প্রতিযোগিতা ২০১৬-১৭।
রোববার (২৭ নভেম্বর) দুপুর ২টায় বিশ্ববিদ্যালয়ের কেন্দ্রীয় খেলার মাঠে উপাচার্য প্রফেসর ড. এস এম ইমামুল হক বেলুন উড়িয়ে প্রতিযোগিতার উদ্বোধন করেন।
অনুষ্ঠানে শারীরিক শিক্ষা দপ্তরের পরিচালক মো. তারিকুল হক, গণিত বিভাগের চেয়ারম্যান হেনা রাণী বিশ্বাস, লোক প্রশাসন বিভাগের চেয়ারম্যান তাসনুভা হাবিব জিসান, প্রক্টর মো. শফিউল আলম, বঙ্গবন্ধু হলের প্রভোস্ট আব্দুল কাইয়ুম, ইংরেজি বিভাগের সহকারী অধ্যাপক মো. আরিফ হোসেন প্রমুখ উপস্থিত ছিলেন।
পরে গণিত বিভাগ ও লোক প্রশাসন বিভাগের মধ্যে উদ্বোধনী খেলা অনুষ্ঠিত হয়।

বছর বছর হল ভর্তি ফি বৃদ্ধি বন্ধ ও ২০১৬ সালে বর্ধিত ফি প্রত্যাহারের দাবিতে খুলনা প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ের (কুয়েট) শিক্ষার্থীরা বিক্ষোভ সমাবেশ করেছেন।
খুলনা: বছর বছর হল ভর্তি ফি বৃদ্ধি বন্ধ ও ২০১৬ সালে বর্ধিত ফি প্রত্যাহারের দাবিতে খুলনা প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয়ের (কুয়েট) শিক্ষার্থীরা বিক্ষোভ সমাবেশ করেছেন।   বুধবার (০৩ আগস্ট) বিকেল ৫টায় কুয়েট অডিটোরিয়ামের সামনে এ সমাবেশ অনুষ্ঠিত হয়।
সমাবেশে চতুর্থ বর্ষের শিক্ষার্থী আব্দুল্লাহর সঞ্চালনায় রাশিক, তানভির, মাহির প্রমুখ বক্তব্য রাখেন।
বক্তারা বলেন, শিক্ষা মানুষের মৌলিক অধিকার হওয়া সত্ত্বেও আজ টাকা দিয়ে কিনে নিতে হচ্ছে এবং প্রতি বছর সেই অর্থের পরিমাণও বৃদ্ধি পাচ্ছে। ২০০৬ সালের ১৭০০ টাকা বার্ষিক হল ভর্তি ফি ২০১৬ সালে হল ভেদে ৬০০০ টাকার ওপরে দাঁড়িয়েছে। হল ইউনিয়ন, লিটারেচার অ্যান্ড কালচার, রিলিজিয়ন ইত্যাদি আয়োজন হলে না থাকলেও খাত উল্লেখ করে অতিরিক্ত ফি আদায় করা হচ্ছে।
এই বর্ধিত ফি বেসরকারিকরণ ও বাণিজ্যিকীকরণ উল্লেখ করে তা বন্ধের দাবি জানান তারা। এছাড়া শিক্ষা খাতে রাষ্ট্রীয় অনুদান বৃদ্ধির দাবিও জানান শিক্ষার্থীরা।

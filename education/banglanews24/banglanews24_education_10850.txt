ঝালকাঠি: ঝালকাঠি জেলার মাধ্যমিক ও উচ্চ মাধ্যমিক পর্যায়ের শিক্ষা প্রতিষ্ঠান প্রধানদের সঙ্গে মানসম্মত শিক্ষা উন্নয়ন বিষয়ক এক মতবিনিময় সভা অনুষ্ঠিত হয়েছে।
সোমবার (জুলাই ১০) সকাল ১০টায় জেলা প্রশাসকের সম্মেলন কক্ষে জেলা প্রশাসন এ মতবিনিময় সভার আয়োজন করে। জেলা প্রশাসক মো. হামিদুল হক এতে সভায় সভাপতিত্ব করেন।
মতবিনিময় সভায় অন্যদের মধ্যে বক্তব্য দেন পুলিশ সুপার মো. জোবায়েদুর রহমান, ঝালকাঠি সরকারি মহিলা কলেজের অধ্যক্ষ বিমল চক্রবর্তী, অতিরিক্ত জেলা প্রশাসক খোন্দকার ফরহাদ আহমদ, জেলা শিক্ষা কর্মকর্তা এ কে এম সালেহ উদ্দিন, ঝালকাঠি প্রেসক্লাবের সভাপতি কাজী খলিলুর রহমানসহ বিভিন্ন শিক্ষা প্রতিষ্ঠানের প্রধান ও প্রতিনিধিরা।
বক্তারা এ সময় মাধ্যমিক ও উচ্চ মাধ্যমিক পর্যায়ে শিক্ষা ব্যবস্থা আরও মান সম্পন্ন করার জন্য কিছু সুনির্দিষ্ট পদক্ষেপ  নেওয়ার কথা বলেন।

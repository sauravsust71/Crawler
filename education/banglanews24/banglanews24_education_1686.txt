ঢাকা: জাতীয় বিশ্ববিদ্যালয়ের ২০১৪-১৫ শিক্ষাবর্ষ ১ম পর্ব মাস্টার্স (নিয়মিত ও প্রাইভেট) প্রোগ্রামের শিক্ষার্থীদের রেজিস্ট্রেশন কার্ড অনলাইনে পাওয়া যাচ্ছে। 
জাতীয় বিশ্ববিদ্যালয়ের পক্ষ থেকে এক বিজ্ঞপ্তির মাধ্যমে এ তথ্য জানানো হয়। 
বৃহস্পতিবার (১১ মে) থেকে ৩০ মে পর্যন্ত রেজিস্ট্রেশন কার্ড অনলাইনে পাওয়া যাবে।
সংশ্লিষ্ট কলেজকে বিশ্ববিদ্যালয়ের নির্দিষ্ট ওয়েবসাইট www.nu.edu.bd/admissions/regicard লিংকে গিয়ে college login এ click করে college profile এর Notification Link  থেকে পাওয়া User name  ও  password ব্যবহার করে Download করতে হবে। 
১ম পর্ব মাস্টার্স (প্রাইভেট) প্রোগ্রামে বিষয়ভিত্তিক রেজিস্ট্রেশন কার্ড ১৫ মে থেকে পাওয়া যাবে।
বাংলাদেশ সময়: ১৯৫৫ ঘণ্টা, মে ১১, ২০১৭ আরআর/এএ

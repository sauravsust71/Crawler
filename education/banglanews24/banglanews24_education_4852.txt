জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি) ও বাংলাদেশ মহিলা পরিষদের যৌথ আয়োজনে ‌‘নারীর প্রতি সকল প্রকার বৈষম্য বিলোপ সনদ-সিডও’ শীর্ষক আলোচনা সভা অনুষ্ঠিত হয়েছে।
জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি) ও বাংলাদেশ মহিলা পরিষদের যৌথ আয়োজনে ‌‘নারীর প্রতি সকল প্রকার বৈষম্য বিলোপ সনদ-সিডও’ শীর্ষক আলোচনা সভা অনুষ্ঠিত হয়েছে।
 
বৃহস্পতিবার (২৭ অক্টোবর) জাহাঙ্গীরনগর বিশ্ববিদ্যালয়ের জহির রায়হান মিলনায়তনে এ সভা অনুষ্ঠিত হয়।
এতে প্রধান অতিথির বক্তব্যে বিশ্ববিদ্যালয় উপাচার্য অধ্যাপক ফারজান ইসলাম বলেন, যোগ্যতার ভিত্তিতে নারী-পুরুষের সমতা প্রতিষ্ঠায় সবাইকে আন্তরিক হতে হবে। নারী-পুরুষ নির্বিশেষে সবার অবদানের স্বীকৃতি দেওয়া হলে সমতা প্রতিষ্ঠায় অগ্রগতি আসতে পারে বলে তিনি মনে করেন।
অনুষ্ঠানে প্রধান বক্তা হিসেবে উপস্থিত ছিলেন বাংলাদেশ মহিলা পরিষদের কেন্দ্রীয় কমিটির সভানেত্রী আয়শা খানম। তিনি তার বক্তব্যে সিডও সনদের বিভিন্ন দিক তুলে ধরেন।
ছাত্রকল্যাণ ও পরামর্শদান কেন্দ্রের পরিচালক অধ্যাপক রাশেদা আখতারের সভাপতিত্বে অনুষ্ঠানে বিশেষ অতিথি হিসেবে উপস্থিত ছিলেন বিশ্ববিদ্যালয় কোষাধ্যক্ষ অধ্যাপক আবুল খায়ের।

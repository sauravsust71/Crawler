জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: ‘ছিন্ন পাতার সাজাই তরণী’ স্লোগানে জাহাঙ্গীরনগর বিশ্ববিদ্যালয় (জাবি) গণিত বিভাগের অ্যালামনাই অ্যাসোসিয়েশনের উদ্যোগে চতুর্থ বারের মতো পুনর্মিলনী হতে যাচ্ছে।
রোববার (১৯ নভেম্বর) বিকেল সাড়ে ৪টায় গণিত বিভাগের কনফারেন্স রুমে এক সংবাদ সম্মেলনে এ তথ্য জানান অ্যালামনাই অ্যাসোসিয়েশনের সাধারণ সম্পাদক ও বিভাগের সভাপতি অধ্যাপক মো. শরিফ উদ্দিন।
আগামী ৮ ডিসেম্বর বিভাগের দিনব্যাপী পুনর্মিলনীর আয়োজনের মধ্যে রয়েছে- পুনর্মিলনীর উদ্বোধন, আনন্দ শোভাযাত্রা, স্মৃতিচারণ, আড্ডা, সন্ধ্যায় বিশ্ববিদ্যালয় মুক্তমঞ্চে সাংস্কৃতিক অনুষ্ঠান, বিভাগের সাবেক শিক্ষার্থীদের সম্মাননা প্রদান এবং রাতে কনসার্ট।
পুনর্মিলনীতে অংশ নিতে চাইলে অনলাইনে অথবা সরাসরি বিভাগে রেজিস্ট্রেশন করা যাবে। রেজিস্ট্রেশন সম্পর্কে বিস্তারিত জানা যাবে www.juaam-math.org  ওয়েবসাইটে।
প্রয়োজনে যোগাযোগ: ০১৭৩০০২৩৯৫৫ (বিপ্লব, ২৩তম ব্যাচ), ০১৮৭৮৪৯৯৬৫৯ (অমিত, ৪১তমব্যাচ), ০১৭১৯৩০৭৫৬৫ (রাজু, ৪১তম ব্যাচ)।
সংবাদ সম্মেলনে গণিত বিভাগের অধ্যাপক মুহাম্মদ নজরুল ইসলাম, সহযোগী অধ্যাপক আমিনুর রহমান খান, মোহাম্মদ ওসমান গণি, আবদুল রাশিদ, সহকারী অধ্যাপক মুরশেদা বেগম, মো. সাব্বির আলমসহ বর্তমান শিক্ষার্থীরা উপস্থিত ছিলেন।

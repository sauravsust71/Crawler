জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) ‘সি’ ইউনিটে ২০১৬-১৭ সেশনের ভর্তি পরীক্ষা শুক্রবার (৩০ সেপ্টেম্বর) অনুষ্ঠিত হবে।
জগন্নাথ বিশ্ববিদ্যালয়: জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) ‘সি’ ইউনিটে ২০১৬-১৭ সেশনের ভর্তি পরীক্ষা শুক্রবার (৩০ সেপ্টেম্বর) অনুষ্ঠিত হবে।
 
ওইদিন বিকেল ৩টা থেকে বিকেল ৪টা পর্যন্ত জগন্নাথ বিশ্ববিদ্যালয় ক্যাম্পাসসহ মোট ১২টি কেন্দ্রে একযোগে এ পরীক্ষা অনুষ্ঠিত হবে।
বৃহস্পতিবার (২৯ সেপ্টেম্বর) জবি জনসংযোগ কার্যালয় থেকে পাঠানো এক সংবাদ বিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
এবার ‘সি’ ইউনিটের ৫৬০টি আসনের বিপরীতে ২৭ হাজার ৮শ' ২৫ জন শিক্ষার্থী আবেদন করেছেন। এ হিসেবে প্রতি আসনের বিপরীতে লড়ছেন প্রায় ৫০ শিক্ষার্থী।
জগন্নাথ বিশ্ববিদ্যালয় ক্যাম্পাস ছাড়াও ঢাকা গভ. মুসলিম হাই স্কুল, বাংলাবাজার সরকারি বালিকা উচ্চ বিদ্যালয়, ঢাকা কলেজিয়েট স্কুল, ঢাকা মহানগর মহিলা কলেজ, কে এল জুবিলী স্কুল অ্যান্ড কলেজ, ঢাকা সেন্ট্রাল গালর্স হাই স্কুল, সেন্টাল উইমেন্স কলেজ, ইউনিভার্সিটি ল্যাবরেটরি স্কুল ও কলেজ, নীলক্ষেত উচ্চ বিদ্যালয়, উইলস লিটল ফ্লাওয়ার স্কুল অ্যান্ড কলেজ এবং হোম ইকোনোমিক্স কলেজ কেন্দ্রে এই পরীক্ষা গ্রহন করা হবে।
ভর্তি পরীক্ষায় যেকোনো ধরনের অনিয়ম ও দুর্নীতি ঠেকাতে কঠোর নিরাপত্তা ব্যবস্থা গ্রহণ করার কথা জানিয়েছে জবি প্রশাসন। প্রতিটি কেন্দ্রে শরীর তল্লাশির মাধ্যমে ছাত্র-ছাত্রীদের প্রবেশ করানো হবে।
প্রতিবারের মতো এবারও পরীক্ষার হলে কোনো ধরনের ইলেকট্রনিক ডিভাইস (ক্যালকুলেটর, ঘড়ি, মোবাইল) নিয়ে কেন্দ্রে প্রবেশ করতে দেওয়া হবে না বলে জানানো হয়েছে।
এদিকে পরীক্ষার হলে পরীক্ষার্থীকে অবশ্যই প্রবেশপত্রের দুই কপি প্রিন্ট সঙ্গে আনতে হবে। প্রবেশপত্র ছাড়া কাউকে ভর্তি পরীক্ষায় অংশ নিতে দেওয়া হবে না বলে ওই বিজ্ঞপ্তিতে সতর্ক করেছে বিশ্ববিদ্যালয় প্রশাসন।

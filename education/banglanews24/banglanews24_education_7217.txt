জুনিয়র স্কুল সার্টিফিকেট (জেএসসি) পরীক্ষার দ্বিতীয় দিনে বরিশাল মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষাবোর্ডের অধীনে ১৬২টি কেন্দ্রে ৩ হাজার ৩০৯ জন পরীক্ষার্থী অনুপস্থিত ছিল।
বরিশাল: জুনিয়র স্কুল সার্টিফিকেট (জেএসসি) পরীক্ষার দ্বিতীয় দিনে বরিশাল মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষাবোর্ডের অধীনে ১৬২টি কেন্দ্রে ৩ হাজার ৩০৯ জন পরীক্ষার্থী অনুপস্থিত ছিল।
বুধবার (০২ নভেম্বর) বাংলা দ্বিতীয়পত্র পরীক্ষায় অনুপস্থিত পরীক্ষার্থীদের মধ্যে বরিশাল জেলায় ৯৮৮ জন, ঝালকাঠিতে ৩০৪, পিরোজপুরে ৩৫৯, পটুয়াখালীতে ৫৩৩, বরগুনায় ৩৯৩ ও ভোলা জেলায় ৭৩২ জন রয়েছে।
বরিশাল মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষাবোর্ডের পরীক্ষা নিয়ন্ত্রক প্রফেসর মুহাম্মদ শাহ আলমগীর বাংলানিউজকে এ তথ্য জানান।
এ বছর বরিশাল শিক্ষাবোর্ডের আওতায় ১ লাখ ১৭ হাজার ৪৫৬ জন পরীক্ষার্থী রয়েছে। গত বছর ছিল ১ লাখ ৫ হাজার ৬২০ জন।
চলতি বছরে পরীক্ষার্থীর সংখ্যা বাড়ার পাশাপাশি প্রথম দিনের মতো দ্বিতীয় দিনেও পরীক্ষায় অনুপস্থিতির হার বেড়েছে। যা গত বছরের চেয়ে দশমিক ১৯ ভাগ বেশি। গতবছর বাংলা দ্বিতীয় পরীক্ষায় ২ হাজার ৮৫১ জন পরীক্ষার্থী অনুপস্থিত ছিল।

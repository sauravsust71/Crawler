বরিশাল শিক্ষা বোর্ডের অধীনে ২০১৬ সালের এইচএসসি পরীক্ষায় পাসের হার ৭০.১৩ শতাংশ। বৃহস্পতিবার (১৮ আগস্ট) বেলা ১১টায় বরিশাল শিক্ষা বোর্ডের সভা কক্ষে ফলাফল ঘোষণা করেন বোর্ডের সচিব আব্দুল মোতালেব হাওলাদার।
বরিশাল: বরিশাল শিক্ষা বোর্ডের অধীনে ২০১৬ সালের এইচএসসি পরীক্ষায় পাসের হার ৭০.১৩ শতাংশ।
বৃহস্পতিবার (১৮ আগস্ট) বেলা ১১টায় বরিশাল শিক্ষা বোর্ডের সভা কক্ষে ফলাফল ঘোষণা করেন বোর্ডের সচিব আব্দুল মোতালেব হাওলাদার।
তিনি জানান, এ বছর পাসের হার ৭০.১৩ শতাংশ যা গত বছরে ছিল ৭০.০৬। এ বছর  ৩১৫টি কলেজের ১০৯টি সেন্টারে ৬২ হাজার ৬৭২ জন পরীক্ষার্থীর মধ্যে অংশগ্রহণ করে ৬১ হাজার ৫৩৮ জন। এর মধ্যে পাস করেছে ৪৩ হাজার ১৫৭ জন।
যাদের মধ্যে জিপিএ ৫ পেয়েছে ৭৮৭ জন, গতবছর জিপিএ ৫ এর সংখ্যা ছিলো ১ হাজার ৩১৯ জন।
গত বছরের মতো এ বছরও ছেলেদের চেয়ে মেয়েদের ফলাফল তুলনামূলক ভালো। তবে জিপিএ-৫ এ আনুপাতিক হারে মেয়েদের সংখ্যা কম।
২৯ হাজার ৮৫৯ জন ছাত্রীর মধ্যে পরীক্ষায় অংশগ্রহণ করে ২৯ হাজার ৩৫৭। এর মধ্যে পাস করেছে ২১ হাজার ২৫০ জন, তার মধ্যে জিপিএ ৫ পেয়েছে ৩৭১ জন, আর পাসের হার ৭২.৩৮।
৩২ হাজার ৮১৩ জন ছ‍াত্রের মধ্যে পরীক্ষায় অংশগ্রহণ করে ৩২ হাজার ১৮১ জন। এর মধ্যে ২১ হাজার ৯০৭ জন পাস করেছে যাদের মধ্যে জিপিএ ৫ পেয়েছে ৪১৬ জন, পাসের হার ৬৮.০৭।
বরিশাল ক্যাডেট কলেজসহ ২টি শিক্ষা প্রতিষ্ঠানে শতভাগ পাস করেছে।
ফলাফলের বিষয়ে বোর্ডের সচিব আব্দুল মোতালেব হাওলাদার বলেন, যুক্তিবিদ্যা, অর্থনীতি ও আইসিটি বিষয়ে নতুন করে সৃজনশীল পদ্ধতি সংযোজিত হওয়ায় এবং শিক্ষার্থীদের পড়াশুনার পাশাপাশি অনুসঙ্গ বেড়ে যাওয়ায় এ বছর জিপিএ ৫ এর সংখ্যা কিছুটা কমে গেলেও পাশের হার বেড়েছে।

ঢাকা: ই-নাইন ফোরামের সদস্য দেশগুলো ঢাকা সম্মেলনে নিজেদের দায়িত্ব সুনির্দিষ্ট করে একটি অ্যাকশন প্ল্যানের মাধ্যমে সহযোগিতার ক্ষেত্রসমূহ চিহ্নিত করেছে বলে জানিয়েছেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ।
তিনি বলেন, ই-নাইন সদস্য রাষ্ট্রসমূহের শিক্ষা বিষয়ক লক্ষ্য বাস্তবায়নে ফোরামের সদস্য রাষ্ট্র ও ইউনেস্কোর যৌথ প্রয়াস টেকসই উন্নয়ন লক্ষ্যমাত্রা (এসডিজি) অর্জনে গুরুত্বপূর্ণ ভূমিকা রাখবে।
মঙ্গলবার (০৭ ফেব্রুয়ারি) ঢাকায় হোটেল র‌্যাডিসনে ই-নাইন মন্ত্রী পর্যায়ের সম্মেলনের সমাপনী অনুষ্ঠানে সভাপতির বক্তব্যে একথা বলেন শিক্ষামন্ত্রী।
শিক্ষা মন্ত্রণালয় জানায়, তিন দিনব্যাপী সম্মেলনের শেষ দিনে সর্বসম্মতিক্রমে ই-নাইন চেয়ারম্যান, সদস্য রাষ্ট্র ও ইউনেস্কোর দায়িত্ব ও কর্তব্য সম্পর্কে সুনির্দিষ্ট টার্মস অব রেফারেন্স গৃহীত হয়েছে। এর ফলে সদস্য রাষ্ট্রসমূহ নিজেদের মধ্যে পারস্পরিক অভিজ্ঞতা বিনিময় ও সহযোগিতার মাধ্যমে শিক্ষার উন্নয়নে গুরুত্বপূর্ণ ভূমিকা রাখবে, যা কিনা ২০৩০ সালের মধ্যে এসডিজি-৪ বাস্তবায়নে সহায়ক হবে।
ই-নাইন ফোরামের ঢাকা সম্মেলনে চার পদক্ষেপ গ্রহণের কথা জানিয়ে শিক্ষামন্ত্রী বলেন, ফোরামের সদস্য দেশগুলোর সাধারণ সমস্যা যেমন- বয়স্ক শিক্ষার হার বাড়ানো, মেয়ে শিশুদের স্কুলে নিয়ে আসা, ঝরে পড়া কমানো এবং মানসম্মত শিক্ষা অর্জনে আরও সুনির্দিষ্ট ও ঘনিষ্ঠভাবে কাজ করতে পদক্ষেপ নিতে হবে।
মন্ত্রী বলেন, এ ফোরামের নতুন সভাপতি হিসেবে এর কার্যক্রম এগিয়ে নিতে বাংলাদেশ সর্বোচ্চ চেষ্টা করবে।
সদস্য রাষ্ট্রসগুলোকে ঢাকা সম্মেলনে অংশ নেওয়ায় ধন্যবাদ জানান মন্ত্রী। চেয়ারম্যান হিসেবে দায়িত্ব পালনকালে সব দেশের সহযোগিতাও কামনা করেন তিনি।
এ সময় অন্যাদের মধ্যে বক্তব্য রাখেন- ভারতের মানবসম্পদ উন্নয়ন প্রতিমন্ত্রী, ইউনেস্কো প্রতিনিধি জর্ডান নাইডু এবং অংশ নেওয়া দেশেগুলোর প্রতিনিধিরা।
প্রতিনিধিরা ঢাকা সম্মেলনকে ই-নাইন ফোরামের অন্যতম সফল সম্মেলন বলে উল্লেখ করে সম্মেলন আয়োজনের জন্য বাংলাদেশকে অভিনন্দন জানান। সম্মেলন আয়োজনে সার্বিক সহায়তার জন্য প্রধানমন্ত্রী শেখ হাসিনা ও ইউনেস্কোর মহাপরিচালক ইরিনা বোকোভাকেও ধন্যবাদ জানান তারা।

কেন্দ্রীয় শহীদ মিনারে সব্যসাচী লেখক সৈয়দ শামসুল হকের কফিনে পুষ্পার্ঘ্য অর্পণ  করেছেন জাতীয় বিশ্ববিদ্যালয়ের উপাচার্য ড. হারুন-অর-রশিদ।
ঢাকা: কেন্দ্রীয় শহীদ মিনারে সব্যসাচী লেখক সৈয়দ শামসুল হকের কফিনে পুষ্পার্ঘ্য অর্পণ  করেছেন জাতীয় বিশ্ববিদ্যালয়ের উপাচার্য ড. হারুন-অর-রশিদ।
বুধবার (২৮ সেপ্টেম্বর) বিশ্ববিদ্যালয়ের জনসংযোগ, তথ্য ও পরামর্শ দফতরের ভারপ্রাপ্ত পরিচালক মো. ফয়জুল করিম স্বাক্ষরিত এক বিজ্ঞপ্তিতে এ তথ্য জানানো হয়।
শহীদ মিনারে তাৎক্ষণিক এক প্রতিক্রিয়ায় উপাচার্য বলেন, ‘সৈয়দ শামসুল হক ছিলেন অসাধারণ প্রতিভাবান ও চিন্তা-মনন-কর্মে খাঁটি বাঙালি। তার সৃজনশীল কর্মের মাধ্যমে তিনি বাংলার সমাজ-সংস্কৃতিকে যেভাবে সমৃদ্ধ করে গিয়েছেন, তাতে তার মৃত্যু নেই। বাঙালি জাতি শতাব্দীর পর শতাব্দী ধরে তার সৃজনশীল কর্মের ‘পায়ের আওয়াজ’ শুনতে পাবে। সৈয়দ শামসুল হক চিরঞ্জীব।’
এ সময় উপস্থিত ছিলেন- বিশ্ববিদ্যালয়ের প্রক্টর এইচ এম তায়েহীদ জামাল, ড. মো. নাছির উদ্দিনসহ শিক্ষক ও কর্মকর্তারা।

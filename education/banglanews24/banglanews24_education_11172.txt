শেরেবাংলা কৃষি বিশ্ববিদ্যালয়ের (শেকৃবি) ২০১৬-১৭ শিক্ষাবর্ষে স্নাতক (সম্মান) শ্রেণিতে ভর্তি পরীক্ষার আবেদনের শেষ সময় ২৫ নভেম্বর থেকে বাড়িয়ে ৩০ নভেম্বর রাত ১২টা পর্যন্ত করা হয়েছে।
ঢাকা: শেরেবাংলা কৃষি বিশ্ববিদ্যালয়ের (শেকৃবি) ২০১৬-১৭ শিক্ষাবর্ষে স্নাতক (সম্মান) শ্রেণিতে ভর্তি পরীক্ষার আবেদনের শেষ সময় ২৫ নভেম্বর থেকে বাড়িয়ে ৩০ নভেম্বর রাত ১২টা পর্যন্ত করা হয়েছে।
বুধবার (২৩ নভেম্বর) বিশ্ববিদ্যালয়ের উপাচার্য অধ্যাপক ড. কামাল উদ্দিন আহাম্মদের সভাপতিত্বে কেন্দ্রীয় ভর্তি কমিটির সভায় এ সিদ্ধান্ত নেওয়া হয়েছে। পরে এক বিজ্ঞপ্তিতে সিদ্ধান্তের বিষয়টি সংবাদমাধ্যমকে জানানো হয়।
বিজ্ঞপ্তিতে বলা হয়, আগামী ৯ ডিসেম্বর ভর্তি পরীক্ষা অনুষ্ঠিত হবে। বন্ধের দিনসহ নির্ধারিত যেকোনো সয়য়ের মধ্যে অনলাইনের মাধ্যমে আবেদন করতে পারবেন ভর্তিচ্ছুরা।
ভর্তি সংক্রান্ত যাবতীয় তথ্য বিশ্ববিদ্যালয়ের ওয়েবসাইট- www.sau.edu.bd এ পাওয়া যাবে।

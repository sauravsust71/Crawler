ঢাকা বিশ্ববিদ্যালয়: ঢাকা বিশ্ববিদ্যালয়ের কেন্দ্রীয় ছাত্র সংসদ ( ডাকসু) নির্বাচনের  দাবিতে কনসার্ট হচ্ছে ঢাবিতে।
সোমবার (৪ ডিসেম্বর) সন্ধ্যায় বিশ্ববিদ্যালয়ের স্মৃতি চিরন্তন চত্বরে এই কনসার্ট শুরু হয়। যেখানে গত ২৫ নভেম্বর থেকে ডাকসু নির্বাচনের দাবিতে অনশন করছেন বিশ্ববিদ্যালয়ের সান্ধ্যকালীন কোর্সের ছাত্র ওয়ালিদ আশরাফ। তার প্রতি সংহতি জানিয়ে এ কর্মসূচি পালন করা হচ্ছে। রাত দশটা পর্যন্ত চলবে এই কনসার্ট।
এতে বিশ্ববিদ্যালয়ের বিভিন্ন বিভাগের শিক্ষার্থীরা গান পরিবেশন করছেন। সংহতি প্রকাশ করে বক্তব্য ও কবিতা আবৃত্তি করেছেন সমাজবিজ্ঞান বিভাগের শিক্ষক ড. সামিনা লুৎফা।
এ বিষয়ে ছাত্র ইউনিয়নের ঢাবি সংসদের সভাপতি তুহিন কান্তি দাস বলেন, আমরা ডাকসুর দাবিতে যে আন্দোলন করছি, তা সাধারণ শিক্ষার্থীদের মাঝে পৌঁছে দিতে চাই গানের মাধ্যমে। এর মধ্য দিয়ে ডাকসুর দাবিকে আমরা তুলে ধরতে চাই।
অনশনকারী ওয়ালিদ আশরাফ বলেন, আমার এ অনশন গণতন্ত্রের জন্য। আমাদের দেশ স্বাধীন হয়েছে কিন্তু এখনো পরাধীনতার মধ্যে আছি। ডাকসু একটি গণতান্ত্রিক দাবি। বিশ্ববিদ্যালয় প্রশাসন এ গণতান্ত্রিক দাবি মানছে না। যেটি খুবই হতাশাজনক।
প্রসঙ্গত, ১৯৯০ সালের পর থেকে ডাকসু নির্বাচন বন্ধ রয়েছে।

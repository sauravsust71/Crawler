ব্রাহ্মণবাড়িয়ায় এসএসসি’র ফলাফলে গত বছরের মতো এবারও জেলার শীর্ষস্থান দখল করেছে অন্নদা সরকারি উচ্চ বিদ্যালয়।
ব্রাহ্মণবাড়িয়া: ব্রাহ্মণবাড়িয়ায় এসএসসি’র ফলাফলে গত বছরের মতো এবারও জেলার শীর্ষস্থান দখল করেছে অন্নদা সরকারি উচ্চ বিদ্যালয়।
এ বছর বিদ্যালয়টি থেকে ৩০২ জন শিক্ষার্থী পরীক্ষা দিয়ে সবাই পাস করেছে। জিপিএ-৫ পেয়েছে ১২০ জন।
বুধবার (১১ মে) একযোগে প্রকাশিত এসএসসি ও সমমান পরীক্ষায় কারিগরি ও দাখিলসহ মোট ১০টি শিক্ষা বোর্ডের ফলাফলে দেখা যায়, শহরের উইসডম স্কুল অ্যান্ড কলেজ থেকে ৬৩ জনের সবাই, রামকানাই হাই একাডেমি থেকে ৫৭ জনের সবাই এবং কোকিল টেক্সটাইল মিল উচ্চ বিদ্যালয় থেকে ২০ জনের সবাই পাস করেছে।
জিপিএ-৫ পেয়েছে উইসডম স্কুল অ্যান্ড কলেজ থেকে সাতজন, রামকানাই হাই একাডেমি থেকে তিনজন। তবে কোকিল টেক্সটাইল মিল উচ্চ বিদ্যালয় থেকে কেউই জিপিএ-৫ পায়নি।
এর বাইরে সরকারি মডেল গার্লস স্কুল থেকে ১৮৩ জন পরীক্ষা দিয়ে ১৮২ জন পাস করেছে। আর জিপিএ-৫ পেয়েছে ৩৯ জন। সাবেরা সোবাহান বালিকা উচ্চ বিদ্যালয় থেকে ২৭৭ জন পরীক্ষায় অংশ নিয়ে ২৭৫ জন পাস করেছে। জিপিএ-৫ পেয়েছে ৫৩ জন।
ব্রাহ্মণবাড়িয়া উচ্চ বিদ্যালয় থেকে ২৮৩ জন পরীক্ষা দিয়ে ২৭৫ জন পাস করেছে এবং জিপিএ-৫ পেয়েছে ১৩ জন। নিয়াজ মুহাম্মদ উচ্চ বিদ্যালয় থেকে ১৬৬ জন পরীক্ষায় অংশ নেয়। এদের মধ্যে ১৫০ জন পাস করে এবং দুইজন জিপিএ-৫ পায়।

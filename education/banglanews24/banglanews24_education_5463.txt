দিনাজপুর: দিনাজপুরের ফুলবাড়ী সরকারি কলেজের (ভারপ্রাপ্ত) ভাইস প্রিন্সিপাল মো. রফিকুল ইসলামের অপসারণ দাবিতে মানববন্ধন, বিক্ষোভ মিছিল ও প্রতিবাদ সভা করেছে কলেজের সাধারণ শিক্ষার্থী ও শিক্ষকরা।
নানা অনিয়মের অভিযোগে বুধবার (৩ জানুয়ারি) দুপুর ১টায় ফুলবাড়ী সরকারি কলেজ এলাকায় এ কর্মসূচি পালন করেন তারা। এসময় অভিযুক্ত শিক্ষককে অপসারণ দাবিতে বিভিন্ন স্লোগান দেন তারা। পরে কলেজ চত্বরে সংক্ষিপ্ত প্রতিবাদ সভা অনুষ্ঠিত হয়।
সভায় শিক্ষার্থীরা অভিযোগ করে বলেন, ভাইস প্রিন্সিপাল রফিকুল ইসলাম গত আড়াই বছর ধরে এ কলেজে দায়িত্বে থাকা অবস্থায় তার অন্যায় অত্যাচারে কোনো শিক্ষক কলেজে থাকতে চান না।
অনেকে বদলি হয়ে চলে গেছেন। তিনি দায়িত্বে থাকায় কলেজের ছাত্রদের ৬টি রুম দখল করে নিয়ে রাখছেন, সরকারি জায়গা দখল হয়ে যাচ্ছে, কলেজের আইন শৃঙ্খলা পরিস্থিতি অবনতি ঘটছে, কলেজে কোনো পরিবেশ নেই, কলেজের শিক্ষক ছাত্র ছাত্রীদের নানাভাবে হয়রানি করা হচ্ছে। এসব অনিয়মের বিরুদ্ধে অভিযোগ করলে তিনি হয়রানি করেন।
এ কর্মসূচিতে উপস্থিত ছিলেন, সরকারি কলেজ শাখার ছাত্রলীগের সভাপতি মো. তৌহিদুজ্জামান, সাধারণ সম্পাদক মো. রাজিউল ইসলাম রাজু, সিনিয়র সহ-সভাপতি এএসএম নাসিম মাহমুদ, ছাত্রদল কলেজ শাখার নেতা মো. সাগর হোসেন প্রমুখ।
মানববন্ধন, বিক্ষোভ মিছিল ও আলোচনা সভা শেষে শতাধিক ছাত্র ছাত্রী ভাইস প্রিন্সিপালের অফিস ঘেরাও করে তার অপসারণের প্রতিবাদ জানান।

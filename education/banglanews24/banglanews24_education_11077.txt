রাজশাহী বিশ্ববিদ্যালয়ের (রাবি) সমাজবিজ্ঞান বিভাগের অধ্যাপক ড. এ কে এম শফিউল ইসলাম লিলন হত্যা মামলার বিচারের দীর্ঘসূত্রিতা নিয়ে হতাশা প্রকাশ করেছেন তার সহকর্মীরা।
রাবি: রাজশাহী বিশ্ববিদ্যালয়ের (রাবি) সমাজবিজ্ঞান বিভাগের অধ্যাপক ড. এ কে এম শফিউল ইসলাম লিলন হত্যা মামলার বিচারের দীর্ঘসূত্রিতা নিয়ে হতাশা প্রকাশ করেছেন তার সহকর্মীরা।
গত বছরের ২৫ নভেম্বর ওই হত্যা মামলার অভিযোগপত্র দেয় পুলিশ। পরে মামলাটি মহানগর দায়রা জজ আদালতে স্থানান্তর করা হয়। কিন্তু সেখানে এখনও অভিযোগপত্র গ্রহণ করেনি আদালত।
এদিকে, ওই মামলার সব আসামি বর্তমানে জামিনে রয়েছেন। এসব নিয়ে শফিউলের সহকর্মীদের মধ্যে চরম ক্ষোভ ও হতাশা বিরাজ করছে।
মঙ্গলবার (১৫ নভেম্বর) ড. শফিউল হত্যাকাণ্ডের দ্বিতীয় বর্ষ উপলক্ষে দ্রুত বিচার দাবিতে মানববন্ধন করেছেন রাবির শিক্ষকরা।
বেলা ১১টায় রাবির সিনেট ভবনের সামনে শিক্ষক সমিতির উদ্যোগে এ কর্মসূচিতে সমাজ বিজ্ঞান বিভাগের শিক্ষক-শিক্ষার্থীরা যোগ দেন।
মানববন্ধনে বক্তারা শফিউল হত্যাকাণ্ডের দ্রুত বিচার শেষ করার দাবি জানান।
রাবি শিক্ষক সমিতির সাধারণ সম্পাদক অধ্যাপক শাহ্ আজমের সঞ্চালনায় এতে বক্তব্য দেন রাখেন, সমাজ বিজ্ঞান বিভাগের সভাপতি অধ্যাপক ওয়ারদাতুল আকমাম, একই বিভাগের অধ্যাপক এ এইচ এম মোস্তাফিজুর রহমান, শিক্ষক সমিতির সভাপতি অধ্যাপক শহীদুল্লাহ্, সমাজকর্ম বিভাগের সভাপতি অধ্যাপক ছাদেকুল আরেফিন ও আইন বিভাগের অধ্যাপক হাসিবুল আলম প্রধান।
২০১৪ সালের ১৫ নভেম্বর দুপুরে বিশ্ববিদ্যালয় সংলগ্ন চৌদ্দপাই এলাকায় নিজ বাড়ির সামনে খুন হন অধ্যাপক শফিউল। পরদিন রাবির রেজিস্ট্রার অধ্যাপক মুহাম্মদ এন্তাজুল হক বাদী হয়ে মতিহার থানায় একটি হত্যা মামলা দায়ের করেন।

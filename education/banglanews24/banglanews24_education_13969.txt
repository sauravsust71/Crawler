সিলেট শিক্ষাবোর্ডে এবার মাধ্যমিক সার্টিফিকেট (এসএসসি) পরীক্ষায় পাসের হার বেড়েছে। এবার পাসের হার ৮৪ দশমিক ৭৭ শতাংশ। যা আগের বছর (২০১৫) ছিল ৮১ দশমিক ৮২ শতাংশ। অর্থাৎ গত বছরের তুলনায় ২ দশমিক ৯৫ শতাংশ পাসের হার বেড়েছে।
সিলেট: সিলেট শিক্ষাবোর্ডে এবার মাধ্যমিক সার্টিফিকেট (এসএসসি) পরীক্ষায় পাসের হার বেড়েছে। এবার পাসের হার ৮৪ দশমিক ৭৭ শতাংশ। যা আগের বছর (২০১৫) ছিল ৮১ দশমিক ৮২ শতাংশ। অর্থাৎ গত বছরের তুলনায় ২ দশমিক ৯৫ শতাংশ পাসের হার বেড়েছে।
তবে পাসের হার বাড়লেও কমেছে জিপিএ-৫। এবার জিপিএ ৫ পেয়েছে ২ হাজার ২৬৬ জন শিক্ষার্থী। অথচ আগের বছরে ২ হাজার ৪৫২ জন শিক্ষার্থী জিপিএ-৫ পেয়েছিলেন।   এবারের  এসএসসি পরীক্ষায় মানবিক, বিজ্ঞান ও ব্যবসায় শিক্ষা শাখায় মোট ৮৪ হাজার ৪৪৮ জন শিক্ষার্থী অংশ নেন। এর মধ্যে পাশ করে ৭১ হাজার ৫৮৬ জন।
গত বছরের ন্যায় এবারও মেয়েদের চেয়ে ছেলেরা এগিয়ে। ছেলেদের পাসের হার ৮৫ দশমিক ৭৯ শতাংশ। মেয়েদের পাসের হার ৮৩ দশমিক ৯৫ শতাংশ।
সিলেট শিক্ষা বোর্ডের অধীনে চার জেলায় পাসের হার সিলেটে ৮৬ দশমিক ৩০শতাংশ,  হবিগঞ্জে ৮৫ দশমিক ৯৯, মৌলভীবাজারে ৮২ দশমিক ৩৮ ও সুনামগঞ্জে ৮৩ দশমিক ৬০ শতাংশ।
বুধবার (১১ মে) দুপুরে সংবাদ সম্মেলনের মাধ্যমে এ ফলাফল ঘোষণা করেন সিলেট শিক্ষাবোর্ডের পরীক্ষা নিয়ন্ত্রক মো. সামসুল ইসলাম।
তিনি বলেন, সৃজনশীল পরীক্ষার কারণে পাসের হার বেড়েছে।

ঢাকা বিশ্ববিদ্যালয়: গাজীপুরে বঙ্গবন্ধু শেখ মুজিব সাফারি পার্কে  ঢাকা বিশ্ববিদ্যালয়ের (ঢাবি) শিক্ষার্থীদের ওপর হামলাক‍ারীদের বিচার দাবিতে মানববন্ধন হয়েছে।
রোববার (১২ ফেব্রুয়ারি) দুপুরে বিশ্ববিদ্যালয়ের অপরাজেয় বাংলার পাদদেশে এ মানববন্ধনের আয়োজন করা হয়। এতে বিশ্ববিদ্যালয়ের স্বাস্থ্য অর্থনীতি ইনিস্টিটিউটের  শিক্ষক-শিক্ষার্থীরা অংশ নেন। 
মানববন্ধনে দাবি করা হয়, ওই পার্কে ইজারাদাররা শিক্ষার্থীদের ওপর হামলা চালিয়েছেন। মানববন্ধনকারীরা এই বর্বরোচিত হামলার বিচার দাবি করে বিভিন্ন ধরনের প্ল্যাকার্ড প্রদর্শন করেন।
কর্মসূচিতে বক্তব্য রাখেন সামাজিক বিজ্ঞান অনুষদের ডিন অধ্যাপক ফরিদ উদ্দীন আহমেদ। তিনি বলেন, শিক্ষার্থীদের ওপর ইজারাদারদের হামলা অত্যন্ত নিন্দনীয়। এ ঘটনা পার্কের ইজারাদারের রাজত্বের প্রমাণ করে। দলীয় পরিচয় বিবেচনায় না এনে প্রকৃত অপরাধীদের দৃষ্টান্তমূলক শাস্তি নিশ্চিত করতে হবে। 
এখনো যারা গ্রেপ্তার হয়নি তাদের দ্রুত গ্রেপ্তারের দাবিও জানান তিনি।
এসময় উপস্থিত ছিলেন ইনস্টিটিউটের পরিচালক সৈয়দ আব্দুল হামিদ, লোক প্রশাসন বিভাগের চেয়ারম্যান অধ্যাপক আখতার হোসেন, ইসলামের ইতিহাস ও সংস্কৃতি বিভাগের অধ্যাপক ছিদ্দিকুর রহমান খান প্রমুখ।
একই দাবিতে সোমবার (১৩ ফেব্রুয়ারি) ১১টায় বিশ্ববিদ্যালয়ে আরও ব্যাপক পরিসরে মানববন্ধন আয়োজন করার কথা জানানো হয়।

ফেনী: বছরের প্রথম দিনটিতেই নতুন বই হাতে পেয়ে আনন্দে আত্মহারা ফেনীর প্রাথমিক ও মাধ্যমিকের শিক্ষার্থীরা।
প্রতিটি স্কুলেই জমে উঠেছে বই উৎসব, চারিদিকে বইছে নতুন বইয়ের সুগন্ধ। নতুন বইয়ের রঙিন পাতা উল্টিয়ে একে অপরের সঙ্গে আনন্দ ভাগাভাগি করে নিতেও ভুল করছে না তারা।
রোববার (০১ জানুয়ারি) জেলার সব প্রাথমিক ও মাধ্যমিক বিদ্যালয়ে এক যোগে বই উৎসবের মধ্য দিয়ে শিক্ষার্থীদের হাতে নতুন বই তুলে দেওয়া হয়।
জেলা শিক্ষা অফিস সূত্রে জানা যায়, সরকারি প্রাথমিক বিদ্যালয়, প্রাক-প্রাথমিক বিদ্যালয়, কিন্ডারগার্টেন ও মাধ্যমিক বিদ্যালয়ের শিক্ষার্থীদের ধাপে ধাপে পুরো জেলা বিনামূল্যে ৩৫ লক্ষাধিক নতুন বই প্রদান করা হবে।
ফেনী সরকারি বালিকা উচ্চ বিদ্যালয়ের মাঠে উৎসবের আনুষ্ঠানিক উদ্বোধন করেন জেলা প্রশাসক মো. আমিন উল আহসান।
বিদ্যালয়ের প্রধান শিক্ষক সুব্রত নাথের সভাপতিত্বে অনুষ্ঠানে আরও উপস্থিত ছিলেন ফেনী সরকারি কলেজ অধ্যক্ষ আবুল কালাম আজাদ, ফেনী সরকারি জিয়া মহিলা কলেজের অধ্যক্ষ ফাতেমা আফরোজ, ফেনী সদর উপজেলা নির্বাহী কর্মকর্তা (ইউএনও) পিকেএম এনামুল করিম প্রমুখ ।
এদিকে শহরের বিভিন্ন স্কুলে গিয়ে দেখা যায়, সকাল ৯টা থেকে শিক্ষার্থীরা মেতে উঠেছে নতুন বই পাওয়ার উৎসবে। বই হাতে পাওয়ার পর পাতা উল্টাতে ব্যস্ত তারা। নতুন বই নিয়ে যেন গোটা শহরেই উৎসব।

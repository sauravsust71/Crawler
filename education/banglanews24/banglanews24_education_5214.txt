‘উড়লে আকাশে প্রজাপতি, প্রকৃতি পায় নতুন গতি’ স্লোগানে সপ্তম বারের মতো প্রজাপতি মেলার আয়োজন করতে যাচ্ছে জাহাঙ্গীরনগর বিশ্ববিদ্যালয় প্রাণিবিদ্যা বিভাগ।
জাহাঙ্গীরনগর বিশ্ববিদ্যালয়: ‘উড়লে আকাশে প্রজাপতি, প্রকৃতি পায় নতুন গতি’ স্লোগানে সপ্তম বারের মতো প্রজাপতি মেলার আয়োজন করতে যাচ্ছে জাহাঙ্গীরনগর বিশ্ববিদ্যালয় প্রাণিবিদ্যা বিভাগ।
মঙ্গলবার (১১ অক্টোবর) প্রজাপতি মেলার আহ্বায়ক অধ্যাপক মো. মনোয়ার হোসেন বাংলানিউজকে এ তথ্য নিশ্চিত করেন।
তিনি জানান, দিন দিন প্রজাতির সংখ্যা কমে যাচ্ছে। তাই সব মানুষের মধ্যে প্রজাপতি সচেতনতা ও সংরক্ষণের প্রয়োজনীয়তা উপলব্ধি করানোর জন্য প্রতি বছরের মতো এবারও মেলার আয়োজন করতে যাচ্ছি। আগামী ৪ নভেম্বর বিশ্ববিদ্যালয় জহির রায়হান মিলনায়তনের সামনে প্রজাপতি মেলার উদ্বোধন করা হবে। যার উদ্বোধন করবেন বিশ্ববিদ্যালয় উপাচার্য অধ্যাপক ফারজানা ইসলাম। সকাল ৯টা থেকে বিকেল ৪টা পর্যন্ত মেলা চলবে।
এবারের মেলায় দিনব্যাপী আয়োজনের মধ্যে থাকছে, র‌্যালি, ছবি আঁকা প্রতিযোগিতা (শিশু-কিশোরদের জন্য), প্রজাপতির আলোকচিত্র প্রদর্শনী, প্রজাপতি বিষয়ক আলোকচিত্র প্রতিযোগিতা, হাট দর্শন (জীবন্ত প্রজাপতি প্রদর্শন), প্রজাপতির আদলে ঘুড়ি উড়ানো, বিতর্ক প্রতিযোগিতা (প্রজাপতি ও জলবায়ু পরিবর্তন), প্রজাপতি চেনা প্রতিযোগিতা, প্রজাপতি বিষয়ক প্রমাণ্যচিত্র প্রদর্শনী, পুরস্কার বিতরণ ও সমাপনী অনুষ্ঠান।
 
২০১০ সাল থেকে প্রাণিবিদ্যা বিভাগের উদ্যোগে ধারাবাহিকভাবে এ মেলার আয়োজন করা হচ্ছে।
বাংলাদেশ সময়: ১৯৪৬ ঘণ্টা, অক্টোবর ১১, ২০১৬

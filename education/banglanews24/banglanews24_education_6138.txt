শাবিপ্রবি: শাহজালাল বিজ্ঞান ও প্রযুক্তি বিশ্ববিদ্যালয়ের (শাবিপ্রবি) যন্ত্রকৌশল বিভাগের ১ম প্রতিষ্ঠাবার্ষিকী পালন করা হয়েছে।
সোমবার (০৯ জানুয়ারি)  দুপুর দেড়টায় আনন্দ শোভাযাত্রা ও কেক কাটার মধ্যদিয়ে দিনটি উদযাপন করেন তারা।
আনন্দ শোভাযাত্রাটি ক্যাম্পাসের একাডেমিক ‘এ’ বিল্ডিং থেকে শুরু হয়ে ক্যাম্পসের প্রধান প্রধান সড়ক প্রদক্ষিণ শেষে একই স্থানে এসে শেষ হয়। শোভাযাত্রা শেষে কেক কেটে ১ম বর্ষপূর্তি পালন করা হয়।
এসময় উপস্থিত ছিলেন- বিভাগীয় প্রধান অধ্যাপক আরিফুল ইসলাম, শিল্প উৎপাদন কৌশল বিভাগের অধ্যাপক ড. মুহসিন আজিজ খান, সহকারী অধ্যাপক জাহিদ হাসান, ফেরদৌস আলম, জাহিদুল ইসলাম, শান্তা সাহা ও সায়েদা কামরুন্নাহার প্রমুখ।
বাংলাদেশ সময়: ১৬২২ ঘণ্টা, জানুয়ারি ০৯, ২০১৭ বিএস

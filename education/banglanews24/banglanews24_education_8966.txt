চলতি বছরের এইচএসসি ও সমমানের পরীক্ষায় গড় পাসের হার ৭৪.৭০ শতাংশ। সারাদেশে জিপিএ-৫ পেয়েছেন ৫৮ হাজার ২৭৬ জন শিক্ষার্থী। গত বছর গড় পাসের হার ছিল ৬৯ দশমিক ৬০ শতাংশ।
ঢাকা: চলতি বছরের এইচএসসি ও সমমানের পরীক্ষায় গড় পাসের হার ৭৪.৭০ শতাংশ। সারাদেশে জিপিএ-৫ পেয়েছেন ৫৮ হাজার ২৭৬ জন শিক্ষার্থী। গত বছর গড় পাসের হার ছিল ৬৯ দশমিক ৬০ শতাংশ।
এ বছর ঢাকা বোর্ডে পাসের হার ৭৩.৫৩ শতাংশ, কুমিল্লা বোর্ডে পাসের হার ৬৪.৪৯ শতাংশ, সিলেট বোর্ডে পাসের হার ৬৮.৫৯ শতাংশ, চট্টগ্রাম বোর্ডে পাসের হার ৬৪.৬০ শতাংশ, দিনাজপুর বোর্ডে পাসের হার ৭০.৬৪ শতাংশ, রাজশাহী বোর্ডে পাসের হার ৭৫.৪০ শতাংশ, বরিশাল বোর্ডে পাসের হার ৭০.১৩ শতাংশ ও যশোর বোর্ডে পাসের হার ৮৩.৪২ শতাংশ।
মাদ্রাসা বোর্ডে পাসের হার ৮৮.১৯ শতাংশ। মোট পাস করেছেন ৮০ হাজার ৬০৩ জন। জিপিএ-৫ পেয়েছেন ২,৪১৪ জন।
কারিগরি বোর্ডে পাসের হার ৮৪.৫৭ শতাংশ। মোট পাস ৮৬ হাজার ৪৬৯ জন। জিপিএ-৫ পেয়েছেন ৬,৫৮৭ জন। এবার ছেলেদের পাসের হার ৭৩.৯৩ শতাংশ, মেয়েদের পাসের হার ৭৫.৬০ শতাংশ।
সিলেট বোর্ডে জিপিএ-৫ পেয়েছেন ১,৩৩০ জন, কুমিল্লা বোর্ডে ১,৯১২ জন, রাজশাহী বোর্ডে ৬,০৭৩ জন, চট্টগ্রাম বোর্ডে জিপিএ-৫ পেয়েছেন ২,২৫৩ জন, দিনাজপুর বোর্ডে জিপিএ-৫ পেয়েছেন ৩,৮৯৯ জন, বরিশাল বোর্ডে জিপিএ-৫ পেয়েছেন ৭৮৭ জন, যশোর বোর্ডে জিপিএ-৫ পেয়েছেন ৪,৫৮৬ জন।
বিদেশে ৭টি শিক্ষা প্রতিষ্ঠান থেকে ২৪৮ জন পরীক্ষার্থী অংশ নেন। পাস করেন ২৩২ জন। পাসের হার ৯৩.৫৫ শতাংশ। জিপিএ-৫ পেয়েছেন ৫৩ জন।
এ বছর মোট পাস করেছেন ৮ লাখ ৯৯ হাজার ১৫০ জন। বিজ্ঞান বিভাগ থেকে জিপিএ-৫ পেয়েছেন ৪১ হাজার ৪৬৮ জন।
এবার এইচএসসিতে কলেজ বোর্ডগুলোর ফলাফলে পাসের হার ৭২.৪৭ শতাংশ, মোট পাস করেছেন ৭,২৯,৮০৩ জন ও জিপিএ-৫ পেয়েছেন ৪৮,৯৫০ জন।
বৃহস্পতিবার (১৮ আগস্ট) সকালে প্রধানমন্ত্রী শেখ হাসিনার কাছে গণভবনে শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ বোর্ডের চেয়ারম্যানদের সঙ্গে নিয়ে ফলের অনুলিপি হস্তান্তর করেন।
দুপুর ১টায় সচিবালয়ে সংবাদ সম্মেলনে ফলের বিস্তারিত তুলে ধরবেন শিক্ষামন্ত্রী।
দুপুর ২টা থেকে শিক্ষার্থীরা নিজ নিজ শিক্ষা প্রতিষ্ঠান এবং মোবাইল এসএমএস ও ইন্টারনেটের মাধ্যমে ফল জানতে পারবেন।
গত ৩ এপ্রিল থেকে শুরু হওয়া এইচএসসি ও সমমানের পরীক্ষায় সারাদেশে দুই হাজার ৪৫২টি কেন্দ্রে ১২ লাখ ১৮ হাজার ৬২৮ জন শিক্ষার্থী অংশ নেয়। এর মধ্যে ছাত্র ৬ লাখ ৫৪ হাজার ১১৪ জন ও ছাত্রী ৫ লাখ ৬৪ হাজার ৫১৪ জন। গত বছরের তুলনায় এবার এক লাখ ৪৪ হাজার ৭৪৪ জন বেশি শিক্ষার্থী অংশ নেয়।

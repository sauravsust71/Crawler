জগন্নাথ বিশ্ববিদ্যালয় (জবি): জগন্নাথ বিশ্ববিদ্যালয়ের (জবি) ২০১৭-১৮ শিক্ষাবর্ষের বিজ্ঞান অনুষদ ও লাইফ অ্যান্ড আর্থ সায়েন্স অনুষদ অন্তর্ভুক্ত ‘এ’ ইউনিটের ভর্তি পরীক্ষা শুক্রবার (১৩ অক্টোবর) অনুষ্ঠিত হবে। 
জবি ক্যাম্পাসসহ রাজধানীর মোট ২৭টি কেন্দ্রে একযোগে এ পরীক্ষা অনুষ্ঠিত হবে। 
বৃহস্পতিবার (১২ অক্টোবর) সন্ধ্যায় বিশ্ববিদ্যালয়ের তথ্য ও গণসংযোগ দপ্তর থেকে পাঠানো এক প্রেসবিজ্ঞপ্তিতে এ তথ্য জানানো হয়। 
এতে বলা হয়, ২০১৭-১৮ শিক্ষাবর্ষের ‘এ’ ইউনিটের স্নাতক (সম্মান) ভর্তি পরীক্ষা শুক্রবার বিকাল ৩টা থেকে ৪টা পর্যন্ত বিশ্ববিদ্যালয়সহ মোট ২৭টি কেন্দ্রে একযোগে হবে।  
এবার বিজ্ঞান অনুষদ ও লাইফ অ্যান্ড আর্থ সায়েন্স অনুষদ অন্তর্ভুক্ত ‘এ’ ইউনিটের ৭৯৭টি আসনের বিপরীতে ৫৯ হাজার ৪৩৩ জন অর্থাৎ প্রতি আসনের বিপরীতে প্রায় ৭৫ জন শিক্ষার্থী ভর্তি পরীক্ষার জন্য আবেদন করেছেন।
প্রেসবিজ্ঞপ্তিতে জানানো হয়, পরীক্ষা কেন্দ্রে পরীক্ষা শুরুর ৩০ মিনিট আগে আসন গ্রহণ করতে হবে। পরীক্ষা শুরুর ১৫ মিনিট আগে কেন্দ্রের মূল ফটক বন্ধ হয়ে যাবে। এরপর কোনো পরীক্ষার্থী কেন্দ্রে প্রবেশ করতে দেওয়া হবে না। 
এছাড়া পরীক্ষা কক্ষে মোবাইল ফোন, ক্যালকুলেটর, ঘড়ি ও অন্য যে কোনো ধরনের ইলেকট্রনিক ডিভাইস সঙ্গে রাখা সম্পূর্ণ নিষিদ্ধ। 
পরীক্ষার্থীদের হাফ শার্ট ও স্যান্ডেল (জুতা ও মোজা ছাড়া) পরে পরীক্ষা কেন্দ্রে আসতে হবে। ধর্মীয় অনুশাসন মেনে যারা পোশাক পরিধান করবে তাদের জন্য পোশাকের শর্ত শিথিলযোগ্য। তবে এর সুযোগ নিয়ে যাতে কেউ অপকর্ম করতে না পারে সে ব্যাপারে বিশেষ নজরদারির ব্যবস্থা থাকবে।
ভর্তি পরীক্ষার আসন বিন্যাসসহ এ সংক্রান্ত যে কোনো তথ্য বিশ্ববিদ্যালয়ের ওয়েবসাইট (http://admission.jnu.ac.bd অথবা http://admissionjnu.info)-এ পাওয়া যাবে।

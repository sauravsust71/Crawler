সিলেট শিক্ষাবোর্ডের অধীনে এবার ২৪৬টি কলেজ উচ্চ মাধ্যমিক (এসএসসি) পরীক্ষায় অংশ নেয়। অংশগ্রহণকারী শিক্ষার্থীদের মধ্যে মেয়েদের অংশগ্রহণ ছিল বেশি।
সিলেট: সিলেট শিক্ষাবোর্ডের অধীনে এবার ২৪৬টি কলেজ উচ্চ মাধ্যমিক (এসএসসি) পরীক্ষায় অংশ নেয়। অংশগ্রহণকারী শিক্ষার্থীদের মধ্যে মেয়েদের অংশগ্রহণ ছিল বেশি।
তবে পরীক্ষায় উপস্থিতিতে মেয়েরা এগিয়ে থাকলেও পাসের হারে এগিয়ে ছেলেরা। বিজ্ঞান, মানবিক ও বাণ্যিজ্য বিভাগে পাসের হারে এগিয়ে ছেলেরা। এমনকি জিপিএ-৫ প্রাপ্তিতেও।
বোর্ডের অধীনে ৬৩ হাজার ৯৫৯ জন শিক্ষার্থী অংশ নিলেও পাস করেছে ৪৩ হাজার ৮৭০ জন। পাসের হার ৬৮ দশমিক ৫৯ শতাংশ। জিপিএ-৫ পেয়েছে এক হাজার ৩৩০জন শিক্ষার্থী। 
ছেলেদের মধ্যে ২৯ হাজার ৬৬৭ জন পরীক্ষার্থীর মধ্যে ২৯ হাজার ৫৫৩ জন পরীক্ষায় অংশ নিয়ে পাস করেছে ২০ হাজার ৭৭৯ জন। 
মেয়েদের মধ্যে ৩৪ হাজার ৪৮৬ জনের মধ্যে পরীক্ষায় অংশ নেয় ৩৪ হাজার ৪০৬ জন। এর মধ্যে ২৩ হাজার ৯১ জন মেয়ে কৃতকার্য হয়। ছেলেদের পাসের হার ৭০ দশমিক ৩১ ভাগ, আর মেয়েদের হার ৬৭ দশমিক ১১ ভাগ।
বিভাগ ভিত্তিক ফলাফলে দেখা গেছে, বিজ্ঞান বিভাগে ৮ হাজার ৫৬৮ পরীক্ষার্থীর মধ্যে পাস করেছে ৭ হাজার ৪৭৪ জন। যেখানে মোট পাসের হার ৮৭ দশমিক ২৩ শতাংশ।
এ বিভাগে ৪ হাজার ৬৭৮ ছেলের মধ্যে পাস করেছে ৪ হাজার ৯৩ জন। পাসের হার ৮৭ দশমিক ৪৯ ভাগ। এ বিভাগে ৩ হাজার ৮৯০ জন মেয়ে পরীক্ষায় অংশ নিয়ে পাস করে ৩ হাজার ৩৮১ জন।  মেয়েদের পাসের হার ৮৭ দশমিক ৪৯ শতাংশ।
মানবিক বিভাগে ৪৩ হাজার ২৬১ জন পরীক্ষার্থী থাকলেও পরীক্ষায় অংশগ্রহণ করে  ৪৩ হাজার ১৪৮ জন। এরমধ্যে কৃতকার্য হয় ২৭ হাজার ১৪ জন। যার মোট পাসের হার ৬২ দশমিক ৬১ শতাংশ।
মানবিকে ১৭ হাজার ৫৩৪ জনের মধ্যে ১১ হাজার ৫৪ জন ছেলে পাস করে। যার পাসের হার ৬৩ দশমিক ০৪ শতাংশ।  এছাড়া ২৫ হাজার ৬১৪ জন মেয়ের মধ্যে পাস করেছে ১৫ হাজার ৯৬০ জন।  মেয়েদের পাসের হার ৬২ দশমিক ৩১ শতাংশ।  বাণিজ্য বিভাগে ১২ হাজার ২৭২ জন পরীক্ষার্থী থাকলেও অংশ নেয় ১২ হাজার ২৪৩ জন। এরমধ্যে কৃতকার্য হয় ৯ হাজার ৩৮২ জন।  পাসের হার ৭৬ দশমিক ৬৩ শতাংশ। 
মানবিকে ৭ হাজার ৩৪১ জন ছেলের মধ্যে পাস করেছে ৫ হাজার ৬৩২ জন। পাসের হার ৭৬ দশমিক ৭২ ভাগ। অন্যদিকে ৪ হাজার ৯০২ জন মেয়ের মধ্যে পাস করেছে ৩ হাজার ৭৫০ জন। এখানে পাসের হার ৭৬ দশমিক ৫০ শতাংশ। 
এরআগে বৃহস্পতিবার (১৮ আগস্ট) সকাল ১১টায় আনুষ্ঠানিকভাবে এ ফলাফল  ঘোষণা করা হয়। এ বছর সিলেট বোর্ডের অধীনে ২৪৬টি শিক্ষা প্রতিষ্ঠানের মাধ্যমে মোট ৬৩ হাজার ৯৫৯ জন শিক্ষার্থী পরীক্ষায় অংশ নিয়ে পাস করেছে ৪৩ হাজার ৮৭০জন। এদের মধ্যে ছেলে ২০ হাজার ৭৭৯ ও মেয়ে শিক্ষার্থী ২৩হাজার ৯১জন। জিপিএ-৫ পেয়েছে মোট ১হাজার ৩৩০জন শিক্ষার্থী। এদের মধ্যে ছেলে ৭৯৪ ও মেয়ে ৫৩৬জন।
আর সিলেট শিক্ষাবোর্ডে এবার শতভাগ ফলাফল অর্জন করেছে ৫টি কলেজ। এগুলো হলো সিলেট ক্যাডেট কলেজ, জালালাবাদ ক্যান্টনমেন্ট পাবলিক স্কুল অ্যান্ড কলেজ, উইমেন্স মডেল স্কুল অ্যান্ড কলেজ, ভাতরাই স্কুল অ্যান্ড কলেজ ও খাজাঞ্জিবাড়ি স্কুল অ্যান্ড কলেজ।

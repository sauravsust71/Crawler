import jdk.internal.org.objectweb.asm.Handle;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Temp {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new FileReader(new File("/home/void/Desktop/a2i_designations.txt")));
        PrintWriter printWriter = new PrintWriter(new FileWriter(new File("/home/void/Desktop/sortedDesignation.txt")));
        HashMap<String,Integer> hashMap = new HashMap<>();
        while (scanner.hasNext()){
            String designation = scanner.nextLine();

            if(hashMap.containsKey(designation)){
                hashMap.put(designation,hashMap.get(designation)+1);
            }
            else {
                hashMap.put(designation,1);
            }
        }
        hashMap = sortByValue(hashMap,false);

        for(String key : hashMap.keySet()){
            printWriter.println(key);

        }
        printWriter.close();
    }

    private static HashMap<String, Integer> sortByValue(Map<String, Integer> unsortMap, final boolean order)
    {
        List<Map.Entry<String, Integer>> list = new LinkedList<>(unsortMap.entrySet());

        // Sorting the list based on values
        list.sort((o1, o2) -> order ? o1.getValue().compareTo(o2.getValue()) == 0
                ? o1.getKey().compareTo(o2.getKey())
                : o1.getValue().compareTo(o2.getValue()) : o2.getValue().compareTo(o1.getValue()) == 0
                ? o2.getKey().compareTo(o1.getKey())
                : o2.getValue().compareTo(o1.getValue()));
        return list.stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> b, LinkedHashMap::new));

    }
}

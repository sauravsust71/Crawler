import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.File;
import java.io.IOException;

/**
 * Created by void on 11/7/17.
 */
public class TestCrawler {

    public static void main(String args[]) throws IOException {
        File input = new File("/home/void/DocClassifier/crawler_py/test.html");
        Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");

        Elements links = doc.select("a[href]"); // a with href
        //Elements pngs = doc.select("img[src$=.png]");
        // img with src ending .png

        Elements ps = doc.select("div.pf-content>p");
        ps.remove(ps.size()-1);

        for(Element p : ps){
            if(p.hasText()) {
                System.out.println(p.text());
            }
        }

        System.out.println(ps.size());

    }


    public TestCrawler() throws IOException {
    }
}

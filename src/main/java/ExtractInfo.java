import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by void on 11/9/17.
 */
public class ExtractInfo {
    public static void main(String args []){

        int length = 0;
        int [] lengthArray ;
        int belowAvgLength = 0;
        File folder = new File("/home/void/Crawler/data_merged");
        File [] listOfFiles = folder.listFiles();
        lengthArray = new int[listOfFiles.length];
        
        

        for(int i = 0;i< listOfFiles.length;i++){
            File file = listOfFiles[i];
            if (file.isFile()){
                try {
                    String doc = FileUtils.readFileToString(file);
                    length += doc.length();
                    if(doc.length() < 400)
                        belowAvgLength++;
                    lengthArray[i] = doc.length();
                    
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("total files "+listOfFiles.length);
        System.out.println("avg length "+length / listOfFiles.length);
        System.out.println("below avg length files "+belowAvgLength);
        Arrays.sort(lengthArray);
        System.out.println(lengthArray[lengthArray.length / 2]);

    }


}

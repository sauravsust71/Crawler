import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class LifeStyleKalerkantho {
    private HashSet<String> links;
    private HashSet<String> linksForParsing;
    private List<List<String>> articles;
    private static final int MAX_DEPTH = 700;
    int count = 0;
    int linkCountTest = 0;
    String filename = "/home/void/IdeaProjects/Crawler/lifestyle/kalerkantho/lifestyle_kalerkantho_2018_";

    public LifeStyleKalerkantho() {
        links = new HashSet<>();
        articles = new ArrayList<>();
        linksForParsing = new HashSet<>();

    }

    //Find all URLs that start with "http://www.mkyong.com/page/" and add them to the HashSet
    public void getPageLinks(String URL,int depth) {
        if (!links.contains(URL) && (depth < MAX_DEPTH ) ) {
            linkCountTest++;
            try {
                Document document = Jsoup.connect(URL)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .timeout(10*1000).get();
               // System.out.println("this is doucument "+document);
                //System.out.println(document.text());
                Elements otherLinks = document.select("a[href^=\"./online/lifestyle/2018/\"]");


                for (Element page : otherLinks) {
                    {
                        links.add(URL);
                        //Remove the comment from the line below if you want to see it running on your editor
                        //System.out.println(URL);
                        System.out.println("calling with this "+ page.attr("abs:href"));
//                        if(!page.attr("abs:href").contains("/life-style/"))
                        //System.out.println("link added");
                        linksForParsing.add(URLDecoder.decode( page.attr("abs:href"), StandardCharsets.UTF_8.name() )) ;
                        //System.out.println(URL);
                        if (links.contains(page.attr("abs:href")))
                            continue;
                        getPageLinks(page.attr("abs:href"),++depth);
                    }

                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
            }
            finally {
                //System.out.println("exception ");
//                getArticles();
//                writeToFile();
//                System.out.println("getArticle called");

            }
        }
    }

    //Connect to each link saved in the article and find all the articles in the page
    public void getArticles() {
        System.out.println("links count "+linksForParsing.size());


        linksForParsing.forEach(x -> {

            {

                String doc = "";
                System.out.println(x);

                Document document;
                try {
                    document = Jsoup.connect(x).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                            .referrer("http://www.google.com")
                            .timeout(10*1000).get();
//                    Elements articleBody = document.select(".content_detail > .detail_holder > .right_part >.col_in")
//                            .select("article > div").select("p");
                    // System.out.println(document);

                    Elements articleBody = document.select("body > div> div.col-sm-12.col-md-8.details > div.some-class-name2 > p");
                    // System.out.println(articleBody);

                    //*[@id="widget_55142"]/div/div/div[3]/div[2]/div/article/div

                    //System.out.println("calledArticle from"+x +"--connected");
                    if(articleBody.size() >2)
                    {
//                        articleBody.remove(articleBody.size()-1);
                        articleBody.remove(articleBody.size()-1);
                    }


                    //System.out.println("articles size " + articleBody.size());
                    for (Element article : articleBody) {
                        if(article.hasText())
                        {

                            doc += article.text();
                            doc += "\n";
                        }

                    }

                    if(doc.length() > 20)
                    {

                        System.out.println(doc);
                        writeNow(doc);
                    }


                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
                finally {
//                    writeToFile();
                }
            }
        });
    }

    private void writeNow(String text) {
        FileWriter writer ;
        {

            {
                try {
                    writer= new FileWriter(filename+Integer.toString(count++)+".txt");

                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer.write(text);
                    writer.close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
//            writer[0].close();
        }
    }

    public void writeToFile() {

        final FileWriter[] writer = new FileWriter[1];
        {

            articles.forEach(a -> {
                try {
                    writer[0] = new FileWriter(filename+Integer.toString(count++));
                    String temp = a.get(0) ;
                    //display to console
                    //System.out.println(temp);
                    //save to file
                    writer[0].write(temp);
                    writer[0].close();
                    System.out.println("file written "+filename+Integer.toString(count));
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            });
//            writer[0].close();
        }
    }

    public static void main(String[] args)  {
        LifeStyleKalerkantho bwc = new LifeStyleKalerkantho();
        bwc.getPageLinks("http://www.kalerkantho.com/online/lifestyle/",0);
        bwc.getArticles();
        //bwc.writeToFile();
        System.out.println("done.");
    }
}
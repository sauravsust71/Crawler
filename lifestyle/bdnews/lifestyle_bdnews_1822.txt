দিয়েছেন আনার সোহেল।
মাংসের জন্য
উপকরণ: কচি গরুর মাংস ২ কেজি। এলাচ ৬টি। লবঙ্গ ৫,৬টি। আদার রস ২ টেবিল-চামচ। রসুনের রস ১ টেবিল-চামচ। তেজপাতা ৪টি। পেঁয়াজের রস ২ টেবিল-চামচ। দারুচিনি ৬ টুকরা। জাফরান আধা চা-চামচ। টকদই ১ কাপ। লবণ স্বাদ মতো। সরিষার তেল আধা কাপ। সাদা গোলমরিচের গুঁড়া ২ চা-চামচ। চিনি ১ টেবিল-চামচ। জয়ত্রিগুঁড়া আধা চা-চামচ।
পদ্ধতি: মাংস বড় টুকরা করে কেটে ধুয়ে পানি ঝরিয়ে নিতে হবে। উপরের সব উপকরণ দিয়ে মাংস মাখিয়ে পাঁ থেকে ছয় ঘণ্টা রেখে দিন মাখানো অবস্থায়।
অন্যান্য
উপকরণ: পোলাওয়ের চাল ১ কেজি। পেঁয়াজ বেরেস্তা আধা কাপ। মাঝারি আকারের আলু ১০,১২টি। জাফরান আধা চা-চামচ। পেস্তাবাদাম কুচি ৪ টেবিল-চামচ। কাজু ১ কাপ। কাঁচামরিচ ১৫,১৬টি। ঘন দুধ ১ কাপ। মিষ্টি আতর ৫,৬ ফোঁটা। ঘি ১ কাপ। মালাই আধা কাপ। দারুচিনি ৬ টুকরা। আলুবোখারা ১০,১২টি। এলাচ ৪টি। কিশমিশ ২ টেবিল-চামচ। লবঙ্গ ৬টি। কেওড়া ১ টেবিল-চামচ।
চাল ধুয়ে পানি ঝরিয়ে পরিমাণ মতো লবণ ও দুই টেবিল-চামচ লেবুর রস মাখিয়ে রাখতে হবে।
আলু ছিলে সামান্য রং ও লবণ মাখিয়ে রাখতে হবে। কেওড়ার পানিতে জাফরান ভিজিয়ে ২০ থেকে ২৫ মিনিট রাখুন। বিরিয়ানির হাঁড়িতে প্রথমে মাখানো মাংস ছড়িয়ে দিন, তারপর আলু দিন ছড়িয়ে।
কাজু ও আলুবোখারা, কাঁচামরিচ, চাল, গরম মসলা দিয়ে দিন এর উপর দিয়ে। এরপর কিশমিশ, পেস্তাবাদাম-কুচি, দুধ, মালাই ও ছয় কাপ পানি দিন।
ঘি, জাফরান মিশ্রিত কেওড়ায় পানি, মিষ্টি আতর, কিছু বেরেস্তা দেওয়ার পর ঢাকনা দিয়ে ঢেকে দিন। তবে ঢাকনার চারপাশ ময়দা দিয়ে (এজন্য ময়দা মাখিয়ে নিন পরিমাণ মতো) আটকিয়ে দিতে হবে।
এরপর এভাবে প্রথমে পাঁচ মিনিট বেশি জ্বালে, ১০ মিনিট মাঝারি জ্বালে এবং ১৫ থেকে ২০ মিনিট অল্প জ্বালে রাখবেন।
১০ থেকে ১৫ মিনিট দমে রেখে তারপর নামাতে হবে। বিরিয়ানি নামিয়ে পরিবেশন পাত্রে ঢেলে কিছু পেস্তাকুচি ও বেরেস্তা দিয়ে সাজিয়ে সালাদ ও বোরহানির সঙ্গে পরিবেশন করুন।
সমন্বয়ে ইশরাত মৌরি।
আরও রেসিপি:
ঢাকাই বিরিয়ানি
কাচ্চি বিরিয়ানি

খাদ্য ও পুষ্টিবিষয় একটি ওয়েবসাইটে জানানো হয়, হলুদ হল মসলার রানী। যাকে ‘নিউট্রিশনাল সাপ্লিমেন্টস’ হিসেব গণ্য করা হয়।
অ্যান্টি-অক্সিডেন্ট, ব্যাক্টেরিয়ারোধী, ফাঙ্গাসরোধী, ভাইরাসরোধী, অ্যান্টি-কারসিনোজেনিক ও প্রদাহরোধী উপাদান থাকে এই এক মসলায়। এছাড়াও থাকে ডায়েটারি ফাইবার বা আঁশ, আমিষ, ক্যালসিয়াম, তামা, লৌহ, ম্যাগনেসিয়াম, দস্তা, ভিটামিন ই এবং সি।
দুধের সঙ্গে হলুদ: পরিবারে কারও সর্দি-কাশি হলে দাদি-নানিরা ছুটে আসেন হলদি দুধ নিয়ে। আদিকালের এই রেসিপিটাও সহজ। ফুটানোর পর বা ফুটন্ত অবস্থায় দুধে আধা টেবিল-চামচ হলুদ যোগ নিতে হবে।
ব্রণ সারাতে: ত্বকের যত্নে এবং ব্রণ সারাতে হলুদ বেশ উপকারী। এক টেবিল-চামচের এক-তৃতীংশ পরিমাণ হলুদের সঙ্গে সমপরিমাণ মধু মিশিয়ে পেস্ট তৈরি করে ব্রনের উপর মাখিয়ে শুকানো পর্যন্ত রেখে পরে ঠাণ্ডা পানি দিয়ে ধুয়ে নিতে হবে।
পেট পরিষ্কার: দেহের দুষিত পদার্থ বের করে দিতে পারে হলুদ।
কুসুম গরম পানিতে এক টেবিল-চামচের এক তৃতীংয়াশ পরিমাণ হলুদ, স্বাদের জন্য মধু, এক টেবিল চামচ লেবুর রস মিশিয়ে প্রতিদিন সকালে খালি পেটে পান করতে পারেন। এতে পেট পরিষ্কার থাকবে।
ফেইস মাস্ক: ত্বক মসৃণ ও উজ্জ্বল করতে হলুদ দিয়ে তৈরি ফেইস মাস্ক ব্যবহার করতে পারেন।
এক টেবিল-চামচ দই, এক টেবিল-চামচের এক তৃতীয়াংশ হলুদ ও আধা টেবিল-চামচ মধু মিশিয়ে মাস্কটি তৈরি করতে হবে। মুখে মাখিয়ে শুকানো পর্যন্ত অপেক্ষা করে ধুয়ে ফেলতে হবে।
চুলকানি সারাতে: এক টেবিল-চামচের এক তৃতীয়াংশ পরিমাণ হলুদ, একই পরিমাণ অ্যালোভেরা জেল মিশিয়ে মিশ্রণ তৈরি করতে হবে। ত্বকে চুলকালি, কামড়ের দাগ, বিষাক্ত পাতা থেকে হওয়া প্রদাহ সারাতে মিশ্রণটি উপকারী। জ্বালাপোড়া করার জায়গায় মিশ্রণটি মাখিয়ে শুকানো পর্যন্ত অপেক্ষা করে ধুয়ে ফেলতে হবে।

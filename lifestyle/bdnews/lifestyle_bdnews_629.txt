রেসিপি দিয়েছেন নুরুন নাহার লিপি।
উপকরণ: মাশরুম ৫টি। বাঁধাকপি ১ কাপ। মুরগির মাংস ১ কাপ (কিউব করে কাটা)। আদাকুচি ১ চা-চামচ। রসুনকুচি ১ চা-চামচ। পেঁয়াজ বড় ১টি। ডিম ১টি। মাখন ১ টেবিল-চামচ। চিনি ১ চা-চামচ। গোলমরিচ-গুঁড়া স্বাদ মতো। সয়া সস ১ চা-চামচ। টমেটো সস ১ টেবিল-চামচ। ধনেপাতা ১ চা-চামচ।
পদ্ধতি: মাশরুম, পেঁয়াজ, বাঁধাকপি, মুরগির মাংস ও ধনেপাতা সিদ্ধ করে ব্লেন্ডারে ব্লেন্ড করে ছেঁকে নিন।
পাত্রে মাখন গরম করে তাতে আদা ও রসুন কুচি দিয়ে বেরেস্তা করে ব্লেন্ড করা মিশ্রণে ঢেলে দিন। পাঁচ মিনিট রান্না হওয়ার পর তাতে গোলমরিচ, সয়া সস, টমেটো সস ও চিনি দিয়ে নাড়তে থাকুন আরও কিছুক্ষণ। এবার ভালো ভাবে ফেটানো ডিম আস্তে আস্তে ঢেলে দিন।

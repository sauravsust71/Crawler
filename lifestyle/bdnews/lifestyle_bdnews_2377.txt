
উপকরণ
ছানা ২ কাপ। ডিম ৩টি। কনডেন্সড মিল্ক আধা কাপ। দুধ ১ কাপ। চিনি ৩-৪ কাপ বা স্বাদমতো। পাউরুটি ১ টুকরা। ভ্যানিলা ফ্লেইভার ১ টেবিল-চামচ।
ক্যরামেলের জন্য
পানি ২ টেবিল-চামচ। চিনি ৩ টেবিল-চামচ।
পদ্ধতি
প্রথমেই দুধে পাউরুটি ১০ মিনিট ভিজিয়ে রাখুন। এবার কাঁটাচামচ দিয়ে চেপে চেপে পাউরুটি দুধে ভালোভাবে মিশিয়ে দিন। চিনি, কন্ডেসড মিল্ক মেশান। চিনি গলে যাওয়া পর্যন্ত জোরে নাড়তে থাকুন।
ছানা দুধের মিশ্রণে কাঁটাচামচ দিয়ে মিশিয়ে দিন। অন্য একটি বাটিতে ডিমগুলো ফেটিয়ে নিন। এবার ডিম ছানার মিশ্রণে ঢেলে হাল্কা ভাবে মেশান। সবশেষে ভ্যানিলা ফ্লেইভার মিশিয়ে দিন।
যে বাটিতে পুডিং করবেন, তাতে ক্যারামেল করে নিন। মাঝারি আঁচে চিনি এবং পানি ফুটাতে থাকুন। চিনি গলে হাল্কা কমলা রং হলে চুলা থেকে নামিয়ে নিন। ক্যারামেল পুড়িয়ে ফেললে পুডিংয়ের স্বাদ খারাপ হবে।
এবার পুডিংয়ের মিশ্রণ ক্যারামেলের উপর ঢেলে দিন।
চুলায় করলে— একটি বড় হাঁড়িতে অল্প পানি দিয়ে তার উপর পুডিংয়ের বাটি বসিয়ে ভালোমতো ঢেকে দিন। ভারি কিছু দিয়ে ঢাকনা চাপা দিতে হবে যেন পানি পুডিংয়ের ভেতর না ঢুকতে পারে।
এবার বড় হাঁড়িও ঢেকে দিন। ৪০ থেকে ৪৫ মিনিট মাঝারি আঁচে রাখুন।

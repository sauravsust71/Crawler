মানব শরীরের সুস্থতা রক্ষায় আঁশজাতীয় খাবারের প্রয়োজনীয়তা সম্পর্কে জানান বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজের খাদ্য ও পুষ্টিবিজ্ঞান বিভাগের প্রধান ফারাহ মাসুদা।
তিনি বলেন, “খাদ্য আঁশ কোনো পুষ্টি উপাদান নয়, কিন্তু স্বাস্থ্য রক্ষায় সাহায্য করে।”
* খাওয়ার পর অন্যান্য খাবারের সঙ্গে খাদ্যআঁশ মিশে মল বৃদ্ধি করে ও কোষ্ঠকাঠিন্য দূর করে। মলাশয়ের নানা রকমের রোগ যেমন- অর্শ, কোলাইটিস ইত্যাদি প্রতিরোধে এটা সাহায্য করে।
* আঁশ রক্তে কোলেস্টেরলের মাত্রা কমায়।
* স্থূলকার ব্যক্তিদের জন্য আঁশজাতীয় খাবার বেশি উপযোগী। এটা দীর্ঘক্ষণ পেটে থাকে ফলে ক্ষুধা কম লাগে। এতে করে ওজন নিয়ন্ত্রণে থাকে।
* খাদ্যআঁশ রক্তে গ্লুকোজের মাত্রা কমায়। তাই ডায়াবেটিস আছে এমন ব্যক্তিদের জন্য খাদ্যআঁশ বেশ উপকারি।
* আঁশজাতীয় খাবার খাওয়ার মাধ্যমে নানা রকমের রোগ যেমন- পিত্তথলির রোগ, মলাশয়ের ক্যানসার, অর্শ, হৃদরোগ, স্থূলতা ইত্যাদি রোগের ঝুঁকি কমে।
সুস্থ ও প্রাপ্ত বয়স্কদের জন্য দৈনিক ৩০ গ্রাম খাদ্য আঁশ প্রয়োজন। শস্য, শাকসবজি ও খোসাসহ ফল থেকে আঁশ গ্রহণ করা ভালো। ডায়াবেটিস রোগীদের ক্ষেত্রে এর মাত্রা বাড়ানো প্রয়োজন। 
আঁশের সবচেয়ে ভালো উৎস হল- শস্য, শস্যজাতীয় খাবার, শাকসবজি, খোসাসহ ফল, ডাটা ইত্যাদি। এছাড়াও ডাল, বাদাম, বীজ, শুকনা ফল, জিরা, ধনে, মটরশুঁটি ইত্যাদিতে উল্ল্যেখযোগ্য পরিমাণে খাদ্যআঁশ পাওয়া যায়।   
বিভিন্ন খাদ্য থেকে প্রাপ্ত আঁশের পরিমাণ-

তেজপাতার স্বাস্থ্যগুণ এবং সৌন্দর্যবর্ধক উপাদান সম্পর্কে স্বাস্থ্যবিষয়ক এক ওয়েবসাইটে জানানো হয়, এই সুগন্ধি পাতা ‘পারফিউম’ তৈরিতে ব্যবহৃত হওয়ার পাশাপাশি, বিভিন্ন ঔষধি এবং ভেষধ উপাদানযুক্ত ‘বে লিফ এসেনসিয়াল অয়েল’য়েও ব্যবহৃত হয়।
সাধারণত তাজা এবং শুকনা এই দুই অবস্থায় পাওয়া যায় তেজপাতা। কিছু দোকানে তেজপাতার গুঁড়াও মেলে।
বিশেষজ্ঞদের মতে, তেজপাতায় থাকে ভিটামিন, মিনারেল এবং বিভিন্ন উদ্ভিজ্জ উপাদান যা ব্যাকটেরিয়া নিধন করা, জ্বালাপোড়া কমানোসহ আরও অনেক উপকার করে।
স্বাস্থ্যগুণ
হজমশক্তি বাড়ায়: তেজপাতা শরীরের হজমপ্রক্রিয়াকে দ্রুত করার মাধ্যমে খাবারের পুষ্টি উপাদানগুলো ভালোভাবে পরিপাক করতে সহায়তা করে। পেটফাঁপা, বদহজম, বুক জ্বালাপোড়া ইত্যাদির চিকিৎসায় তেজপাতা ব্যবহৃত হয়ে থাকে। তেজপাতার মাধ্যমে খাবারে ঝালভাব এনে রুচি বর্ধণ করার ক্ষেত্রে উপকারী।
সর্দিকাশি বা ফ্লু এড়াতে: তেজপাতার ‘অ্যান্টিমাইক্রোবায়াল’ উপাদান শ্বাসযন্ত্রের বিভিন্ন প্রদাহ কামাতে সহায়ক। সর্দিকাশি, কফ এবং ফ্লু থেকে মুক্তি পেতে তেজপাতা সিদ্ধ করে খেতে কিংবা বুকে মাখা যেতে পারে।
শরীরের বিভিন্ন ব্যথা কমাতে: শরীরের বিভিন্ন ধরণের ব্যথা বা ফুলে যাওয়া উপশম করতে তেজপাতার এসেনসিয়াল অয়েল বেশ উপকারী। ব্যথাযুক্ত জোড়া কিংবা টান পড়া মাংসপেশিতে তেলটি মালিশ করতে পারেন। মাথাব্যথা বা মাইগ্রেইনের ব্যথা সারাতেও এই তেল ব্যবহার করা যায়।


হৃদরোগের ঝুঁকি কমাতে: তেজপাতার মাধ্যমে ঝালভাব আনা স্বাস্থ্যকর রান্নার পদগুলো খাওয়ার মাধ্যমে কমতে পারে হৃদরোগের ঝুঁকি। কারণ তেজপাতায় থাকা বিভিন্ন অ্যান্টিঅক্সিডেন্ট উপদান স্বাস্থ্য এবং হৃদপিণ্ড ভালো রাখতে সাহায্য করে।
সৌন্দর্যবর্ধক গুণাবলী
তরুণ্যদীপ্ত ত্বক ধরে রাখতে: তেজপাতার উদ্ভিজ্জ উপাদান ত্বকে বলিরেখা সৃষ্টির জন্য দায়ি ‘ফ্রি র্যা ডিকেল’ নিষ্ক্রিয় করে। ঘরে সহজেই ‘অ্যান্টি-এইজিং সলিউশন’ বানাতে চাইলে তেজপাতা ভেজানো ফুটন্ত পানির বাষ্প মুখে লাগানো যেতে পারে।
শরীরের দুর্গন্ধ কমাতে: বিলাশবহুল এবং সুগন্ধিযুক্ত গোসলের জন্য মোটা অঙ্কের টাকা খরচ করার প্রয়োজন নেই। বরং এক টুকরা পরিষ্কার কাপড়ে গুঁড়া তেজপাতা কুসুম গরম পানিতে কিছুক্ষণ ডুবিয়ে রেখে, এই পানি দিয়ে গোসল করতে পারেন।
ত্বক ও দাঁত উজ্জ্বল করতে: তেজপাতা সিদ্ধ করুন। ঠাণ্ডা হলে তা দিয়ে মুখ ধুয়ে ফেলুন। ত্বক ফর্সা করার পাশাপাশি এই মিশ্রণ ব্রণ শুকাতেও সহায়ক। উজ্জ্বল দাঁত পেতে সপ্তাহে কয়েকবার দাঁতে তেজপাতা ঘষা যেতে পারে।
খুশকি এবং চুলপড়া কমাতে: তেজপাতা সিদ্ধ পানি দিয়ে চুল ধুলে খুশকি কমে। চুলপড়া বন্ধ করতেও কার্যকর। চুল কমে যাওয়ার স্থানগুলোতে তেজপাতার এসেনসিয়াল অয়েল মাখতে পারেন।
যষ্টিমধুর গুণ

দেওয়া হয় কাটা এবং আস্তমসলা। একেবারেই কম ঝাল এবং অসাধারণ স্বাদের এই বিরিয়ানির রেসিপি দিয়েছেন ফারহানা রহমান।
সুগন্ধি পানির উপকরণ
১ কেজি ওজনের মুরগি চামড়া ছাড়িয়ে ছয় টুকরা করা। ১২ কাপ গরম পানি। ২ টেবিল-চামচ আদাকুচি। ১ টেবিল-চামচ রসুনকুচি। আধা টেবিল-চামচ আস্ত গোলমরিচ। ১০,১২টি এলাচ। ২ টুকরা দারুচিনি। ১ চা-চামচ আস্তজিরা। ১ চা-চামচ মৌরি। ১০টি লবঙ্গ। ১ চা-চামচ জয়ত্রী। ৩টি তেজপাতা। স্বাদ মতো লবণ।
মুরগি রান্নার জন্য উপকরণ
আধা কাপ টকদই। ৪,৫টি আস্ত কাঁচামরিচ। স্বাদ মতো লবণ। আধা কাপ পেঁয়াজ-বেরেস্তা। ২ টেবিল-চামচ আদা ও রসুনবাটা। ১ কাপ দুধ। ১টি তেজপাতা। ৫টি এলাচ। ৬টি লবঙ্গ। ১ টুকরা দারুচিনি। ৬টি গোলমরিচ। ১ চা-চামচ জয়ত্রী শুকনা ভেজে হাত দিয়ে গুঁড়া করে নেওয়া। তেল পরিমাণ মতো। ৩ টেবিল-চামচ ঘি। ১ চা-চামচ চিনি।
চালের জন্য উপকরণ
৪ কাপ বাসমতি চাল ১ ঘণ্টা পানিতে ভেজানো (২৪০ মি.লি. এর কাপ)। ৭ কাপ আগে করে রাখা সুগন্ধি পানি। ৪,৫টি লবঙ্গ। লবণ স্বাদ মতো। ৪ টেবিল-চামচ ঘি। ৪টি আলুবোখারা। ১ মুঠো কিশমিশ।
অন্যান্য উপকরণ
কাজু-বাদাম। আধা কাপ পেঁয়াজ-বেরেস্তা। আস্ত ৪,৫টি কাঁচামরিচ। দুধে ভেজানো জাফরান।
পদ্ধতি
প্রথমে সগন্ধি পানির জন্য উপকরণগুলো একটি ছোট্ট পাতলা সুতির কাপড়ে বেঁধে ১২ কাপ পানিতে দিয়ে দিন। এবার এই পানি ফুটাতে হবে। অল্প পরিমাণে লবণ দিন। পানি ফুটে উঠলে মুরগির টুকরা দিয়ে না ঢেকে পানিতে আধা সিদ্ধ করুন। এরপরে মুরগি তুলে নিন। পানি মসলাসহ ফুটাতে থাকুন। যখন পানি কমে সাত কাপ হবে তখন চুলা বন্ধ করুন। মসলার পোটলা তুলে ফেলে দিন।
এবার মুরগির জন্য বেরেস্তা করুন। আধা কাপ তেলে পেঁয়াজ-বেরেস্তা করে তুলে নিন। মুরগি রান্নার জন্য যেসব উপকরণ বলা হয়েছে, সেখান থেকে তেল ও ঘি বাদে বাকি সব দিয়ে মুরগি মাখিয়ে নিন। এবার ওই তেলেই ঘি দিয়ে গরম করে মাখিয়ে নেওয়া মুরগি ছেড়ে দিন। ভালো মতো কষিয়ে রান্না করুন। লবণ চেখে দেখুন। একটু পানি দিয়ে রান্না করুন। হয়ে গেলে সামান্য ঝোলসহ নামান। কিছুটা কোরমার মতো হবে দেখতে।
এবার চালের জন্য একটি ননস্টিক বড় হাঁড়ি নিন। ঘি দিয়ে গরম করে পানি ঝরানো চাল দিন। সুগন্ধি পানিটুকু ঢেলে ঢেকে মাঝারি আঁচে রান্না করুন। লবণ চেখে দেখুন। লাগলে আরও লবণ দিন। পানি যখন প্রায় শুকিয়ে আসবে তখন কিশমিশ এবং আলুবোখারা দিয়ে নেড়ে মিশিয়ে দিন। সম্পূর্ণ পানি শুকিয়ে গেলে চুলা বন্ধ করুন।


বড় পাত্রে পানি ফুটিয়ে মাঝারি আঁচে ওই পাত্রের উপর বিরিয়ানির হাড়ি বসিয়ে দমে রাখুন। ৩০ মিনিট পর নামিয়ে গরম গরম পরিবেশন করুন ।
কিছু সাধারণ তথ্য
পেঁয়াজ-বেরেস্তা পুড়িয়ে ফেললে বিরিয়ানির স্বাদ নষ্ট হয়ে যাবে। ইচ্ছে করলে ঘিয়ের পরিমাণ আরও বাড়াতে পারেন। চাল সিদ্ধ করার সময় খুব বেশি যেন সিদ্ধ না হয় সেদিকে খেয়াল রাখুন। লবণের দিকে লক্ষ রাখতে হবে। যেহেতু তিনবারে লবণ দেওয়া হচ্ছে, তাই অল্প অল্প করে লবণ দিতে হবে।
সমন্বয়ে: ইশরাত মৌরি।
ঢাকাই বিরিয়ানি
ঝটপট বিরিয়ানি

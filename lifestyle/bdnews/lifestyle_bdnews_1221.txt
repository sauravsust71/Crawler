ঘাম শরীরের তাপমাত্রা নিয়ন্ত্রণের পদ্ধতি। গরমে শরীরের তাপমাত্রা বেড়ে গেলে তা স্বাভাবিক রাখতে ঘাম হয়। এছাড়াও ঘামের সঙ্গে শরীরের বিভিন্ন বর্জ্য পদার্থ বেরিয়ে যায়। গরমের দিনে ঘাম হওয়া অতি সাধারণ একটি বিষয়। তবে অতিরিক্ত ঘামের কারণে বিভিন্ন শারীরিক সমস্যা দেখা দিতে পারে।
ঘামের কারণ এবং এর বিভিন্ন ক্ষতিকর প্রভাব থেকে বাঁচার উপায় জানালেন কুর্মিটোলা জেনারেল হাসপাতালের মেডিসিন বিশেষজ্ঞ ডা. মোহাম্মদ কামরুল হাসান।
তিনি বলেন, “মানুষের শরীরে অ্যাক্রিন ও অ্যাপোক্রিন গ্ল্যান্ড নামক দুইধরনের ঘামগ্রন্থি থাকে। রোদের কারণে শরীরের তামপাত্রা বেড়ে যায়। তাই তাপমাত্রা স্বাভাবিক করে আনার জন্য ঘাম হয়। পাশাপাশি বিভিন্ন বিষাক্ত উপাদান বের করে দেওয়ার মাধ্যমে শরীরের ‘প্রটেকটিভ মেকানিজম’ হিসেবেও কাজ করে ঘাম।”
ঘাম হওয়ার আরও কারণ সম্পর্কে ডা. কামরুল হাসান বলেন, “গরম ছাড়াও পরিশ্রম, উত্তেজনা, দুশ্চিন্তা, ভয়, রাগ ইত্যাদি কারণেও ঘাম হতে পারে। ঠাণ্ডা পরিবেশেও অনেকের হাত ও পায়ের তালু ঘামার সমস্যা দেখা যায়। এ ধরণের রোগীদের মধ্যে বেশিরভাগই থাইরয়েড গ্রন্থি কিংবা বিভিন্ন হরমোনজনিত সমস্যায় আক্রান্ত। এক্ষেত্রে ডাক্তারের পরামর্শ নেওয়াই বুদ্ধিমানের কাজ হবে।”
“নিউরোলজিকাল ও ঘুমের ওষুধ সেবন করলে কিংবা রক্তে শর্করার পরিমাণ কমে যাওয়ার কারণেও ঘাম হতে পারে।” বলেন ডা. কামরুল।
ঘামের অপকারীতা সম্পর্কে তিনি জানান, শরীরে ব্যাকটেরিয়া ছড়ানোর অন্যতম মাধ্যম হল ঘাম। ঘামাচি, চুলকানি, ব্রণ ইত্যাদি তো আছেই। তাছাড়া শরীরের ক্ষতস্থানেও ঘামের মাধ্যমে ব্যাকটেরিয়ার সংক্রমণ হতে পারে।
এছাড়া অতিরিক্ত ঘামের কারণে শরীর থেকে প্রচুর পানি বেরিয়ে যায়, ফলে পানিশূন্যতায় আক্রান্ত হওয়ার সম্ভাবনা থাকে।
ঘামে ভেজা পোশাক দীর্ঘ সময় পরে থাকার কারণে ছত্রাকের সংক্রমণ হওয়ার সম্ভাবনা থাকে। শরীর ঘামে ভেজা থাকলে ত্বকে ধুলাবালি আটকে থাকার পরিমাণও বেড়ে যায়। 
ঘামের কিছু উপকারী দিকও আছে। এ সম্পর্কে ডা. কামরুল হাসান বলেন, “ঘামে শরীর ভেজা থাকার কারণে হিট স্ট্রোকের সম্ভাবনা কমে যায়। পাশাপাশি শরীর থেকে অতিরিক্ত লবণ বের করে দিয়ে লবণের ভারসাম্য বজায় রাখে। পরিষ্কার রাখে ত্বকের লোপকুপগুলো।”
অতিরিক্ত ঘাম নিয়ন্ত্রণে করণীয়
অতিরিক্ত ঘাম হলে প্রচুর পানি পান করতে হবে। ফলের শরবত, গ্লুকোজ, স্যালাইন ইত্যাদি পান করাও বেশ উপকারী। তাছাড়া বাইরের গরম থেকে ঘরে ফিরে ঠাণ্ডা পানি না খেয়ে স্যালাইন বা গ্লুকোজ খাওয়ার পরামর্শ দিলেন ডা. কামরুল হাসান।
চা-কফি, ভাজাপোড়া-জাতীয় খাবার যতটা সম্ভব কম খাওয়াই ভালো।
গরমের মৌসুমে পরিষ্কার পরিচ্ছন্নতার উপর বিশেষ নজর দিতে হবে। এক্ষেত্রে একাধিকবার গোসল করা যেতে পারে। তবে ঠাণ্ডা লেগে যাওয়ার ভয় থাকলে ভেজা কাপড় দিয়ে শরীর মুছে নেওয়া যেতে পারে।
গরমে বাইরে থেকে ফিরেই অতিরিক্ত ঠাণ্ডা স্থানে বসা, অতিরিক্ত ঠাণ্ডা পানীয় পান করা বা গোসল না করার পরামর্শ দেন ডা. কামরুল।
তিনি বলেন, “বাইরে থেকে এসে কিছুটা সময় জিরিয়ে নিতে হবে। এতে শরীরের তাপমাত্রা স্বাভাবিক হওয়ার সুযোগ পাবে। অন্যথায় ঠাণ্ডা লেগে যেতে পারে।”
এছাড়া ঘামে ভেজা জামাকাপড় বেশিক্ষন পরে থাকা উচিত নয়। গরমের দিনে হালকা রংয়ের পোশাক বেশি আরামদায়ক। ঘামের দুর্গন্ধ এড়াতে পারফিউম ব্যবহার করা যেতে পারে। আর ঘামাচি থেকে বাঁচতে ট্যালকম পাউডার ব্যবহার করা যেতে পারে।
ঘাম কমানোর উপায়

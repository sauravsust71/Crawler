আমাদের দেশে কুকুর পোষা প্রাণী হিসেবে বেশ পরিচিত হলেও শহুরে এলাকায় এর সংখ্যা কম। তবে দিন দিন অনেকেই ঘরের সদস্য হিসেবে কুকুর বেছে নিচ্ছেন। তবে একটি কুকুরকে ঘরে নিয়ে এলেই শেষ নয়। সঠিকভাবে তার পরিচর্যা করতে হয়।
পোষাপ্রাণী-বিষয়ক একটি ওয়েবসাইটের প্রতিবেদনে কুকুর পোষার আগে কী কী পূর্ব-প্রস্তুতি নেওয়া দরকার, সে বিষয়ে জানানো হয়।
- ঘরে কুকুর আনার আগে পরিবারের সবার মতামত নিন। কারও আপত্তি আছে কিনা বা কুকুরের কারণে কারও কোনো ধরনের অ্যালার্জির সমস্যা আছে কিনা- এই সব বিষয়ে জেনে নিতে হবে। পাশাপাশি বাড়িওয়ালা এবং প্রতিবেশীদের মতামতও জেনে নেওয়া জরুরি।
- পোষা প্রাণীর যত্ন নেওয়ার মতো পর্যাপ্ত সময় আছে কিনা সেই বিষয়টিও মাথায় রাখতে হবে। একটি ছোট শিশুর মতোই একটি কুকুরের যত্ন নেওয়া জরুরি। তাকে সময় দিতে হয় এবং প্রশিক্ষণ দিতে হয়। এর জন্য যে আলাদা সময় দরকার তা করতে পারবেন কিনা সেই বিষয়েও খেয়াল রাখতে হবে।
- নতুন একটি ছোট কুকুরকে ঘরে আনতে চাইলে তাকে গোড়া থেকে প্রশিক্ষণ দিতে হবে। তাই এই সময়ে ধৈর্য্য ধরে অপেক্ষা করতে হবে এবং কুকুরটির পিছনে বেশ কিছুটা সময় ব্যয় করতে হবে। এসব ব্যাপারে ধৈর্য আছে কিনা সেই বিষয়ও খেয়াল রাখতে হবে। না হলে বড় কুকুর বেছে নেওয়াই হবে বুদ্ধিমানের কাজ।
- প্রতিটি কুকুরই দিনের কিছুটা সময় বাইরে খেলাধুলা ও হাঁটাতে নিয়ে যেতে হয়। আকার অনুযায়ী আধা ঘণ্টা থেকে দিনে সম্ভব হলে দু’বার এদের খেলার সময় দিতে হয়। এই সময়ও দিতে পারবেন কিনা তা বুঝে নিতে হবে।
- ঘর এবং আশপাশের পরিবেশ কুকুরটির জন্য উপযোগী কিনা, সেটা খেয়াল করুন। বড় আকারের কুকুরের জন্য প্রচুর জায়গা প্রয়োজন। তাই অ্যাপার্টমেন্টে পালার জন্য ছোট আকারের কুকুর বেছে নেওয়াই ভালো।
- একটি কুকুর পালা অনেক ক্ষেত্রেই বেশ ব্যবহুল। এর জন্য বিশেষ খাবার, খেলনা, ভ্যাক্সিন এবং ডাক্তার দেখানো সব কিছুই বেশ খরচ সাপেক্ষ। তাই এই ব্যয় বহন করতে আপনি সক্ষম কিনা সেগুলোও চিন্তা করতে হবে আগেই।


- কুকুর না বুঝেই সব কিছু মুখে দেয়। তাই ক্ষতিকর জিনিস কুকুরের নাগালের বাইরে রাখতে হবে।
- ঠিক মতো প্রশিক্ষণ না দিলে কুকুর যেখানে সেখানে ‘বাথরুম’ করতে পারে। এছাড়া কুকুর অনেক ক্ষেত্রেই চিবিয়ে জিনিস নষ্ট করে। এসব দুর্ঘটনার জন্য আগে থেকেই মানসিক প্রস্তুতি নিয়ে রাখা দরকার।
- কাজের কারণে যদি দুএকদিন ঘর থেকে দূরে থাকতে হয় তাহলে আপনার কুকুর কোথায় থাকবে সেই ব্যবস্থাও আগে করে নিতে হবে। কারণ কুকুরটিতে তো আর একা ঘরে রেখে যাওয়া যাবে না।

স্বাস্থ্যবিষয়ক ওয়েবসাইটে প্রকাশিত এক প্রতিবেদনে জানানো হয়, প্রচণ্ড গরম থেকে সর্দিগর্মি, জ্বর হতে পারে। আবার শরীরের স্বাভাবিক তাপমাত্রা নিয়ন্ত্রণ হারিয়ে অচেতনও হয়ে যেতে পারে। এই সমস্যা থেকে পরিত্রাণের জন্য প্রাচীনকাল থেকেই ভারত অঞ্চলে বিভিন্ন ধরনের পানীয় প্রচলিত আছে।
এগুলো পান করলে ‘হিট স্ট্রোক’য়ের ঝুঁকি কমায়।
কাঁচাআমের শরবত: গরমে কাঁচাআমের জুস খুবই উপকারী। এটা শরীর ঠাণ্ডা রাখে এবং ‘হিট স্ট্রোক’য়ের ঝুঁকি কমায়। 
‘বাটারমিল্ক’ বা মাঠা: আয়ুর্বেদবিদ্যায় বলা হয় প্রতিদিন এক গ্লাস মাঠা খাওয়া শরীরের জন্য উপকারী। এটা প্রোবায়োটিক্সয়ের ভালো উৎস এবং গরমে পানি স্বল্পতা দূর করতে সাহায্য করে। তাই গরমের অস্বস্তি থেকে বাঁচতে প্রতিদিনের খাবার তালিকায় মাঠা যুক্ত করুন। 
তেঁতুলের শরবত: সুস্বাদু এই শরবত সবাই খুব পছন্দ করে। তাছাড়া গরমে এই শরবত খাওয়া সবচেয়ে বেশি উপযোগী। ভিটামিন এবং ইলেক্ট্রোলাইট সম্পন্ন তেঁতুল গরমে শরীর ঠাণ্ডা রাখার পাশাপাশি পেটের নানান ধরনের সমস্যা সমাধান করে। 
পেঁয়াজের রস: ‘হিট স্ট্রোক’ থেকে বাঁচার অন্যতম ভালো উপায় হল পেঁয়াজের রস। অনেকেই এর ঝাঁজালো স্বাদ পছন্দ করে না। তবে এর রস ঔষধি গুণাগুণ সম্পন্ন। পেঁয়াজের রস গরম থেকে বাঁচাতে সাহায্য করে। 
ধনিয়া ও পুদিনার শরবত: এই প্রাকৃতিক উপাদানে তৈরি শরবত গরমে শরীর ঠাণ্ডা রাখে। যা উন্নতমানের ‘ডিটক্সিফাইইয়িং’ বা শরীরের দূষিত পদার্থ বের করে দেওয়ার জন্য উপযুক্ত পানীয়। খালি পেটে পান করলে তা শরীরের জন্য বেশি ভালো।  
অ্যালোভেরার শরবত: রোদপোড়া থেকে বাঁচতে অ্যালোভেরার শরবত অনন্য। এটা হজমে সাহায্য করে, বুক জ্বালাপোড়া কমায় এবং পেটের নানারকম সমস্যা দূর করে। গরমে প্রতিদিন এক গ্লাস অ্যালোভেরার শরবত পান করলে ‘স্ট্রোক’য়ের ঝুঁকি কমে আসে।

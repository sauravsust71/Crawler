গুড়ের স্বাস্থ্যগুণ সম্পর্কে জানিয়েছেন ভারতের ‘ম্যাক্স হেলথকেয়ার ইনস্টিটিউট’য়ের জ্যেষ্ঠ পুষ্টিবিদ মানজারি চন্দ্র।
প্রাকৃতিক পরিষ্কারক: শ্বাসনালী, ফুসফুস, অন্ত্র, পাকস্থলী এবং খাদ্যনালী অত্যন্ত কার্যকরভাবে পরিষ্কার করতে পারে গুড়। তাই দুষিত পরিবেশে কাজ করেন এমন কর্মী যেমন কারখানা কিংবা কয়লা খনির শ্রমিকদের গুড় খাওয়ার পরামর্শ দেওয়া হয়। এতে থাকে প্রচুর লৌহ, ফলে রক্তশুন্যতা সারাতে গুড় আদর্শ প্রাকৃতিক সমাধান। পাশাপাশি এতে রক্ত সঞ্চালনের উন্নতি হয়।
রয়েছে ‘অ্যান্টি-অ্যালার্জিক’ উপাদান। যা অ্যালার্জির উপাদান থেকে রক্ষা করে শ্বাসযন্ত্র সুস্থ রাখতে সাহায্য করে। কাশি ও হাঁপানির সমস্যা দূর করতে সাহায্য করে।
রক্ত পরিশোধণ: সামন্য পরিমাণে প্রতিদিন গুড় খেতে পারলে রক্ত পরিষ্কার হয়। ফলে দেহ সুস্থ রাখার পাশাপাশি ত্বকের কালচে দাগ দূর করে, ব্রণ প্রতিহত করে এবং বলিরেখা পড়তে দেয় না।


ওজন কমাতে: যেখানে অন্যান্য মিষ্টিতে বাড়ে মেদ, সেখানে গুড় ওজন কমাতে সহায়তা করে। পটাশিয়ামের উৎকৃষ্ট উৎস গুড়। এই খনিজ উপাদান শরীরে ইলেক্ট্রোলাইটের ভারসাম্য বজায় রাখতে সাহায্য করে। ফলে পেশি গঠন দ্রুত হয়, বিপাকীয় ক্ষমতা বাড়ায়। পটাশিয়াম শরীরে পানি ধারণের মাত্রা কমিয়ে ওজন নিয়ন্ত্রণেও সাহায্য করে। 
সর্দি-কাশির চিকিৎসা: গুড়ে রয়েছে অ্যান্টিঅক্সিডেন্ট এবং দস্তা ও সেলেনিয়াম’য়ের মতো খনিজ উপাদান। যা শরীরের মুক্ত-মৌলজনীত ক্ষয় কমায় এবং রোগ সংক্রমণের বিরুদ্ধে রক্ষাকবচ তৈরি করে। কুসুম গরম পানিতে গুড় গুলে পান করতে পারেন। চায়ে চিনির পরিবর্তেও গুড় ব্যবহার করতে পারেন।
হাঁপানির চিকিৎসায়: গুড়ের অ্যালার্জিরোধী উপাদান হাঁপানির চিকিৎসায় আদর্শ। পাঁচ চা-চামচ হলুদ, এক চা-চামচ মাখন এবং ছোট্ট এক টুকরা গুড় একসঙ্গে মিশিয়ে নিন। হাঁপানির কষ্ট কমাতে এই মিশ্রণ প্রতিদিন তিন থেকে চারবার খেতে হবে।


আরও পড়ুন
ঘি খাওয়ার উপকারিতা  
বাদামি চিনি বনাম সাদা চিনি  

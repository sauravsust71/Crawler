রেসিপি দিয়েছেন আনার সোহেল।
পালং সবজি
উপকরণ: পালংশাক এক আঁটি (কুচি করে কাটা)। মিষ্টি কুমড়া আধা কাপ (স্লাইস করে কাটা)। কচি ঢ্যাঁড়স ৫,৬টি (২ ভাগ করা)। আলুকুচি ১টি। এছাড়া আরও পছন্দ মতো সবজি যোগ করতে পারেন। লবণ স্বাদ মতো। পেঁয়াজকুচি ১টি। রসুন-ছেঁচা ৫,৬টি কোয়া। আদাবাটা ১ চা-চামচ। মরিচগুঁড়া আধা চা-চামচ। হলুদগুঁড়া আধা চা-চামচ। জিরাগুঁড়া আধা চা-চামচ। কাঁচামরিচ-ফালি ৪টি। তেল ২,৩ টেবিল-চামচ। ধনেপাতা-কুচি।
পদ্ধতি: পালংশাক ভালো করে ধুয়ে পানি ঝরিয়ে রাখুন। পানিতে ভিজিয়ে রাখুন কিছুক্ষণ। তারপর ভালো করে ধুয়ে দুভাগ করে কেটে নিন। মিষ্টি-কুমড়া ও আলু কেটে ধুয়ে নিন।


সবজি একটু নরম হয়ে আসলে শাক মিশিয়ে দিয়ে কয়েক সেকেন্ড পর অল্প পানিসহ ঢেকে দুই মিনিট রান্না করুন।
ভাজা ভাজা হয়ে আসলে ধনেপাতা-কুচি দিয়ে নেড়ে নামিয়ে পরিবেশন করুন।
আলু-শিং মাছের তারকারি
উপকরণ: শিং মাছ আধা কেজি (মাঝারি আকার)। আলু ২টি (বড় ৪ ভাগ করা)। টমেটো ১টি কুচি। পেঁয়াজ ১টি কুচি। পেঁয়াজ বাটা ২ চা-চামচ। মরিচগুঁড়া ১ চা-চামচ। হলুদগুঁড়া আধা চা-চামচ। জিরাগুঁড়া ১ চা-চামচ। লবণ স্বাদ মতো। গরম পানি ২,৩ কাপ। কাঁচামরিচ ২,৩টি। তেল ২,৩ টেবিল-চামচ। ধনেপাতা-কুচি পরিমাণ মতো।
পদ্ধতি: শিং মাছ ভালো ভাবে পরিষ্কার করে ছোট টুকরা করুন। লবণ, হলুদ দিয়ে ভালো করে মেখে ধুয়ে নিন। পানি ঝরিয়ে রাখুন।
এখন প্যানে তেল গরম করে, আলু দিয়ে হালকা ভেজে তুলুন। আরও অল্প তেল যোগ করে পেঁয়াজকুচি দিয়ে নাড়ুন। তারপর পেঁয়াজবাটা দিয়ে কিছুক্ষণ নেড়ে, টমেটো কুচি দিন। একে একে গুঁড়ামসলা, লবণ ও অল্প গরম পানি দিয়ে কষান।



৬ মে আনুষ্ঠানিক উদ্বোধনের পর এ সড়ক এখন পর্যটকদের জন্য খোলা। তাই কক্সবাজারের সবচেয়ে আকর্ষণীয় ভ্রমণ গন্তব্য এই মনমাতানো ৮০ কিলোমিটার মেরিন ড্রাইভ সড়ক।
কক্সবাজার শহরের কলাতলী থেকে সামান্য কিছু জায়গায় মেরিন ড্রাইভ সাগর ছুঁতে পারেনি। তাই মেরিন ড্রাইভের আসল স্বাদ পাওয়া যাবে দরিয়ানগর থেকে। এখান থেকে টেকনাফ সমুদ্র সৈকত পর্যন্ত একটানা চলে গেছে এ সড়ক।  মেরিন ড্রাইভে ভ্রমণের শুরুতে যাত্রা বিরতি নিন হিমছড়ি।
হিমছড়ির পাহাড় চূড়া থেকে পাখির চোখে দেখা যায় এই রাস্তা। আর দিগন্ত বিস্তৃত নীল সমুদ্রও দেখা যায় এখান থেকে।
এবার চলুন সামনের দিকে। হিমছড়ি পর্যটন কেন্দ্র ছেড়ে প্রায় একশ গজ সামনের সমুদ্র সৈকত ভাটার সময় দখল করে নেয় লাল কাঁকড়ার দল। কিছুটা পথ চলার পর নিসর্গের হাতছানিতে হয়ত থমকে দাঁড়াবেন। এখানে রেজুখাল এলাকাটি ছবির মতো।
রেজুখাল আর সমুদ্রের মিলনস্থলের একপাশে সারি সারি সুপারি গাছ, অন্যপাশে ঝাউবন। দুই পাশে আছে শ্বাসমূলীয় বন। এখান থেকে বেশি দূরে নয় ইনানী সমুদ্র সৈকত। ভাটার সময় ইনানীর সৈকতে জেগে ওঠা মৃত প্রবালের মাঝে দুদণ্ড সময় ভালোই লাগবে।


কক্সবাজার থেকে প্রায় চল্লিশ কিলোমিটার দূরে মেরিন ড্রাইভের চমৎকার একটি জায়গা শামলাপুর। এখানকার সমুদ্র সৈকতটি বেশ সুন্দর ও নির্জন। শামলাপুর থেকে প্রায় চার কিলোমিটার সামনে আরেকটি সৈকত হাজামপাড়া। এটি কিন্তু নির্জন নয়। এখানে রংবেরংয়ের মাছ ধরার নৌকা আর কর্মব্যস্ত জেলেদের আনাগোনা দেখা যায়।
হাজামপাড়া থেকে প্রায় পাঁচ কিলোমিটার সামনে জায়গার নাম শিলখালি। এখানে মেরিন ড্রাইভের একপাশে শিলখালি গর্জন বন। বিশাল বিশাল এ গর্জন বিন গেম রিজার্ভের অংশ। অন্য পাশটায় শিলখালি সমুদ্র সৈকত। এই জায়গাকে অনেকে সেন্টমার্টিন বলে ভুল করতে পারেন।
হাজাম পাড়া থেকে সামনে মেরিন ড্রাইভের আরেকটি সুন্দর জায়গা দরগার ছড়া। এখানে সড়ক লাগোয়া রঙিন জেলে নৌকা আর পাহাড়ের সৌন্দর্য মিলে ভিন্ন আমেজ তৈরি করেছে।
দরগাছড়া থেকে টেকনাফ সমুদ্র সৈকত বেশি দূরে নয়। মাত্র পনের কিলোমিটার। বাংলাদেশের সবচেয়ে পরিচ্ছন্ন এবং নির্জন সৈকতগুলোর একটি। টেকনাফ সমুদ্র সৈকতের রঙিন জেলে নৌকাগুলো ছবির মতো।
টেকনাফের এ সৈকতে এসেই শেষ হয়েছে দীর্ঘ ৮০ কিলোমিটারের মেরিন ড্রাইভ। বাংলাদেশের সবচেয়ে নবীন ভ্রমণ গন্তব্য।


কীভাবে যাবেন
প্রথমে কক্সবাজার। ঢাকা থেকে সড়ক ও আকাশপথে সরাসরি কক্সবাজার যাওয়া যায়। ঢাকার ফকিরাপুল, কমলাপুর ও সায়দাবাদ থেকে কক্সবাজার যায় সোহাগ পরিবহন, সেন্টমার্টিন সার্ভিস, দেশ ট্রাভেলস, শ্যামলী পরিবহন, হানিফ এন্টারপ্রাইজ, সিল্কলাইন সার্ভিস, রিল্যাক্স ট্রান্সপোর্ট, গ্রীন লাইনসহ আরও কিছু পরিবহনের এসি বাস। ভাড়া ১ হাজার ৬শ’ থেকে ২ হাজার টাকা।
এছাড়া শ্যামলী পরিবহন, হানিফ এন্টারপ্রাইজ, এস আলম সার্ভিস, ইউনিক সার্ভিস, এনা পরিবহনের নন এসি বাসে ভাড়া ৮শ’ টাকা।  
ঢাকার হযরত শাহজালাল বিমানবন্দর থেকে বিমান বাংলাদেশ, নভো এয়ার, ইউএস বাংলা এয়ার এবং রিজেন্ট এয়ারের বিমান নিয়মিত কক্সবাজার যায়। একপথের ভাড়া ৪ হাজার টাকা থেকে শুরু।
কোথায় থাকবেন
এ ভ্রমণে থাকতে পারেন কক্সবাজার শহরে। শহরে বেশ কিছু হোটেল রিসোর্ট আছে। সমুদ্র সৈকতের কাছাকাছি কিছু হোটেল রিসোর্ট হলো- কলাতলী সমুদ্র সৈকতে হোটেল সি ক্রাউন (০৩৪১-৬৪৭৯৫, ০১৮১৭ ০৮৯৪২০, ভাড়া ৪ হাজার টাকা থেকে শুরু)। সুগন্ধা সৈকতে হোটেল প্রাসাদ প্যারাডাইস (০১৭৩৬১১২২৩৩, ভাড়া ৪ হাজার টাকা থেকে শুরু)। লাবনী সৈকতে হোটেল মিডিয়া ইন্টারন্যাশনাল (০১৭১১৩৪১১৬৪, ভাড়া ২ হাজার টাকা থেকে শুরু)। হোটেল মোটেল জোনে হোটেল সি গাল (০৩৪১-৬২৪৮০, ০১৭৬৬৬৬৬৫৩০, ভাড়া ৬ হাজার টাকা থেকে শুরু), হোটেল কক্স টুডে (০৩৪১-৫২৪১০-২২, ০১৭৫৫৫ ৯৮৪৪৯, ভাড়া ৭ হাজার টাকা থেকে শুরু)। কলাতলী সৈকতে সায়মন বিচ রিসোর্ট (০৯৬১০৭৭৭৮৮৮, ০১৭৫৫৬৯১৯১৭ ভাড়া ৭ হাজার ৬শ’ টাকা থেকে শুরু)।
তবে মেরিন ড্রাইভে তিনটি আকর্ষণীয় রিসোর্ট আছে। এ ভ্রমণে এসব রিসোর্টে অবস্থান করলে ভ্রমণটা উপভোগ্য হবে।
সেগুলো হল- ইনানীর প্যাঁচার দ্বীপে মারমেইড ইকো রিসোর্ট (০১৮৪১৪১৬৪৬৪, ভাড়া ৩ হাজার ৫শ’ টাকা থেকে শুরু)। ইনানীর জালিয়াপালং’য়ে এ রয়েল টিউলিপ বিচ রিসোর্ট (০৩৪১-৫২৬৬৬-৮০, ভাড়া ৭ হাজার টাকা থেকে শুরু)। টেকনাফ সমুদ্র সৈকতে সেন্ট্রাল রিসোর্ট (০১৭১১৫৩৪২০৫, ভাড়া ৩ হাজার টাকা থেকে শুরু)।


মেরিন ড্রাইভে ভ্রমণের জন্য কক্সবাজার শহর থেকে খোলা জিপ ভাড়ায় নিতে পারেন। সারাদিন ভ্রমণের জন্য একটি জিপের ভাড়া পড়বে ৪ হাজার থেকে ৬ হাজার টাকা। দশজন ভালোভাবে ভ্রমণ করা যায় এসব জিপে। এছাড়া দলে লোক অনুযায়ী সেডান কার, মাইক্রো বাস কিংবা অটো রিকশাও ভাড়া নিতে পারেন।
কক্সবাজার শহরের রেন্ট-এ-কারগুলোতে সেডান কারের সারাদিনের ভাড়া ৩ হাজার ৫শ’ থেকে ৪ হাজার টাকা। মাইক্রো বাস ৪ হাজার থেকে ৬ হাজার টাকা এবং অটো রিকশার ভাড়া ২ হাজার থেকে ২ হাজার ৫শ’ টাকা।
প্রয়োজনীয় তথ্য

স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে এমনই কিছু সমস্যার ঘরোয়া সমাধানগুলো উল্লেখ করা হয়। এই প্রতিবেদনে সমাধানগুলো তুলে ধরা হল।
নখের কোণায় ফাঙ্গাস
বর্ষার মৌসুমে নোখের কোণায় ফাঙ্গাস বেশ সাধারণ একটি সমস্যা। অনেক সময় এর ওষুধ হাতের কাছেও পাওয়া যায় না। কিন্তু এই সমস্যাটির সহজ সমাধান হতে পারে মাউথওয়াশ। প্রতিদিন দুবার যে কোনো মাউথওয়াশে পায়ের আক্রান্ত অংশ ১৫ থেকে ২০ মিনিট ভিজিয়ে রাখলেই কিছুদিনের মধ্যেই এই সমস্যা দূর করা সম্ভব। মাউথওয়াশে থাকা অ্যান্টিসেপটিক উপাদান নখের ফাঙ্গাস দূর করতে সাহায্য করে।


চিনি বিহীন দইয়ে থাকা ব্যাক্টেরিয়া মুখের দুর্গন্ধ সৃষ্টিকারী ব্যাক্টেরিয়া দূর করতে সাহায্য করে।
একজিমা দূর করতে অলিভ অয়েল
ভিটামিন ই সমৃদ্ধ অলিভ অয়েল ত্বক নমনীয় করতে সাহায্য করে। গোসলের পর হালকা ভেজা ত্বকে এক্সট্রা ভার্জিন অলিভ অয়েল ব্যবহার একজিমা দূর করতে সাহায্য করে।
হেচকি রোধে চিনি
হেচকি বেশ বিরক্তিকর সমস্যাই বটে। পরিত্রাণ পেতে এক চামচ চিনিই যথেষ্ট। গবেষণায় দেখা গেছে হঠাৎ জিহ্বায় মিষ্টি স্বাদ স্নায়ূ শীতল করে হেচকি দূর করতে সাহায্য করে।


গবেষণায় দেখা গেছে, পেপারমিন্ট বা দারুচিনির গাম চিবানোর ফলে ২০ শতাংশ অবসাদ দূর করা সম্ভব। গবেষণায় জানা যায়, ওই ফ্লেভার সমৃদ্ধ চুইংগাম খাওয়ার ফলে একজন মানুষের ৩০ শতাংশ সচেতনতা বৃদ্ধি পায়, অন্যদিকে তাদের দুশ্চিন্তা করার পরিমানও ২৫ শতাংশ হ্রাস পায়।
আঁচিল সরাতে ডাক্ট টেপ
অবাক হলেও সত্যি! ডাক্ট টেপ দিয়ে আঁচিল দূর করা সম্ভব। আর এর সফলতার হার শতকরা ৮৫ ভাগ।
আঁচিলের উপর ডাক্ট টেপ লাগিয়ে রাখুন এক সপ্তাহ। তারপর টেপ খুলে আক্রান্ত জায়গায় ঝামা পাথর দিয়ে ঘষে নিন। যতদিন আঁচিল না যায় ততদিন এই পদ্ধতি চালিয়ে যেতে হবে।


মাঝে মধ্যে মুখের চোয়ালে চাপের কারণে মাথাব্যথা হয়ে থাকে। একটি পেন্সিল দাঁতর মাঝে চেপে ধরে রাখলে চোয়ালের পেশিগুলো শিথিল হয়। আর এতে মাথাব্যথা কিছুটা দূর হয়।
সি সিকনেস দূর করতে লেবু এবং জলপাই
সমুদ্র যাত্রার সময় অনেকেরই শারীরিক নানা সমস্যা হতে দেখা যায়। এক্ষেত্রে বমি হওয়া বা বমি বমিভাব হওয়ার সমস্যা দেখা দেয়। এ সমস্যা দূর করতে লেবুর রস বা জলপাই বেশ কার্যকর। লেবু এবং জলপাইয়ে আছে ট্যানিন যা বমিভাব দূর করতে সাহায্য করে।
ব্রণের প্রতিকারে টমেটো
ত্বকে ব্রণের সমস্যা হলে একটি টমেটো ছেঁচে রস ব্রণের উপর পুরো ত্বকে ছড়িয়ে এক ঘন্টা অপেক্ষা করতে হবে। দিনে এক বার করে টানা এক সপ্তাহ এই রস ব্যবহারের ফলে ব্রণের সমস্যা দূর করা সম্ভব।
টমেটোতে আছে প্রচুর ভিটামিন সি এবং এ, অ্যান্টিঅক্সিডেন্ট এবং অ্যাসিডিক উপাদান যা ত্বক সুস্থ করতে দারুণ উপকারী।
 
ছবি: রয়টার্স।
 
পেট খারাপে ঘরোয়া টোটকা
ঘরোয়া উপায়ে ‘ব্ল্যাক হেডস’ দূর
ঘরোয়া উপায়ে গ্যাস্ট্রাইটিস নিরাময়

ব্যস্ততা, ঠাণ্ডা, মাইগ্রেইন, সাইনাস ইত্যাদি বিভিন্ন কারণে মাথাব্যথা হতে পারে।
স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটের প্রতিবেদন অনুসারে আদা, দারুচিনি, লবঙ্গ ইত্যাদি বিভিন্ন মাথাব্যথা দূর করতে সাহায্য করে। ওই দিকগুলো এখানে তুলে ধরা হল।
আদা: মস্তিষ্কের রক্তনালীর প্রদাহ দূর করতে কার্যকর একটি উপাদান আদা। তাই মাথাব্যথার সময় আদা খেলে আরাম পাওয়া যায়। যারা নিয়মিত মাথাব্যথার সমস্যায় ভোগের তারা আদার রস এবং লেবুর রস সমপরিমাণে মিশিয়ে দিনে দু’বার সেবন করতে পারেন। তাছাড়া পানিতে আদা ফুটিয়ে ওই ভাপ টেনে নিলে আরাম পাওয়া যাবে।
দারুচিনি: দারুচিনি মাথাব্যথা দূর করতে বেশ কার্যকর। খানিকটা দারুচিনি গুঁড়ার সঙ্গে পানি মিশিয়ে পেস্ট তৈরি করে নিতে হবে। ওই পেস্ট মাথার দুপাশে লাগিয়ে ৩০ মিনিট অপেক্ষা করুন। এরপর কুসুম গরম পানি দিয়ে ধুয়ে ফেলুন।

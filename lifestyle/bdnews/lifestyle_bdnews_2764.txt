সেখান থেকে বের হয়ে আসার চেষ্টা করতে হবে দুজনকেই।
আজকাল সংসার একঘেয়ে লাগে। কোনো বিষয় নিয়ে তিক্ততা কাজ করছে যেন রোমানা ও শিহাবের মাঝে। রোমানার কেবলই মনে হয় যে জীবনসঙ্গীর সঙ্গে দূরত্ব বাড়ছে। সারাদিন দুজনেই কর্মক্ষেত্রে বাইরে থাকে। রাতে বাসায় এসে বিছানায় যেতেই দুজনের সেই তীক্ততা। কী করবে রোমানা ভেবে পায়না।
উপরের ঘটনাটা সত্যি। শুধু নাম বদলে দেওয়া হয়েছে। আরেকটা ঘটনা হচ্ছে এরকম— অনেকদিন সংসার করার পর রফিকের মনে হচ্ছে স্ত্রীর সোহানার সঙ্গে ঠিক আগের মতো বনিবনা হচ্ছে না। চাকরি করেন না সোহানা। সাত বছরের সংসার জীবনে গৃহিনী হিসেবেই জীবন পার করছেন। একঘেয়ে জীবনে মাঝে মাঝে রোমাঞ্চ চায় সে। তবে স্বামীর সঙ্গে এই বিষয় কথা বলতে গেলেই লাগে ঝগড়া। তাই সন্তানকে স্কুল থেকে আনা নেওয়া আর রান্নাবান্না করেই নীরবে দিন পার করছেন তিনি।
শহুরে যান্ত্রিক জীবনে এরকম সমস্যা হয়ত অনেক সংসারেই চলছে। এক্ষেত্রে করণীয় কী?


তিনি আরও বলেন, “একই সঙ্গে সংসারের সব ঝক্কি-ঝামেলাও অনেক সময় এক হাতেই সামলাতে হয়। তাই ত্যাগের ক্ষেত্রে, মানিয়ে চলার ক্ষেত্রটা যেন একজনের ঘাড়ে না চেপে যায়, সেদিকটায় সচেতন হতে হবে।”
ছোটখাটো কিংবা বড় সমস্যায় সমঝোতার মধ্য দিয়ে যদি দুজন সুন্দর মানিয়ে চলেন, তবে সম্পর্কটা অনেক সহজ হবে। মধুর হবে দাম্পত্য জীবন, বলে মত দেন এই অভিনেত্রী।
তিক্ত সম্পর্কের ইতি টেনে অতীতের মধুর সম্পর্কে আবার ফিরে পেতে পরামর্শ দিয়েছেন বিশেষজ্ঞ মনোরোগ চিকিৎসক মোহিত কামাল।
* যতই মনোমালিন্য হোক, রাগ করে জীবনসঙ্গীর থেকে মুখ ফিরিয়ে রাখবেন না। রোমান্স করুন, ভালোবাসা প্রকাশ করুন। তাকে জানতে দিন যে তার কতটা পরোয়া আপনি করেন।
* আজকাল মোবাইল ফোন ছাড়া যেন আমাদেও এক মুহূর্তও চলেনা। তাই বলে নিজের একান্ত সময়ে ফোন বন্ধ রাখাই ভালো। মানে ফেইসবুক হোক বা বন্ধুর সঙ্গে আড্ডা, অফিসের কল হোক বা জরুরি কাজ— দিনের কিছু সময় এই যন্ত্র দূরে রেখে জীবনসঙ্গীকে সময় দিন।
* রাতে বাসায় এসে হাতের কাজ যত দ্রুত সম্ভব শেষ করুন। একই সময়ে রাতের খাবার শেষে চেষ্টা করুন শত ব্যস্ততা সত্ত্বেও একইসঙ্গে বিছানায় যেতে। এতে দাম্পত্য মধুর থাকে সেটা পরীক্ষিত সত্য।


- যৌনতাকে প্রশ্রয় দিন। সুন্দর যৌন সম্পর্ক অনেক সমস্যারই সমাধান করে। যৌন সম্পর্কে নতুন কিছু করুন, সঙ্গীকে খুশি রাখুন। যৌনতার বাইরেও ভালোবাসা আছে। আলিঙ্গন, হাতে হাত রাখা, সকালে একটা মিষ্টি চুমু ইত্যাদি— সম্পর্ক থেকে কখনও যেন হারিয়ে না যায় খেয়াল রাখবেন।
* রোজ রোজ সেই একঘেয়ে সংসারের প্যাচাল আর খিটমিট, সুন্দর সম্পর্কেও ফাটল ধরাতে পারে। নতুন কিছু বলুন, নতুন বিষয় নিয়ে কথোপকথন চালান। সম্পর্ক নতুন হয়ে উঠবে।
* নিজের প্রিয় মানুষকে ধন্যবাদ দিন। তার সেই প্রতিটি কাজের জন্য যা তিনি করেন আপনার খাতিরে। আবার অনেক সময় বিরক্ত হয়েও করেন। সেইসব ক্ষেত্রে আরও বেশি ধন্যবাদ দিন। দেখবেন ধন্যবাদ পেলে তার বিরক্তি অনেক কমে যাবে।
ছবি: রয়টার্স।
সুখের দাম্পত্য জীবন
একসঙ্গে ঘুমাতে গেলে সম্পর্ক সুখের হয়
টাকা দিয়ে সুখ পাওয়ার উপায়

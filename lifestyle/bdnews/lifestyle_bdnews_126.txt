তরুণ প্রজন্মের চাহিদা অনুযায়ী ভিন্নতা এসেছে এর ডিজাইনে। ফরমাল ও ক্যাজুয়াল ব্লেজারে ছেয়ে আছে রাজধানীর তৈরি পোশাকের দোকানগুলো।
খরচ একটু বেশি হলেও চাহিদার কমতি নেই দর্জি দোকানগুলোতে। প্রিয় তারকাদের পরা ব্লেজারের ডিজাইন অনুযায়ী ব্লেজার বানাতে অনেকেই বায়না ধরছেন দোকানগুলোতে।
তবে হাল ফ্যাশনের রেডিমেইড ব্লেজার কিনতে চাইলে যেতে হবে আর্টিস্টি, ক্যাটস আই, ফ্রিল্যান্ড, এক্সটেসি ইত্যাদি দোকানগুলোতে।
ক্যাটস আইয়ের ডিজাইনার সাদেক কুদ্দুস বলেন, “এবারের শীত ফ্যাশনে টু বাটন কটন ব্লেজারের চাইদা ছিল সবচাইতে বেশি। দাম ৬ হাজার থেকে ৬ হাজার পাঁচশ’ টাকা। এছাড়াও ফরমাল ডিজাইনের শুধু ব্লেজারগুলো পাওয়া যাবে ৬ হাজার টাকায়।”
তিনি আরও জানান, একই কাপড়ের ব্লেজার ও প্যান্ট বা কমপ্লিট পাওয়া যাচ্ছে ৭ হাজার থেকে ৯ হাজার টাকায়। আর ১ হাজার পাঁচশ’ টাকায় পাওয়া যাচ্ছে বিভিন্ন ডিজাইনের কোটি।


তার মতে, ব্লেজারের কাপড় যাচাই-বাছাই করার অভিজ্ঞতা না থাকলে একদরের দোকানগুলো থেকেই কাপড় কেনা ভালো।
তিনি বলেন, “৩ হাজার টাকা থেকে শুরু করে ৫০ হাজার টাকা দামের কাপড় আছে আমাদের সংগ্রহে। এগুলোর মধ্যে চীনা ও ভারতীয় কাপড়েই বেশি। এছাড়া ইতালি, ইংল্যান্ড ও অন্যান্য দেশের ব্র্যান্ডের কাপড়ও পাওয়া যাবে এখানে।”
তবে ৮ হাজার থেকে ১৫ হাজার টাকার কাপড় কেনার ক্ষেত্রেই বেশি জোর দিলেন মজিবুর দেওয়ান।
ঢাকার দর্জির দোকানগুলোর মধ্যে ফিট এলিগেন্স, ফপ’স, ফেরদৌস, সানমুন, রেমন্ড, টপটেন এসব দোকানগুলোর হাতের কাজ ভালো, জানালেন মজিবুর।


ফপ’স-এ সিঙ্গেল ব্লেজারের মজুরি ৪ হাজার টাকা, টু পিস কমপ্লিটের মজুরি ৪ হাজার পাঁচশ’ টাকা আর তিন পিস কমপ্লিট বানাতে লাগবে ৬ হাজার টাকা। নিজস্ব ডিজাইনের বানাতে চাইলে বাড়তি ১ হাজার টাকা মজুরি দিতে হবে। এছাড়া ওভারকোট ও লঙ কোটের মজুরি ৫ হাজার এবং কোটি বানানোর মজুরি ১ হাজার সাতশ’ টাকা।
“বর্তমানে সব বয়সি ক্রেতাদের মধ্যে ফিটিংস ব্লেজার বেশি জনপ্রিয়।” বলেন মজিবুর।
কলার বা পকেটের ডিজাইনের মধ্যে আছে পাইপিং, অ্যাঙ্গেল পকেট, রুল কলার, তালিপকেট ইত্যাদি ডিজাইনের ভিন্নতা।
ওভার কোটের ডিজাইনের মধ্যে আছে শার্ট কলার, ওপেন কলার. স্যুট কলার ইত্যাদি। ক্যাটালগ দেখে পছন্দসই ডিজাইনটি বেছে নেওয়া যাবে।  
স্বাভাবিক গড়নের একটি ব্লেজার বানাতে সাধারণত দুইগজ কাপড় লাগে। প্যান্টসহ বানালে সোয়া তিনগজ আর প্যান্ট ও কোটিসহ বানাতে কাপড় লাগবে চারগজ। ওভারকোট ও লঙ কোটে কাপড় লাগবে তিনগজ।
তুলনামূলক কমদামে ব্লেজার বা কমপ্লিট বানাতে চাইলে ঢুঁ দিতে পারেন রাজধানীর নীলক্ষেতের কাপড়ের দোকানগুলোতে। এখানে খোঁজ নিয়ে দেখা গেল চার থেকে পাঁচ হাজার টাকায় প্যান্টসহ ব্লেজার বানিয়ে নেওয়া যাবে।
ছবি: ঋত্বিকা আলী।
হুডিতে মুডি
শীতপোশাকের দরদাম

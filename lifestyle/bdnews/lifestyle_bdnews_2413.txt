রেসিপি দিয়েছেন সামিয়া রহমান


উপকরণ
পরোটার জন্য: আটা ২ কাপ। ঘি বা, বাটার ১ টেবিল–চামচ। লবণ ১ চা-চামচ। পানি পরিমাণমতো।
সসের জন্য: দই, চিলি সস, টমেটো কেচাপ।
কাবাবের জন্য: হাড় ছাড়া মুরগির মাংস ১ কেজি। দই ৬ টেবিল–চামচ। রসুনবাটা ২ টেবিল–চামচ। আদাবাটা ২ টেবিল–চামচ। জিরাগুঁড়া ১ চা-চামচ। ধনেগুঁড়া ১ চা-চামচ। শুকনামরিচ ২-৩টি। তেল পরিমাণমতো। লবণ স্বাদমতো।
পদ্ধতি
মাংস ধুয়ে কিউব করে কেটে নিন। এবার তেল, মাংস বাদে বাকি সব উপকরণ একসঙ্গে মিশিয়ে এই মিশ্রনে মাংস দিয়ে আবার ভালো করে মাখিয়ে ১ ঘণ্টা রেখে দিন। তারপর কাবাবের কাঠিতে মাংসের কিউবগুলো দিয়ে গ্রিলারে ২০ মিনিটের জন্য গ্রিল করে বের করুন।
কাবাব রোল বানানোর পদ্ধতি: আটা, ঘি, লবণ আর পরিমাণমতো পানি দিয়ে ডো বানিয়ে ৩০ মিনিট রেখে দিন। এবার একটু মোটা করে রুটি বেলে, প্যানে তেল বা, ঘি বা, বাটার দিয়ে ভাজুন। ভাজা ভাজা হয়ে নানের মতো হলে নামিয়ে ফেলুন।
দই, চিলি সস ও টমেটো কেচাপ মিশিয়ে বানানো সস নানের উপর ছড়িয়ে দিন। তারপর গ্রিল করা মাংস, সঙ্গে শসাকুচি আর পেঁয়াজ দিয়ে মাঝখানে লম্বালম্বিভাবে রেখে এবার নানরুটি রোল করে নিন। টুথপিক দিয়ে আটকিয়ে দিন। হয়ে গেল কাবাব রোল।


উপকরণ
পরোটার ডো তৈরির জন্য: আটা ২ কাপ। ঘি বা, বাটার ১ টেবিল–চামচ। লবণ ১ চা-চামচ। পানি পরিমাণমতো।
পরোটার ফিলিংয়ের জন্য: আগের দিনের বেঁচে যাওয়া যে কোনো রান্না মাংস ২ কাপ। পেঁয়াজ ১টি। ক্যাপসিকাম ১টি। কাঁচামরিচ ২টি। ডিম ২টি। টমেটো সস।
পদ্ধতি
আটা, ঘি, লবণ আর পরিমাণমতো পানি দিয়ে ডো বানিয়ে নিন। এবার পরোটা বানিয়ে ফেলুন।
ডিম ২টি ফেটিয়ে ভাজুন। গোল করে ভাজবেন। যে কয়টা পরোটা হবে সে কয়টা ডিম গোল করে ভেজে রাখুন। এবার মাংসগুলো হাত দিয়ে ঝুরি করুন। প্যানে তেল দিয়ে ঝুরি মাংস, ক্যাপসিকাম, পেঁয়াজকুচি ও কাঁচামরিচ দিয়ে কিছুক্ষণ নেড়ে, নামানোর আগে সস দিয়ে মাখিয়ে নামিয়ে ফেলুন।

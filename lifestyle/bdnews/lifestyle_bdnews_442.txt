সাজেক উপত্যকা সমুদ্রপৃষ্ঠ থেকে প্রায় সতেরশ ফুট উচ্চতায়- যেন রাঙামাটির ছাদ। সাজেকের কংলাক চূড়া থেকে মেঘমুক্ত সময়ে দূর থেকে তাকালে পাহাড়ের বন্ধন পেরিয়ে চোখে পড়ে কাপ্তাই লেকের সুবিশাল জলধারা। ক্যামেরার লেন্স নয় চোখের রেটিনায় ধরা দেবে মনোমুগ্ধকর দৃশ্য।


সাজেকের পথে এবারের যাত্রাটা নদীপথ ধরে, নীল জল ছুঁয়ে পাড়ি দিব কাপ্তাই লেক। যাত্রার শুরুটা রাঙামাটির রিজার্ভ বাজার থেকে।


এখানে হাতের রেখার মতো ছোট ছোট দ্বীপ ছড়িয়ে আছে জলের চারপাশে। যেন জলদ্বীপের মাঝখানে ছোট ছোট বসতি। পানকৌড়ির ঝাঁকে কয়েকটা সারস পাখিও উড়ছে পানির উপর।


শুভলং বাজারে কিছুক্ষণের বিরতি পেরিয়ে আবার যাত্রা হল গভীর লেক ধরে। বিস্তৃত লেক চারপাশে। অনেকাংশে জলের সীমানাটুকু চোখ এড়িয়ে যাই। বসন্তের সকাল প্রকৃতির স্নিগ্ধতায় মুগ্ধ হবে যে কেউ, লেকের পাশের প্রাকৃতিক বনজুড়ে হলুদ রংয়ের ক্যানভাস।


লেকের তীরে সবুজ জনপথ লংগদু, লংগদু জীপে যাত্রা করলাম মাঝ দুপুরে। দুপুরে অলস প্রকৃতি, সর্পিল পাহাড়ে পথ পেরিয়ে সবুজ বনানীতে চললাম, দূরের পাহাড়গুলো যেন আকাশ হেলান দেয়। কালো পাহাড়ে সীমানা পেরিয়ে দুর্গম বনের গন্ধ মেখে পাহাড়ের পথে সাজেকগামী পর্যটকের দল। দুপুরের রোদ মেখে জীপের ছাদে চললাম উপত্যকার দিকে, পথে স্বল্প চা বিরতি। চায়ের উষ্ণতাও যেন পাহাড়ের গন্ধ।


তখন প্রায় সন্ধ্যা। পূর্ণিমায় আলোকিত পাহাড়ি রাজ্য। গভীর অরণ্যের পাড়ার দুপাশে কাচালং রির্জাভ ফরেস্টের দুর্গম বনাঞ্চল। আকাশজুড়ে সহস্র নক্ষত্র। আকাশের তারা যেন আছড়ে পড়বে পাহাড়ের পৃথিবীতে। উপত্যকা ভাঁজে ভাঁজে জমবে মেঘ, সাদা মেঘে ঢেকে যাচ্ছে পাহাড়। পূর্ণিমায় আলোয় আলোকিত পুরো পাড়া।



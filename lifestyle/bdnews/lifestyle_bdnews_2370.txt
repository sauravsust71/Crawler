সম্প্রতি এক গবেষণায় দেখা গেছে, অতিরিক্ত ফেইসবুক আসক্তির কারণে নিজের জীবন নিয়ে অসন্তুষ্টি এবং নিরাশা জাগতে পারে। আর এর ফলাফল অবসাদ ও হতাশা।
ফেইসবুক বর্তমান বিশ্বে সবচেয়ে জনপ্রিয় সোশাল নেটওয়ার্কিং সাইট। তবে বন্ধু এবং কাছের মানুষের সঙ্গে যোগাযোগের এই মাধ্যমের প্রতি অতিরিক্ত আসক্তির কারণে ব্যবহারকারী ডুবে যেতে পারে হতাশায়।
অস্ট্রিয়ান মনোবিজ্ঞানী ক্রিস্টিনা সাইগিওগ্লৌ এবং টোবাইস গ্রেইটমেইয়ার বলেন, “হতাশা এবং মন খারাপের সঙ্গে ফেইসবুকের সম্পর্ক রয়েছে।”
এ গবেষণার জন্য জার্মানের ১২৩ জন ফেইসবুক ব্যবহারকারীর উপর জরিপ চালানো হয়।
তাদের ক্ষেত্রে দেখা গেছে, ফেইসবুকে বেশি সময় কাটানোর পর তাদের মন কিছুটা হলেও খারাপ হয়ে যায়।
এরপর আমাজন ম্যাকানিকাল টার্ক প্রোগ্রামের ২৬৩ জনের উপর জরিপ চালানো হয়। তাদের ক্ষেত্রেও ফেইসবুকের একই ধরনের প্রভাব দেখা যায়।

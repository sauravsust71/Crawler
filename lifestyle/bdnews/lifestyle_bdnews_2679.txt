তাই প্রয়োজন বিশেষ যত্ন।
স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে বসা চোখের কারণ ও তা সারিয়ে তোলার উপায় উল্লেখ করা হয় যা এখানে দেওয়া হল।
কারণ
চোখ কোটরে চলে যাওয়া বংশগত এবং সাধারণত চোখের নিচে চর্বির অভাবে হয়ে থাকে। এই সমস্যা সমাধানের জন্য প্রয়োজন বিশেষ যত্ন।
কসমেটিক সার্জন ডক্টর ভিনদ ভিজ বলেন, “খাবারে অনিয়ম, না খেয়ে থাকা ইত্যাদির কারণে পুষ্টির অভাব দেখা দেয়, যা থেকে বিভিন্ন শারীরিক সমস্যা হতে পারে। ফলে চোখের নিচে মেদ কমে যায়, ত্বক শুষ্ক হয়ে ওঠে। চোখের আশপাশের অংশ তুলনামূলক কোমল এবং সংবেদনশীল হওয়ার কারণে ত্বক কুঁচকে যাওয়া ও বলিরেখার সমস্যা দেখা দিতে পারে বয়সের আগেই।”
অপর্যাপ্ত পানি পান চোখ ডেবে যাওয়ার ক্ষেত্রে বড় ভূমিকা রাখে। নিয়মিত পর্যাপ্ত পানি পান করা হলে চোখ আবারও স্বাভাবিক অবস্থায় ফিরতে পারে।
তবে অতিরিক্ত ঘামের কারণে শরীরে পানির অভাব দেখা দিতে পারে। সে থেকেও এই সমস্যার সূত্রপাত হওয়ার ঝুঁকি থেকে যায়।
সিনিয়র কসমেটিক সার্জন ডক্টর মোহন থমাস বলেন, “গরমে ক্লান্তি অনুভব করা, ডায়রিয়া, বমি, অতিরিক্ত মদ্যপান ইত্যাদি কারণে চোখের নিচে কালচেভাব বৃদ্ধি পেতে পারে। তাছাড়া এতে করে চোখের নিচের ত্বক পাতলা ও স্বচ্ছ হয়ে যেতে পারে। অপর্যাপ্ত ঘুমের কারণেও চোখ ডেবে যেতে পারে।”
চোখ ডেবে যাওয়ার আরও কিছু কারণ রয়েছে। যেমন:
- ধূমপান।
- হঠাৎ করে ওজন কমে যাওয়া। এতে মুখের কোষ কমে যায় এমনকি চোখের নিচেও।
- অতিরিক্ত অ্যালকোহল আসক্তি।
- বয়স বেড়ে যাওয়া।
- অপর্যাপ্ত ঘুম ও অবসাদ।
- অতিরিক্ত দুশ্চিন্তা ও শারীরিক পরিশ্রম।
- চোখের নিচে কালি পরলে তার যত্ন না নেওয়া।
- প্রয়োজনীয় পুষ্টির অভাব।
এ সমস্যা থেকে মুক্তি পেতে প্রয়োজন কিছু সাধারণ নিয়ম মেনে চলা। এমনই কিছু বিষয় এখানে উল্লেখ করা হল।
- একজন প্রাপ্তবয়স্ক মানুষের দিনে টানা ৭ থেকে ৮ ঘন্টা ঘুমানো উচিত। এতে সারাদিনের মানসিক চাপ ও ক্লান্তি দূর হয়।
- চোখের উপর শসা বা আলুর টুকরা করে কেটে দিয়ে রাখলে তা চোখ ও এর আশপাশের ত্বক ঠাণ্ডা করে এবং রক্ত সঞ্চালন বৃদ্ধিতে সাহায্য করে। এতে চোখের ক্লান্তি ভাব দূর হয়। পাশাপাশি চোখের সংবেদনশীল ত্বকের কালচেভাব দূর করতে সাহায্য করে।
- ব্যবহৃত টি ব্যাগ ঠাণ্ডা করে চোখের উপর কয়েক মিনিট হালকাভাবে চেপে ধরে রাখলে রক্ত সঞ্চালন বৃদ্ধি পায়।
- চোখের নিচের কালো দাগের জন্য তরমুজ, শসা, শাকসবজি ইত্যাদি বেশি করে খাওয়া উচিত। এতে শরীরের অতিরিক্ত পানি বের হয়ে যেতে সাহায্য করে এবং চোখের নিচে কালচেভাব এবং ফোলাভাব কমে আসবে।

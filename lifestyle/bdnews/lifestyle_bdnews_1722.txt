প্রাক্তন প্রিয়জনের সঙ্গে যোগাযোগ ধরে রাখার চেষ্টা কিংবা ঘনিষ্ঠতা গড়ে ওঠা আপনার বর্তমান প্রেমের সম্পর্ক নষ্ট করবে, এমনকি ভবিষ্যতে নতুন সম্পর্কে জড়ানোর ক্ষেত্রেও তা বাধা হয়ে দাড়াবে। আপনি যত যুক্তিই দেখান না কেনো প্রাক্তন প্রেমিক প্রেমিকার মধ্যে শুধু বন্ধুত্ব থাকা কখনও সম্ভব নয়।
সম্পর্কবিষয়ক একটি ওয়েবসাইটে এই বিষয়ের উপর প্রকাশিত প্রতিবেদন অবলম্বনে জটিলতার মাত্রাগুলো এখানে দেওয়া হল। 
* প্রাক্তন প্রেমিক কিংবা প্রেমিকার সঙ্গে দেখা করা, যোগাযোগ রাখার পরিষ্কার অর্থ হল দুজনেরই পরস্পরের প্রতি দুর্বলতা আজও বিদ্যমান। তবে যার সঙ্গে বর্তমানে আপনার প্রেমের সম্পর্ক আছে, তার দৃষ্টিভঙ্গিতে বিষয়টা একেবারেই ভিন্ন। তার কাছে এটাই মনে হবে যে আপনি আজও আপনার প্রাক্তনকেই বেশি ভালোবাসেন।
* প্রাক্তনের সঙ্গে যোগাযোগ হলে আপনার বর্তমান প্রেমিক কিংবা প্রেমিকাকে নিজে থেকে জানিয়ে দেওয়াই বুদ্ধিমানের কাজ। নইলে ভবিষ্যতে হাটে হাঁড়ি ভেঙে যাওয়ার অবস্থা হলে বর্তমান সম্পর্ক বাঁচানোর উপায় থাকবে না। আর কখনও কোনো কারণে বর্তমানের সঙ্গে পুরনোর তুলনা করে বসলে মারাত্বক বিপদে পড়বেন।   
* আপনার বর্তমান সঙ্গী হয়ত আপনাকে সত্যিকার অর্থেই গভীরভাবে ভালোবাসে। তারপরও প্রাক্তনের জন্য এখনও আপনার মন কাঁদে। এটা যদি প্রাক্তনের প্রতি ঈর্ষা না হয়, তবে ঈর্ষা শব্দটার অস্তিত্বই নেই!
* প্রাক্তন প্রিয়জনের সঙ্গে আপনি হয়ত নিছক স্মৃতিচারণ করছেন, তবে প্রাক্তন হয়ত ভাবছে পুনর্মিলনের কথা, এমনকি হয়ত দেখছে ভেঙে যাওয়া সম্পর্ক জোড়া লাগার স্বপ্ন। একই দুশ্চিন্তায় হয়ত আপনার বর্তমান প্রেমিক কিংবা প্রেমিকাও ভুগছে। ফলাফল, আপনি দুজন মানুষকে দ্বিধা-দ্বন্দ্বের মধ্যে ফেলছেন। 
* এই পরিস্থিতির সবচাইতে মারাত্বক বিষয় হল পুরানো সম্পর্কের ঘোর থেকে বেরিয়ে আসার অনিহা। আপনার অতীত ক্রমেই ঘাড়ে বোঝা হয়ে জেঁকে বসছে, ফলে বর্তমান সম্পর্ক ক্ষতিগ্রস্ত হচ্ছে প্রতিনিয়ত। এমনকি বর্তমানে কোনো সম্পর্কে আবদ্ধ না থাকলেও সম্ভাব্য ভবিষ্যত সম্পর্কের দিকে পা বাড়ানো যায় না। তাই স্থির সিদ্ধান্ত নেওয়া জরুরি।
ছবি: রয়টার্স।
আরও পড়ুন
ভুলতে পেরেছেন কি অতীত প্রেম !  
বিচ্ছেদের অবসাদ দূর করতে  

গবেষকরা জানিয়েছেন, প্রতিদিন ভিটামিন সি সাপ্লিমেন্ট গ্রহণের মাধ্যমে স্থূল এবং বাড়তি ওজনধারী প্রাপ্তবয়স্করা নিয়মিত ব্যায়ামের হৃৎপিণ্ডজনিত স্বাস্থ্যগুণগুলো পেতে পারেন।
স্থূল ও বাড়তি ওজনধারী প্রাপ্তবয়স্কদের রক্তনালীতে সংকুচিত রক্তনালী করার প্রোটিন ‘এন্ডোথেলিন (ইটি)-ওয়ান’য়ের কার্যকারীতা বেশি থাকে।
ইটি-ওয়ান’য়ের বাড়তি কার্যকারীতার কারণে এই রক্তনালীগুলোর বন্ধ হয়ে যাওয়ার আশঙ্কা বেড়ে যায়। কমে যায় রক্ত প্রবাহের চাহিদার প্রতি সাড়া দেওয়া এবং বেড়ে যায় রক্তনালীজনিত জটিলতায় আক্রান্ত হওয়ার আশঙ্কাও। 
শরীরচর্চার মাধ্যমে ইটি-ওয়ানের কার্যকারীতা কমতে দেখা গেছে। তবে দৈনিক রুটিনে শরীরচর্চাকে জায়গা দেওয়া কঠিন হযে যায়। 
মার্কিন যুক্তরাষ্ট্রের বউল্ডারের ইউনিভার্সিটি অফ কলোরাডো করা এই গবেষণায় দেখা হয়, ভিটামিন সি— যা রক্তনালীর কার্যকারীতা বাড়ায় বলে বলা হয়, তা ইটি-ওয়ান’য়ের কার্যকারীতা কমাতে পারে কি না। 
গবেষকরা দেখেন, দৈনিক ৫০০ মিলিগ্রাম ভিটামিন সি সাপ্লিমেন্ট গ্রহণ করার মাধ্যমে ইটি-ওয়ান’য়ের কারণে রক্তনালী বন্ধ হয়ে যাওয়া কমানো সম্ভব, যা সকালে হাঁটার গুণের সমপরিমাণ।
গবেষকরা লেখেন, ‘বাড়তি ওজনধারীদের জন্য ভিটামিন সি সাপ্লিমেন্ট গ্রহণ করা ইটি-ওয়ানের কারণে রক্তনালী বন্ধ হয়ে যাওয়া কমানোর একটি কার্যকরী জীবনযাত্রা।’
যুক্তরাষ্ট্রের জর্জিয়াতে অনুষ্ঠিত ১৪তম ইন্টারন্যাশনাল কনফারেন্স অন এন্ডোথেলিন: ফিজিওলজি, প্যাথোফিজিওলজি অ্যান্ড থেরাপিউটিক্সিইন’য়ে এই গবেষণা উপস্থাপন করা হয়।
ছবি: রয়টার্স।

কিশোর বয়সে ঘুম অনেক ইতিবাচক প্রভাব ফেলে। বিশেষ করে যারা যখন তখন রেগে যায়; কান্না, উদ্বেগ বা ক্লান্তির মতো ইত্যাদি সমস্যায় ভোগে তাদের জন্য ঘুম বেশ গুরুত্বপূর্ণ। 
স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে এই বিষয়ের উপর করা একটি গবেষণার সূত্র ধরে করা প্রতিবেদনে জানানো হয়, যদি মনে করেন আপনার কিশোর সন্তান অলস সময় পার করে বিছানায় শুয়ে থাকে, তাহলে জেনে নিন বিজ্ঞান তাদের পাশে আছে।
‘ইউনিভার্সিটি অফ ক্যালিফোর্নিয়া, লস অ্যাঞ্জেলেস’ বা ইউসিএলএ পরিচালিত এক গবেষণায় দেখা গেছে, কিশোরদের মানসিক বিকাশের জন্য অন্যদের তুলনায় বেশি ঘুমের প্রয়োজন।
গবেষক অ্যান্ড্রু ফুলিঙ্গি'র করা আগের এক গবেষণায় দেখা গেছে, মানুষের কতটুকু ঘুম প্রয়োজন তা নির্ভর করে তার মানসিক স্বাস্থ্য ও অ্যাকাডেমিক কার্যক্রমের উপর। আগের গবেষণায় আরও দেখা গেছে, কিশোরদের কম ঘুমের জন্য সামাজিক যোগাযোগ মাধ্যমগুলো ভূমিকা রাখে। 
তার নতুন গবেষণার জন্য, ফুলিঙ্গি লস অ্যাঞ্জেলস’য়ের নবম ও দশম গ্রেডের ৪১৯ শিক্ষার্থীকে ১৪ দিনের জন্য পর্যবেক্ষণ করেন।
কিশোরদের মনের উপর ঘুম পরেরদিন কীভাবে প্রভাব ফেলে তা দেখার জন্য তাদেরকে ঘুমাতে যাওয়ার আগে পূর্ববর্তী রাতের ও সারাদিনের মানসিক অবস্থা সম্পর্কে তিন পৃষ্ঠাব্যাপী ‘চেকলিস্ট’ পূরণ করতে বলেন।  
তাদের ঠিকঠাক মনোভাব প্রকাশের সুবিধার জন্য কিশোরদের প্রতিদিন তাদের কার্যাকলাপের চেকলিস্ট পূরণ করে ইলেক্ট্রনিক টাইম স্ট্যাম্প’য়ের মাধ্যমে সিল করতে হয়। এবং ঠিকভাবে এই কাজ করার জন্য তাদেরকে সিনেমার টিকেট সরবারহ করা হয়।
এখানে দেখা গেছে, প্রতিদিনকার মন ভালো রাখার ক্ষেত্রে কৈশোরে ঘুমের পার্থক্য প্রভাব রাখে।
গবেষণায় দেখা গেছে, ঘুম কিশোরদের উপর ইতিবাচক প্রভাব রাখে এবং যারা ভয় বা হতাশার নানারূপ লক্ষণ যেমন- কান্না, উদ্বেগ বা ক্লান্তি ইত্যাদির সমস্যার মুখোমুখি হয় তাদের জন্য এটা বেশ উপকারী।
যুবক বা যাদের এই ধরনের লক্ষণ কম আছে তাদের সঙ্গে তুলনা করে দেখা গেছে যে, এই দুই দলের মধ্যে যারা আগের রাতে ভালোভাবে ঘুমিয়েছে তাদের পরের দিনের মন মেজাজ বেশি ভালো ছিল। এই কিশোরদের মানসিক কার্যকরিতা বাড়ানোর জন্য আরও বেশি ঘুমের প্রয়োজন হতে পারে বলে ধারণা করা হয়।
এছাড়াও, কম ঘুম বা খুব বেশি ঘুম দুটাই ক্লান্তি বাড়ায়। কিছু কিশোরকে রাতে সাত ঘণ্টার কম ঘুমিয়েও ঠিকঠাক কাজ করতে দেখা গেছে।
গবেষণায় আরও দেখা গেছে, ১১ ঘন্টার বেশি ঘুম মোটেও উপকারী নয়।   
ফুলিঙ্গি বলেন, “সমাজ হিসেবে আমরা শিশুদের ঘুমকে অবমূল্যায়ণ করি। বিশেষ করে কিশোরদের প্রয়োজন অনুযায়ী। তাদের ঘুম কম হওয়ার অনেক কারণ আছে এবং অনেক কিশোর আছে যারা স্কুল ও অন্যান্য কাজের কারণে ঘুমকে বিসর্জন দিতে হয়।”
তিনি এখন বিশ্বাস করেন যে, ব্যক্তি স্বাস্থ্য ও কার্যক্রমের উপর ঘুমের প্রয়োজনীয়তার কতখানি নির্ভর করে সে বিষয়ে আরও গবেষণা করতে হতে পারে।

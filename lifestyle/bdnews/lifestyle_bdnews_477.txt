স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটে স্তন ক্যান্সার প্রতিরোধ করতে পারে এমন কিছু খাবারের নাম উল্লেখ করা হয়। এই প্রতিবেদন সেসব খাবারের বিষয়ে তুলে ধরা হল।
বিনস
বিভিন্ন ধরনের দানাদার খাবার নিয়মিত খেলে স্তন ক্যান্সার হওয়ার সম্ভাবনা কমে যায়। বিভিন্ন গবেষণায় দেখা গেছে যারা সপ্তাহে অন্তত দুবার দানাজাতীয় খাবার খেয়ে থাকেন তাদের ক্ষেত্রে অন্যদের তুলনায় ব্রেস্ট ক্যান্সার হওয়ার সম্ভাবনা কম থাকে।
সব ধরনের বিনস বা দানাদার খাবারে রয়েছে প্রচুর পরিমানে আঁশ। আর কালো বিনস এস্ট্রোডিওলের প্রবাহ নিয়ন্ত্রণ করতে সাহায্য করে। এস্ট্রোডিওল, ইস্ট্রোজেন হরমোনের একটি ধরন যা ইস্ট্রোজেনের প্রভাবে স্তন ক্যান্সার হওয়ার জন্য দায়ী থাকে। বিনস ইস্ট্রোজেনের এই ক্ষতিকর ধরণটির প্রভাব কমাতে সাহায্য করে।
ব্রোকলি


রসুন
গবেষণাগারের করা পরীক্ষায় দেখা গেছে, রসুনের অ্যালায়াল সালফার এবং অন্যান্য উপাদান টিউমারের কোষ বৃদ্ধি প্রতিরোধ করতে কার্যকর ভূমিকা পালন করে।
মাশরুম
বিভিন্ন ধরনের মাশরুমে রয়েছে ক্যন্সার প্রতিষেধক উপাদান। অ্যারিজোনার একজন পুষ্টিবিদ কারেন গ্রাহাম (আর.ডি) বলেন, “কুমড়া, গাজর এবং টমেটোর তুলনায় মাশরুমে অ্যান্টিঅক্সিডেন্টের পরিমাণ বেশি থাকে। কারণ মাশরুমে রয়েছে রোগপ্রতিরোধ ক্ষমতা বৃদ্ধির দুটি বিশেষ অ্যান্টিঅক্সিডেন্ট উপাদান এরগোথিয়োনেইন এবং সেলেনিয়াম। এই হরমোনগুলো ক্যান্সার এবং কার্ডিওভাস্কুলার রোগের বিরুদ্ধে রোগপ্রতিরোধক ক্ষমতা বৃদ্ধিতে সহায়তা করে।”
ইন্টারন্যাশনাল জার্নাল অফ ক্যান্সার’য়ে প্রকাশিত এক গবেষণার ফলাফলে জানা যায়, যারা দিনে ১০ গ্রাম মাশরুম খেয়ে থাকেন তাদের স্তন ক্যান্সার হওয়ার সম্ভাবনা ৬৪ শতাংশ কমে যায়।


পাঁচ বছরের গবেষণার ফলাফলের ভিত্তিতে ইউনিভার্সিটি অফ সাউদার্ন ক্যালিফোর্নিয়া এবং ন্যাশনাল ইউনিভার্সিটি অফ সিঙ্গাপুরের গবেষকরা জানান, মেনোপজের পরে যারা প্রতিদিন দেড় থেকে তিন আউন্স স্যামন বা সামুদ্রিক মাছ খেয়ে থাকেন তাদের ক্ষেত্রে অন্যদের তুলনায় স্তন ক্যান্সার হওয়ার সম্ভাবনা ২৬ শতাংশ কমে আসে।
পালংশাক
এতে আছে বেটাক্যারটিন এবং লুটেইন নামের দুটি শক্তিশালী অ্যান্টিঅক্সিডেন্ট উপাদান। ন্যাশনাল ইন্সটিটিউট অফ এনভারোনমেন্টাল হেল্থ সায়েন্সের গবেষকরা জানান, যারা সপ্তাহে অন্তত দুবারের বেশি পালংশাক খেয়ে থাকেন তাদের স্তন ক্যান্সার হওয়ার সম্ভাবনা কম থাকে।
স্তন ক্যান্সারের ঝুঁকি কমাতে কফি

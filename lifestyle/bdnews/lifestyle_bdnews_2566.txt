রেসিপি দিয়েছেন আনিসা হোসেন।
উপকরণ
ইলিশ মাছ ৫ টুকরা (বড় আকারের)। কুমড়া-পাতা বা লাউপাতা ১০টি (বড়, ছোট মিলিয়ে)। আস্ত সরিষা ৩ টেবিল-চামচ। আস্ত রসুনকোয়া ৫,৬ টি (বড় কোয়া)। আস্ত কাঁচামরিচ ৪,৫টি। পেঁয়াজকুচি দেড় কাপ। হলুদগুঁড়া আধা টেবিল-চামচ। সরিষার তেল ১/৪ কাপ। লবণ স্বাদ মতো।
পাতুরি রান্নার উপকরণ
নারিকেল দুধ এক কাপ। ভাজা জিরাগুঁড়া আধা টেবিল-চামচ। পাপরিকা আধা চা-চামচ। লাল কাঁচামরিচের ফালি ৪,৫টি।
পদ্ধতি
সরিষা ধুয়ে সঙ্গে রসুন, কাঁচামরিচ এবং একটু লবণ দিয়ে মিহি করে বেটে নিন৷
মাছের টুকরা ও কুমড়া-পাতা ধুয়ে পানি ঝরিয়ে রাখুন৷ একটি বাটিতে পেঁয়াজ কুচির সঙ্গে হলুদগুঁড়া, সরিষা-বাটা এবং সরিষার তেল দিয়ে হাত দিয়ে ভালো করে চটকে নিন। মাছের টুকরাগুলো এর সঙ্গে মেশান।
প্রত্যেকটি মাছের টুকরার সঙ্গে মসলা ভালো করে মাখবেন৷ সরিষা বাটার সময় লবণ দেওয়া হয়েছিল তাই মসলার মাখানোর সময় লবণ একটু সাবধানে দেবেন ৷
একটি বড় পাতা নিন। একটি মসলা মাখানো মাছের টুকরা পাতার মাঝে রেখে কিছু মাখানো মসলা দিন। এবার মাছের উপর আরেকটি পাতা দিয়ে পেঁচিয়ে নিন৷ এভাবে সবগুলো টুকরা আলাদা আলাদা করে পাতা দিয়ে প্যাঁচান।
কড়াইতে দুই টেবিল-চামচ তেল গরম করুন। এবার পাতায় মোড়ানো মাছগুলো কড়াইয়ের গরম তেলে দিয়ে এক মিনিট উলটেপালটে খুব সাবধানে ভাজুন।
ভাজা হলে নারিকেল দুধ, ভাজা জিরাগুঁড়া, ফালি করা মরিচ ও একটু লবণ দিন। ঢেকে ২০ মিনিট রান্না করুন। মাঝে একবার মাছের পাতুরি উল্টে দিন৷
নারিকেল দুধ শুকিয়ে ঘন হয়ে আসলে চুলার আগুন বন্ধ করুন। নামিয়ে, ইলিশ মাছের পাতুরি গরম গরম পরিবেশন করুন গরম ভাতের সঙ্গে।
সমন্বয়ে: ইশরাতে মৌরি।
আরও রেসিপি:
তেঁতুল ইলিশ
বেগুন-লাউপাতা দিয়ে ইলিশ

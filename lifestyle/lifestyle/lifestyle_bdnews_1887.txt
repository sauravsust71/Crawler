তাই উৎসবের মৌসুমে মেইকআপ করার ক্ষেত্রে ফাউন্ডেশন ব্যবহারের কিছু খুঁটিনাটি জেনে রাখা দরকার।
ভারতীয় মেইকআপ বিশারদ কনিকা গৌরভ টানডান নিখুঁতভাবে ফাউন্ডেশন দেওয়ার ক্ষেত্রে কিছু পরামর্শ দেন।
কনিকা বলেন, “ফাউন্ডেশন লাগানোর আগে ত্বক ভালোভাবে পরিষ্কার করে নেওয়া উচিত। পাশাপাশি হাতও ভালোভাবে ধুয়ে নিতে হবে। এ জন্য ত্বকের উপযোগী ফেইসওয়াশ বেছে নেওয়া জরুরি।”
যাদের ত্বক তৈলাক্ত তাদের জন্য ‘টি ট্রি অয়েল’, নিম নির্যাস এবং সিলিসাইলিক অ্যাসিড যুক্ত ফেইসওয়াশ বেছে নেওয়ার পরামর্শ দেন তিনি। এই উপাদানগুলো ত্বক পরিষ্কার করে। তবে ত্বক শুষ্ক করে না।
- আঙুল দিয়ে ফাউন্ডেশন লাগানো এড়িয়ে চলার পরামর্শ দেন কনিকা। তিনি ফাউন্ডেশন লাগানোর জন্য ফাউন্ডেশন ব্রাশ বা স্পঞ্জ ব্যবহারের পরামর্শ দেন। কারণ আঙুল দিয়ে লাগাতে গেলে ফাউন্ডেশন আঙুলে লেগে যেতে পারে। তাছাড়া জীবাণু সংক্রমণের সম্ভাবনাও কম থাকে।
তবে এক্ষেত্রে পরিষ্কার ব্রাশ ব্যবহার করা জরুরি।
- মেইকআপ দীর্ঘস্থায়ী করতে ফাউন্ডেশন লাগানোর আগে প্রাইমার লাগিয়ে নিতে হবে। প্রাইমার ব্যবহার করলে কেমিকেলের কারণে ত্বকের যে কোনো ক্ষতি হওয়ার সম্ভাবনা কম থাকে। তাছাড়া ত্বক স্বাস্থ্যোজ্জ্বল দেখায়।


- পুরো মুখে লেপ্টে ফাউন্ডেশন না লাগিয়ে গালে, কপালে, থুতনিতে এবং নাকে ফাউডেশন অল্প অল্প ফোটার মতো করে লাগিয়ে দিতে হবে।
- এরপর ফাউন্ডেশন ব্রাশ বা স্পঞ্জ দিয়ে পুরো মুখে ফাউন্ডেশন ছড়িয়ে দিতে হবে। অতিরিক্ত ফাউন্ডেশন ব্যবহার করলে দেখতে অস্বাভাবিক মনে হতে পারে।
- সব শেষে ট্রান্সলুসেন্ট লুজ পাউডার দিয়ে ফাউন্ডেশন সেট করে নিতে হবে। এতে ফাউন্ডেশন দীর্ঘস্থায়ী হবে এবং দেখতেও স্বাভাবিক লাগবে। তাছাড়া এতে ত্বকের তৈলাক্তভাবও দূর হবে।
ছবির মডেল: জাকিয়া উর্মি। ছবি: ঋত্বিকা আলী।
মেইকআপ ‘ট্রিকস’
দশ মিনিটে মেইকআপ
মেইকআপ ছাড়াই সুন্দর চোখ
মেইকআপ ব্রাশের পরিচর্যা

গ্রীষ্মের অন্যতম সুস্বাদু ফল লিচু। খুব অল্প সময় থাকে বলে চাহিদা অনেক বেশি।
বাংলাদেশ গার্হস্থ্য অর্থনীতি কলেজের ‘খাদ্য ও পুষ্টি বিজ্ঞান’য়ের বিভাগীয় প্রধান ফারাহ মাসুদা জানান লিচুর পুষ্টি ও গুণের কথা।
মৌসুমি ফল লিচু ভিটামিন ও খাদ্যশক্তির অন্যতম উৎস। এতে রয়েছে মানব শরীরের জন্য প্রয়োজনীয় খনিজ উপাদান। শ্বেতসার এবং ভিটামিন সি’র ভালো উৎস। ছোট বড় সব বয়সের মানুষই এই সুস্বাদু ফল খেতে পারে।
তিনি জানান, লিচুতে রয়েছে সামান্য পরিমাণে প্রোটিন ও ফ্যাট যা মানব দেহের জন্য প্রয়োজন। প্রতি ১০০ গ্রাম লিচুতে রয়েছে ১.১ গ্রাম প্রোটিন এবং ০.২ গ্রাম ফ্যাট।
লিচুতে পর্যাপ্ত পরিমাণে শ্বেতসার পাওয়া যায়। প্রতি ১০০ গ্রাম লিচুতে ১৩.৬ গ্রাম শ্বেতসার থাকে।


লিচুতে রয়েছে ভিটামিন ‘সি’ যা ত্বক, দাঁত ও হাড়ের জন্য ভালো। প্রতি ১০০ গ্রাম লিচুতে ৩১ মি.গ্রা ভিটামিন সি পাওয়া যায়। নানারকম চর্মরোগ ও স্কার্ভি দূর করতে সাহায্য করে ভিটামিন সি। তাছাড়া এটি ত্বক উজ্জ্বল করতে ও বলিরেখা কমাতেও সাহায্য করে।
প্রতি ১০০ গ্রাম লিচুতে ১০ মি.গ্রা ক্যালসিয়াম রয়েছে। ক্যালসিয়াম দেহের হাড় গঠন করে ও হাড়ের সুস্থতা বজায় রাখতে সাহায্য করে। লিচুতে অল্প পরিমাণে লৌহ পাওয়া যায়। প্রতি ১০০ গ্রামে ০.৭ মি.গ্রা লৌহ।
এছাড়াও লিচুতে রয়েছে থিয়ামিন, নিয়াসিন ইত্যাদি, যা লিচুর পুষ্টিগুণ আরও বৃদ্ধি করে। এসব ভিটামিন শরীরের বিপাক ক্ষমতা বাড়ায়।
শক্তির ভালো উৎস লিচু। প্রতি ১০০ গ্রাম লিচু থেকে ৬১ কিলোক্যালরি শক্তি পাওয়া যায়।
এটি শরীরের কোলেস্টেরলের মাত্রা কমায়। পাশাপাশি চর্বি কমাতে সাহায্য করে।
লিচুতে খাদ্য হজমকারী আঁশ, ভিটামিন এবং অ্যান্টি-অক্সিডেন্ট আছে যা শরীরে জমে থাকে ও দেহ সুস্থ রাখে বলে জানান ফারাহ মাসুদা।
লিচুর অলিগোনল নামের উপাদান অ্যান্টি-অক্সিডেন্ট এবং অ্যান্টি-ইনফ্লুয়েঞ্জা হিসেবে কাজ করে। এ উপাদান রক্ত চলাচল স্বাভাবিক রাখে, ত্বকে ক্ষতিকর অতি বেগুনি রশ্মির প্রভাব নিয়ন্ত্রণ করে এবং ওজন কমায়।


লিচুতে রয়েছে ম্যাগনেসিয়াম, পটাসিয়াম এবং অন্যান্য খনিজ উপাদান যা হৃদরোগের ঝুঁকি কমায়। রক্ত চলাচল স্বাভাবিক রাখে ও রক্তচাপ নিয়ন্ত্রণ করে।
এটি শরীরে ফ্লুইডের পরিমাণ বাড়ায়। লিচুতে ক্যালরির পরিমাণ বেশি থাকে তাই ডায়াবেটিস রোগীদের পরিমিতভাবে লিচু খাওয়ার পরামর্শ দেন ফারাহ মাসুদা।
গ্রীষ্মকালীন সবজি
গ্রীষ্মকালীন শাক
ফলপ্রসূ ফল
ত্বকের জন্য উপকারী ফল
যষ্টিমধুর গুণ
কিশমিশের উপকারিতা

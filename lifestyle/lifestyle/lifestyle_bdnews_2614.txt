রেসিপি দিয়েছেন উম্মাহ মোস্তফা।
ওপেন ফ্রাইড স্যান্ডউইচ
উপকরণ: পাউরুটি ৬ টুকরা। সিদ্ধ মুরগির মাংস ১/৪ কাপ। আলু সিদ্ধ ১টি। ধনেকুচি অল্প। পুদিনাকুচি অল্প। কাঁচামরিচ-কুচি স্বাদ মতো। লবণ স্বাদ মতো। চিজ ১/৪ কাপ। ডিম ১টি। তেল ভাজার জন্য।
পদ্ধতি: পাউরুটি আর তেল বাদে বাকি সব উপকরণ ভালো করে মাখিয়ে পুর তৈরি করুন। এখন পুর পাউরুটির গায়ে লাগিয়ে তেলে ভেজে নিন। আমের সস দিয়ে পরিবেশন করুন।
আমের সস
উপকরণ: পাকাআম ১টি। ধনেপাতা আধা কাপ। পুদিনা পাতা ১ টেবিল-চামচ। কাঁচামরিচ ১টি। চিনি ২ টেবিল-চামচ। লবণ স্বাদ মতো।
পদ্ধতি: আম ছিলে রস বের করে নিন। এবার আমসহ বাকি সব উপকরণ ব্লেন্ডারে ব্লেন্ড করে নিন। হয়ে গেল আমের মজাদার সস।
সমন্বয়ে: ইশরাত মৌরি।

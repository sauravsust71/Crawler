তবে কিছু খাবার আছে যেগুলো অনেক সময় ধরে ক্ষুধা নিবারণ করার পাশাপাশি সুস্থ থাকতেও সাহায্য করে। হাফিংটনপোস্ট ডটকমের এক প্রতিবেদনে এমনই কিছু খাবারের নাম উল্লেখ করা হয়েছে।
আপেল: ওয়েলনেস ওয়ার্কডেইস-এর প্রেসিডেন্ট ডেবরা ওয়াইন এক প্রতিবেদনে জানান, আপেলের ফাইবার ও পানি, হজম হতে অনেকটা সময় নেয়। তাই আপেল পেট ভরা রাখে অনেকক্ষণ। তাই ইফতারে আপেল পেট ভরানোর ক্ষেত্রে দারুণ কাজ করবে। তাছাড়া আপেলের পুষ্টিগুণ সুস্থ রাখতেও সাহায্য করে।
সুপ: নিউট্রিশন কিচেন ডটকমের প্রধান বেথ সাল্টজ জানান, যেকোনো ধরনের সুপ ক্ষুদা নিবারণে দারুণ কাজ করে। কারণ খাওয়া যায় বেশি। অপরদিকে সুপে তুলনামূলক কম ক্যালরি থাকে।
ডার্ক চকলেট: ব্লাড প্রেশার নিয়ন্ত্রণে সহায়তা করে এই খাবার। তাছাড়া গবেষণায় জানা গেছে, দুধ মেশানো চকলেটের তুলনায় ডার্ক চকলেট বেশি স্বাস্থ্যকর।

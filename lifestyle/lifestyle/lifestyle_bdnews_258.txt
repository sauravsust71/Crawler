এই স্মার্টফোনের যুগে অনেকের পকেটেই থাকে দামি মোবাইল ফোন। কাজের প্রয়োজনে ল্যাপটপ, ক্যামেরাসহ নানান দামি বৈদ্যুতিক যন্ত্র সঙ্গে রাখেন। সেগুলোকেও চাই বর্ষা থেকে রক্ষা করা ব্যবস্থা।
এই ধরনের বিড়ম্বনা থেকে বাঁচতে সঙ্গে রাখুন ছাতা, রেইনকোট বা বর্ষাতি, বৈদ্যুতিক যন্ত্রপাতির জন্য প্লাস্টিকের ব্যাগ ইত্যাদি।  
বৃষ্টির দিন এলেই ছাতা ও রেইনকোটের বিক্রি বেড়ে যায়, জানালেন রাজধানীর নিউ মার্কেটের বীর বিক্রমপুর ছাতা স্টোরের বিক্রয় প্রতিনিধি মোহাম্মদ নাসির উদ্দিন।
রাজধানীর প্রায় সব মার্কেটেই কমবেশি ছাতা ও রেইনকোট পাওয়া যায়। তবে নিউ মার্কেট, নবাবপুর রোড, পলওয়েল মার্কেট, পুরান ঢাকার ইমামগঞ্জ, স্টেডিয়াম মার্কেটে ছাতার দোকান সবচেয়ে বেশি।
ক্রেতাদের চাহিদা বেশি হওয়ার কারণে এখন ফুটপাথেও কমবেশি ছাতা বিক্রি হতে দেখা যায়।
ছাতা ও রেইনকোটের বিভিন্ন ধরন, ডিজাইন ও দরদাম সম্পর্কে জানালেন নাসির উদ্দিন।
ছাতা


মানভেদে ছাতাগুলোর দাম পড়বে ৩শ’ থেকে ৮শ’ টাকা।
তিনি আরও বলেন, “দুই ভাঁজের ছাতাগুলো বেশি কেনে ছেলেরা। দাম ৪শ’ থেকে সাড়ে ৬শ’ টাকা।”
এই বিক্রেতা জানান, দুই ভাঁজের ছাতার মধ্যে আরও আছে কলম ছাতা, চীন ও জাপানের ঐতিহ্যবাহী ডিজাইনের ছাতা। কলম ছাতার দাম সাড়ে ৩শ’ টাকা। চীনা ও জাপানি ডিজাইনের ছাতাগুলো পাওয়া যাবে ৫শ’ টাকায়।
“আর আমাদের দেশীয় বড় ছাতার দাম পড়বে ৭শ’ টাকা।” বললেন নাসির।
ছাতার ব্র্যান্ড সম্পর্কে নাসির উদ্দিন বলেন, “বাংলাদেশে ছাতার ব্র্যান্ডের মধ্যে আছে শংকর, অ্যাটলাস, ফারুক, সারোয়ার, ফিলিপস, তোফায়েল ইত্যাদি। তবে এই প্রতিষ্ঠানগুলো নিজেরা কোনো ছাতা তৈরি করেনা। বেশিরভাগই চীন থেকে আমদানি করে থাকে।”
ছাতা কেনার সময় ছাতার শিকগুলো মজবুত কিনা এবং ছাতার কাপড়ের গুণগত মান দেখে কেনার পরামর্শ দিলেন এই বিক্রয়কর্মী।
আর ছাতার যত্ন সম্পর্কে বলেন, “ব্যবহারের পর ভেজা ছাতা খুলে রাখতে হবে যেন শুকিয়ে যায়। শুকানোর পর অবশ্যই ছাতার কাপড়ের ভাঁজ অনুযায়ী ভাঁজ করতে হবে। আর বেঁধে রাখার সময় বেশি চেপে বাঁধা উচিৎ নয়। এতে কাপড় ছিঁড়ে যেতে পারে।”
বর্ষাতি বা রেইনকোট


হাঁটু পর্যন্ত লম্বা রেইনকোটগুলো আকারভেদে শিশু ও মেয়েরাই বেশি কেনেন। আর ‘টু পিস রেইনকোট’গুলো কেনেন ছেলেরা। সবগুলোর সঙ্গেই টুপি থাকে।”
দাম সম্পর্কে তিনি বলেন, “রেইনকোটের দাম নির্ভর করে কাপড়ের মান এবং কাপড়ের নিচে কয়টি পরত আছে তার উপর।”
রেইনকোটের ব্র্যান্ডের মধ্যে আছে ডার্বিসুপার, কমফোর্ট, ব্রিদেবল, রেড চ্যাম্পিয়ন ইত্যাদি।
মেয়েদের রেইনকোটের দাম পড়বে ৬শ’ থেকে ২ হাজার টাকা। ছেলেদের ‘টু পিস রেইনকোট’গুলো পাওয়া যাবে ৭শ’ থেকে ৫ হাজার টাকায়। শিশুদের রেইনকোটের দাম ৩শ’ থেকে ১ হাজার টাকা।

একজন প্রাপ্তবয়ষ্ক মানুষের দিনে ১ হাজার মিলিগ্রাম ক্যালসিয়ামের প্রয়োজন হয়। যা প্রায় তিনটি আট আউন্স গ্লাস পরিমাণ দুধ থেকে পাওয়া যায়।
তবে আপনি যদি নিরামিষাশী হন বা দুধ যদি সহ্য না হয় অথবা দুগ্ধজাত খাবার খেতে ভালো না লাগে, তাহলে কী করবেন?
দুধ ও দুগ্ধজাত খাবার ছাড়াও অন্য অনেক খাবার থেকেই ক্যালসিয়ামের চাহিদা পূরণ করা যায়।
স্বাস্থ্য বিষয়ক একটি ওয়েবসাইট অনুসারে সেইসব খাবারের নাম এখানে দেওয়া হল।


ব্রকলি
দুই কাপ পরিমাণ ব্রকলিতে রয়েছে ৮৬ মিলিগ্রাম পরিমাণ ক্যালসিয়াম। ক্যালসিয়াম ছাড়াও ব্রকলিতে রয়েছে একটি কমলালেবুর তুলনায় দ্বিগুণ ভিটামিন সি। তাছাড়া ব্রকলি ক্যান্সারের ঝুঁকিও কমিয়ে আনতে পারে।
কমলালেবু
বড় আকারের একটি কমলালেবুতে থাকে প্রায় ৭৪ মিলিগ্রাম পরিমাণ ক্যালসিয়াম। আর এক কাপ কমলার রসে থাকে ২৭ মিলিগ্রাম ক্যালসিয়াম। তাছাড়া ভিটামিন সি এবং অ্যান্টিঅক্সিডেন্টযুক্ত খাবার হিসেবে এই ফলের জুড়ি নেই।
স্যামন মাছ
আমাদের দেশে এখন যে কোনও বড় দোকানে টিনজাত স্যামন মাছ পাওয়া যায়। আধা টিন স্যামন মাছে থাকে প্রায় ২৩২ মিলিগ্রাম ক্যালসিয়াম।


ঢ্যাঁরশ
এক কাপ পরিমাণ ঢ্যাঁরশে ক্যালসিয়ামের পরিমাণ প্রায় ৮২ মিলিগ্রাম। তাছাড়া ফাইবারযুক্ত খাবারের দারুণ একটি উৎস হলো এই সবজি।
কাজুবাদাম
এক আউন্স পরিমাণ কাজুবাদামে থাকে প্রায় ৭৫ মিলিগ্রাম ক্যালসিয়াম। তাছাড়া সারাদিনের প্রয়োজনীয় প্রোটিনের ১২ শতাংশ চাহিদা পূরণ করতে পারে এই বাদাম। এছাড়া কাজুবাদামে রয়েছে ভিটামিন ই এবং পটাশিয়াম।

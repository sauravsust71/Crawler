রেসিপি দিয়েছেন রন্ধনশিল্পী তাসনুভা মাহমুদ নওরিন।
উপকরণ: বাঁধাকপি-কুচি আড়াই কাপ। গাজর-কুচি আধা কাপ। লালমরিচ-গুঁড়া আধা চা-চামচ বা ঝাল অনুয়ায়ী। আদা ও রসুন বাটা আধা চা-চামচ করে। কর্নফ্লাওয়ার ২ টেবিল-চামচ। লবণ স্বাদ মতো। ময়দা ১ কাপ। পানি ২-৩ টেবিল-চামচ বা যতটুকু প্রয়োজন হয়। তেল ভাজার জন্য পরিমাণ মতো।
গ্রেইভির জন্য যা যা লাগবে–  তেল ৩-৪ টেবিল-চামচ। রসুনকুচি ২টি। পেঁয়াজ-কলি কুচি করা পরিমাণ মতো। পেঁয়াজ কিউব করে কাটা আধা কাপ। ক্যাপ্সিকাম কিউব করে কাটা আধা কাপ। কাঁচামরিচ-কুচি ১ চা-চামচ বা ১টি। চিলি সস ১ চা-চামচ। টমেটো সস ৪ টেবিল-চামচ। ভিনিগার ২ টেবিল-চামচ। সয়া সস ২ টেবিল-চামচ। লবণ স্বাদ মতো। চিনি ২-৩ টেবিল-চামচ বা নিজের স্বাদ মতো। লাল শুকনা-মরিচ টালা ১/৪ চা-চামচ।
পদ্ধতি: বাঁধাকপি ধুয়ে পানি ঝরিয়ে সালাদ কাটারে মিহি করে গ্রেট করে নিন। একই ভাবে গাজর কুচি করুন।
বড় বাটিতে একে একে বাঁধাকপি, গাজর, লবণ, আদা ও রসুন বাটা, লালমরিচ গুঁড়া এবং কর্নফ্লাওয়ার মিশিয়ে নিন। তারপর ১ কাপ ময়দা থেকে যতটুকু লাগে আস্তে আস্তে মিশিয়ে দরকার পড়লে পানি যোগ করুন।
বড়া বানাতে যেমন ঘন ব্যাটার লাগে ঠিক তেমন হবে। বেশি পাতলা হলে খুলে যাবে।
তেল গরম হলে গোল গোল বড়ার মতো তৈরি করুন। বাদামি করে ভেজে তুলে রাখুন।
অন্যদিকে ছোট বাটিতে সয়া সস, টমেটো সস, লবণ, ভিনিগার, শুকনা-টালা-মরিচ-গুঁড়া এবং চিলি সস মিশিয়ে রাখুন।
তেল গরম হলে রসুনকুচি বাদামি করে ভেজে পেঁয়াজ কলি দিন। তারপর কেটে রাখা পেঁয়াজের কিউব, ক্যাপ্সিকাম কিউব দুতিন মিনিট ভেজে বাঁধাকপির বড়া দিয়ে দিন।
মিনিট পাঁচেক কষিয়ে এবার তৈরি করে রাখা সস দিয়ে পরিমাণ মতো পানি দিন যেন বাঁধাকপি সিদ্ধ হয়ে আসে। বেশি নাড়াচাড়া করার দরকার নেই। নয়ত বড়া ভেঙে যাবে। বরং হাঁড়ি ধরে নেড়ে দিন।
মাঝে নাড়িয়ে চিনি দিন। সবকিছু ঠিকঠাক থাকলে যখন গ্রেইভিটা টেনে ঘন হয়ে আসবে তখন নামিয়ে নিন।
আরও রেসিপি
বাঁধাকপি রোল  

বানানো সহজ, খেতেও খুব মজা হয়। রেসিপি দিয়েছেন তামান্না জামান।
উপকরণ
সুজি ১ কাপ। তরল দুধ এককাপের তিনভাগের একভাগ। চিনি এককাপের তিনভাগের একভাগ। ঘি ২ টেবিল-চামচ। মাখন ১ টেবিল-চামচ (না দিলেও চলবে)। দারুচিনি ১ টুকরা। এলাচ ১টি। তেজপাতা ১টি। লাল রং ১ চিমটি (যদি লাল রং করতে চান)।
পদ্ধতি
প্যানে ঘি দিন। হালকা গরম হলে গরমমসলা (তেজপাতা, এলাচ, দারুচিনি) দিয়ে ভেজে নিন। গন্ধ ছড়ালে মাখন আর সুজি দিয়ে খুব ভালো করে ভেজে নিন। এবার একএক করে বাকি সব উপকরণ (দুধ,চিনি) দিয়ে ঘন ঘন নাড়তে থাকুন। আঠা আঠা হলে নামিয়ে নিন।
ঠান্ডা হলে পছন্দমতো আকার করে পরিবেশন করুন।
মিষ্টি দই ও পেস্তা-ড্রাইফ্রুট পুডিং
দুটি মিষ্টি
দইয়ের দুই পদ

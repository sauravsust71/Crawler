রেসিপি দিয়েছেন বীথি জগলুল।
উপকরণ: অর্ধেক মুরগির মাংস। বাঁধাকপির পাতা ৭,৮টি। আলু সিদ্ধ ১টি (বড়)। গোলমরিচের গুঁড়া স্বাদ মতো। মরিচগুঁড়া আধা চা-চামচ। সয়া সস ২,৩ টেবিল-চামচ। ফিস সস ২,৩ টেবিল-চামচ। আদা ও রসুন বাটা ২ চা-চামচ করে। টেম্পুরা পাউডার আধা কাপ। তেল ভাজার জন্য যতটুকু প্রয়োজন।
পদ্ধতি: আলু সিদ্ধ করে একদম পানি শুকিয়ে ফেলে চটকিয়ে রাখুন। মরুগির সঙ্গে আদা ও রসুন বাটা, গোলমরিচের গুঁড়া, মরিচগুঁড়া,

মাংস ঠাণ্ডা হলে হাড় থেকে ছাড়িয়ে নিন। তারপর ছিঁড়ে ছিঁড়ে রাখুন।
ফুটন্ত গরম পানিতে আস্ত বাঁধাকপির পাতা ভাপ দিয়ে কিচেন টাওয়েলের উপর উঠিয়ে রাখুন।
মাংসের সঙ্গে আলু সিদ্ধ মাখিয়ে সাত থেকে আট ভাগ করে নিন। একেকটি ভাগে একেকটি পাতায় মুড়িয়ে রোল তৈরি করুন।
পরিমাণ মতো পানি দিয়ে টেম্পুরার ঘন মণ্ড বানিয়ে নিন।


সস দিয়ে গরম গরম পরিবেশন করুন বিকেলের নাস্তায়।

পুষ্টিতে ভরপুর আর খেতেও দারুন। রেসিপি দিয়েছেন আতিয়া হোসেন তনী।
উপকরণ
বড় ডিম ৩টি। মোৎজারেলা চিজ ১ কাপ।
মাফিনের ফিলিং অথবা পুর: ক্যাপ্সিকাম ২টি, ছোট টুকরা করে কাটা। মাঝারি আকারের পেঁয়াজ ১টি, ছোট টুকরা করে কাটা। ছোট সসেজ ১টি, টুকরা করা।
এছাড়া ১/৪ কাপ দুধ। আধা চা-চামচ তেল। ২ টেবিল-চামচ পার্মেজান চিজ। আধা চা-চামচ বেইকিং পাউডার। লবণ ও গোলমরিচ স্বাদ মতো।
পদ্ধতি
ফ্রাইপ্যানে সামান্য তেল দিয়ে পেঁয়াজ, ক্যাপ্সিকাম, সসেজ দিয়ে সামান্য ভেজে নিতে হবে। সঙ্গে স্বাদ মতো লবণ ও মরিচ দেবেন।


প্রথমে ফিলিং দিয়ে এর উপর মোৎজারেলা চিজ দিয়ে, ডিমের মিশ্রণ ঢেলে দিন। প্রিহিট করা ওভেনে ১৯০ ডিগ্রি সেলসিয়াসে ১৫ থেকে ২০ মিনিট বেইক করুন।
তারপর বের করে কিছুটা ঠাণ্ডা হলে ট্রে থেকে উঠিয়ে পরিবেশন করুন।
সমন্বয়ে: ইশরাত মৌরি।
আরও রেসিপি:
ডিম কোরমা
সবজি-ডিমের শিঙ্গাড়া
ডিমপিঠা

এসম্পর্কে উপদেশ দিয়েছেন ডেনভার এবং এনভি থাউজেন্ড নামক প্রসাধনী ব্র্যান্ডগুলোর প্রস্তুতকারক ও ভারতীয় প্রতিষ্ঠান ভেনেসা কেয়ার প্রাইভেট লিমিটেডের পরিচালক সৌরভ গুপ্তা।
শরীরের দুর্গন্ধ, ঘাম বা অন্যান্য কারণে শরীরের আর্দ্রভাব দূর করতে অ্যান্টি-ব্যাকটেরিয়াল ট্যালকম পাউডার ব্যবহার করা যেতে পারে।
তোয়ালে-মোছা শরীরে ঠাণ্ডাভাব হয় এমন ট্যালকম পাউডার ব্যবহার করলে উপকার পেতে পারেন। সামান্য পরিমাণে পাউডার গায়ে ছিটিয়ে শরীরে ঠাণ্ডাভাব ছড়িয়ে দিতে হবে। ফলে আদ্র আবহাওয়াতেও মিলবে শুষ্কভাব।   
ডিওডোরেন্ট বা বডি স্প্রে হওয়া উচিৎ প্রতিদিনের সঙ্গী। বাইরে যাওয়ার আগে নিয়মিত এগুলো ব্যবহার করতে হবে। আর বেছে নিতে হবে দীর্ঘস্থায়ী এবং কড়া সুগন্ধযুক্ত ডিওডোরেন্ট বা বডি স্প্রে।
পছন্দের সুগন্ধি হওয়া উচিৎ আপনার ব্যক্তিত্বের পরিচয়। অনন্য একটি সুগন্ধি আপনার নিজস্ব পরিচয়পত্রের মতো, যা আপনার ব্যক্তিত্বকে অন্যদের মাঝে আলাদাভাবে তুলে ধরে।
সঠিক সুগন্ধি আবার আপনার মানসিকতা, উপলক্ষ্য এবং ঋতুর উপরেও নির্ভরশীল। আপনার ব্যক্তিত্ব এবং উপস্থাপনাকে প্রবল করে এমন সুগন্ধি বেছে নেওয়া উচিৎ।  
উপলক্ষ্য অনুযায়ী বেছে নিতে হবে মানানসই পারফিউম। গরম এবং আর্দ্র আবহাওয়ায় নিজেকে চাঙা রাখতে ব্যবহার করতে পারেন লেবুজাতীয় সুগন্ধির সঙ্গে ফার্নজাতীয় মাত্রা মেশানো পারফিউম। আর নিজেকে আবেদনময় এবং রহস্যময় হিসেবে তুলে ধরতে চাইলে ব্যবহার করতে পারেন ঝাঁঝালো মাত্রাযুক্ত পারফিউম।
বেশিরভাগ ডিওডোরেন্ট শরীরের দুর্গন্ধ ঢেকে রাখে, তবে এরমধ্যে মাত্র কয়েকটি ব্যাকটেরিয়ার সঙ্গে লড়াই করে। ডিওডোরেন্ট কাপড়ে স্প্রে না করে শরীরে স্প্রে করা উচিৎ।
দুর্গন্ধ এড়াতে সুগন্ধি তেলও বেশ কার্যকর। পানিতে কয়েক ফোঁটা সুগন্ধি তেল মিশিয়ে গোসল করলে দুর্গন্ধ থেকে মুক্তি পেতে পারেন।
পারফিউম ব্যবহারের আগে হালকা ময়েশ্চারাইজার মেখে শরীরকে পারফিউম ব্যবহারের উপযোগী করে নেওয়া উচিৎ।
অতিরিক্ত সুগন্ধি মেখে শরীরের দুর্গন্ধ ঢেকে রাখার চেষ্টা করা উচিৎ নয়। বরং এতে হিতে বিপরীত হতে পারে।
সুগন্ধি বেছে নেওয়ার উপায়
সুরভিত সুগন্ধি

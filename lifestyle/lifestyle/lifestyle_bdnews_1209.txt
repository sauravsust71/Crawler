রেসিপি দিয়েছেন পাপন শ্রাবণ  
উপকরণ
শোল বা টাকিমাছ ৪-৫ টুকরা। পেঁয়াজবাটা ২ টেবিল-চামচ। আদাবাটা আধা চা-চামচ। রসুনবাটা আধা চা-চামচ। হলুদগুঁড়া আধা চা-চামচ। মরিচগুঁড়া আধা চা-চামচ। ধনেগুঁড়া চা-চামচের এক তৃতিয়াংশ। সরিষার তেল ২ টেবিল-চামচ। আস্ত কাঁচামরিচ ৩-৪টি। ধনেপাতা ২ টেবিল-চামচ। লবণ স্বাদমতো।
পদ্ধতি
লাউয়ের খোসা ছাড়িয়ে ছোট টুকরা করে নিন। কড়াইতে তেল গরম করে পেঁয়াজ আর আদাবাটা দিয়ে একটু লালচে করে ভাজুন। অল্প পরিমাণ গরম পানি দিন।
এবার রসুনবাটা, মরিচগুঁড়া, ধনেগুঁড়া দিয়ে ভালো করে কষিয়ে নিন। লবণ দিন আর সঙ্গে মাছগুলো দিয়ে কিছুক্ষণ ঢেকে রাখুন।
মসলায় মাখা মাখা হলে মাছগুলো উঠিয়ে আলাদা রেখে দিন। কড়াইয়ের বাকি মসলায় লাউগুলো দিয়ে আর একটু নেড়ে ঢেকে দিন। পানি দিবেন না। লাউ থেকে যে পানি বের হবে তাতেই লাউ সিদ্ধ হয়ে যাবে।
কিছুক্ষণ পর ঢাকনা সরিয়ে আরেকটু নেড়ে মাছগুলো দিয়ে আবার ঢেকে দিন।
মাছ আর লাউ মোটামুটি রান্না হয়ে আসলে আস্ত কাঁচামরিচ আর ধনেপাতা দিয়ে আরও দুই মিনিট রান্না করে নামিয়ে পরিবেশন করুন।
 
ভর্তা ও তরকারি
সবজি-ডাল
ভেটকি মাছ আর সবজি
বেগুনে ভাজা মাছ
মাছের ঝাল-মালাইকারি

সম্প্রতি সৌন্দর্য চর্চায় ভিনিগার যেন প্রয়োজনীয় উপাদান হয়ে দাঁড়িয়েছে।
অভিনেত্রী মেগান ফক্সের সুন্দর দাঁতের রহস্য নাকি ভিনিগার। সে প্রায়ই পানিতে অ্যাপল সাইডার ভিনিগার মিশিয়ে দাঁত পরিষ্কার কারেন। তিনি দাবি করেন, এটা শরীরের ভিতরটাও পরিষ্কার রাখে।
ডেইলিমেইল ডটকো. ডটইউকের তথ্য অনুসারে, সুপার মডেল মিরান্ডা কের তার সালাদে ব্যবহার করেন ভিনিগার। ওদিকে অভিনেত্রী গিনেথ প্যাল্ট্রো ও গায়িকা ম্যাডোনা নিজেদের সুন্দর দেখাতে এই তরলের উপর নির্ভির করেন।
খাবারের দোকান, দি রো ডাক ইন লন্ডন'স হ্যাকনি তাদের মেন্যুতে এক ধরনের খামির রেখেছেন। যা অ্যাপল সাইডার ভিনিগারের সঙ্গে চিনি ও ঝুরা আপেল দিয়ে মিশিয়ে তিনদিন ধরে প্রক্রিয়াজাত করে তৈরি করা হয়।
দোকানের মালিক ররি ম্যাককয় বলেন, "এটা খুবিই সুস্বাদু ও হজমে সহায়ক।"
তিনি আরও বলেন, "প্রোবায়োটিক হিসেবে খাওয়ার জন্য নানান ধরনের জিনিস রয়েছে। তবে এটা সত্যি কার্যকর। স্বাস্থ্য ভালো রাখার জন্য আমি প্রতিদিনি ভিনিগার পান করার চেষ্টা করি বা ওই খামিরটা খাই।"
দি নিউটরি সেন্টারের পুষ্টিবিদ কেইটি মাসোন জানান, প্রায় একশ বছরের বেশি সময় ধরে আচার তৈরি ও স্বাস্থ্য ভালো রাখার জন্য ভিনিগার ব্যবহার করা হচ্ছে।

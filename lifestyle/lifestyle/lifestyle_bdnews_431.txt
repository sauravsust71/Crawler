তাই তিন রকম লাড্ডু বানানোর রেসিপি দিয়েছেন রন্ধনশিল্পী তামান্না জামান।
 


উপকরণ
মিষ্টিআলু ৩০০ গ্রাম। মাখন ২ টেবিল–চামচ। গুঁড়াদুধ ২ টেবিল–চামচ। তরল দুধ ১ কাপ। তেল এক কাপের তিনভাগের একভাগ। চিনি আধা কাপ। ঘি ১ টেবিল-চামচ। তেজপাতা ২টি। এলাচ ৩টি। দারুচিনি ৩ টুকরা।
পদ্ধতি
মিষ্টিআলু সিদ্ধ করে উপরের চামড়া ছাড়িয়ে নিন। এবার টুকরা করে কেটে, দুধ আর আধা কাপ পানি দিয়ে ব্লেন্ডারে মেশান।
ননস্টিক প্যানে তেল গরম করে এতে গরম-মসলাগুলো (এলাচ, তেজপাতা, দারুচিনি) দিয়ে অল্প আঁচে নাড়তে থাকুন। গরম মসলার গন্ধ ছড়িয়ে গেলে মিষ্টিআলুর মিশ্রণ দিয়ে আবার নাড়তে থাকুন।
এরপর গুঁড়াদুধ আর মাখন দিয়ে নাড়ুন। ৫-৬ মিনিট নেড়ে চিনি দিন। মিষ্টি আলু খুব সহজে প্যানে লেগে যায় তাই বেশ ঘন ঘন নাড়তে হবে। যখন দেখবেন তেল উপরে উঠে এসেছে আরও ঘি দিয়ে নাড়তে থাকুন। আর কিছুক্ষণ নেড়েই নামিয়ে ফেলুন। ঠাণ্ডা হতে দিন।
ঠাণ্ডা হয়ে গেলে হাতে তেল মাখিয়ে গোল গোল করে লাড্ডুর আকার বানিয়ে নিন। এবার পরিবেশন করুন মজাদার মিষ্টিআলুর লাড্ডু। তবে ফ্রিজে ২-৩ ঘণ্টা রেখে তারপর খেলে বেশি ভালো লাগবে।
 


উপকরণ
বেসন ১ কাপ। গুঁড়াদুধ ১ টেবিল–চামচ। আধা চা-চামচ মাখন। ঘি ১ চা–চামচ। তরলদুধ পরিমাণ মতো। চিনি স্বাদমতো। তেল ১ টেবিল–চামচ। তেজপাতা ১টি। দারুচিনি ২টি। এলাচ ২টি।
পদ্ধতি
প্রথমে গুঁড়াদুধ আর বেসন একসঙ্গে মিশিয়ে নিন। এরপর মাখন আর সামান্য দুধ (মাখা মাখা হওয়ার জন্য যতটুকু প্রয়োজন) দিয়ে বিটার (যে কোনো) দিয়ে বিট করে নিন ভালো করে।
এরপর প্যানে তেল গরম করে তাতে তেজপাতা, দারুচিনি আর এলাচ দিয়ে নেড়ে গন্ধ ছড়িয়ে গেলে বেসনের মিশ্রণটি দিয়ে দিন আর চুলার আঁচ কমিয়ে দিন।
পরিমাণ মতো চিনি দিয়ে নাড়তে থাকুন। এবার তরল দুধ শুকিয়ে আসলে ঘি দিয়ে নাড়তে থাকুন। যখন দেখবেন তেলতেলে হয়ে গিয়েছে, নামিয়ে ফেলবেন।
এবার মন মতো কিংবা গোল আকার করে একটু ঠাণ্ডা করে পরিবেশন করুন।
 


উপকারণ
গাজর ৩ কেজি। গুঁড়াদুধ ২ টেবিল–চামচ। লবণ ১ চা-চামচ। ঘি ১ চা–চামচ। চিনি স্বাদমতো। তেল পরিমাণমতো। তেজপাতা ১টি। দারুচিনি ২টি। এলাচ ২টি।
পদ্ধতি

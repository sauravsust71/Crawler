এবিষয়ে পরামর্শ দিয়েছেন অনলাইন ভিত্তিক পোশাকের ব্র্যান্ড পোস্ট ফোল্ডের সহ-প্রতিষ্ঠাতা আশীষ গুরনানি।
* অফিসে প্যান্টের ভেতর শার্ট গুঁজে পরা আবার সেই শার্টই অফিস শেষে প্যান্টের উপর দিয়ে পরলে আপনাকে দিতে পারে একই পোশাকে দুরকমের ‘লুক’। এরসঙ্গে ‘ক্লাসিক’ ধাঁচে তৈরি করা ব্লেজার আপনার পোশাকে আনবে তীক্ষ্ণতা।
* কনুইতে আলগা কাপড় বা ‘এলবো প্যাচ’ দেওয়া ক্লাসিক ধাঁচের আধুনিক ব্লেজারগুলো পোশাকে আনতে পারে অভিজাত ভাব। পরতে হবে চেক শার্টের উপরে।
* কলার আর হাতে গাঢ় রঙের ইলাস্টিক দেওয়া বেসবল জ্যাকেটগুলো পরার উপযুক্ত সময় শীতকাল।
* প্রিন্টেড সোয়েটারও বেশ উপযোগী পোশাক। কাঁধে হালকা ডিজাইন বা নকশা থাকলে সান্ধ্যকালীন ঘোরাঘুরির জন্য উপযুক্ত পোশাক এটি।

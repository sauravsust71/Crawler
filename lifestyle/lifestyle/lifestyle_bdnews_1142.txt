নতুন এক গবেষণায় দেখা গেছে ইমালসিফায়ার নামক যে বস্তুটি বেশিরভাগ প্রক্রিয়াজাত খাবারে দেখতে সুন্দর ও বেশিদিন টেকার জন্য ব্যবহার করা হয় তা আসলে অন্ত্র বা পেটের ব্যথার কারণ হতে পারে। খাবারের এই বাড়তি সংযুক্তি অন্ত্রে মাইক্রোবাইয়োটার গঠনের ধরন বদলে দেয়। আর এই বদল পেটে ব্যথা ও হজমরোগ তৈরি করে।
অন্ত্রের রোগ বা ইনফ্ল্যামাটোরি বাওয়াল ডিজিজ (আইবিডি) থেকে মলাশয়ে ব্যথা এবং নাড়িভুঁড়ির রোগ হতে পারে।
‘‘এই আধুনিক প্লেইগ অন্ত্রের মাইক্রোবাইয়োটা এমনভাবে বদলে দেয় যা থেকে পেটে ব্যথা তৈরি করে’’ বলেন প্রধান গবেষক জর্জিয়া স্টেইট বিশ্ববিদ্যলয়ের অ্যান্ড্রু টি. গেয়ার্টজ।
তিনি আরও বলেন ‘‘খাবার খুব ঘনিষ্ঠভাবে মাইক্রোবাইয়োটার সঙ্গে ক্রিয়া করে থাকে। তাই এইসব আধুনিক খাবারে যেসব বাড়তি জিনিস যোগ করা হয়, তা থেকে সম্ভবত অন্ত্রেপ্রদাহের কারণ হতে পারে।’’
স্থূলতার সঙ্গে সম্পর্কিত এই ধরনের পেটের অসুখ থেকে টাইপ টু ডায়াবেটিস, হৃদপিণ্ডসম্বন্ধীয় বা যকৃতের রোগ হতে পারে।
গবেষক দলটি ইঁদুরের ওপর নিরীক্ষা চালান। প্রক্রিয়াজাত খাবারে বেশি ব্যবহার করা হয় এরকম উপাদান ইমালসিফায়ার, পলিসরবেট ৮০ ও কার্বোক্সিমেথিলসেলুলোজ ইঁদুরগুলোকে খাওয়ানো হয়। গবেষক দল দেখতে পান, এই ইমালসিফায়ারগুলো পেটের মাইক্রোবাইয়োটার গঠন এমনভাবে বদলে দেয় যা দ্রুত অন্ত্রপ্রদাহ তৈরি করে।     
ব্যাকটেরিয়ায় এই বদল সাধারণত ফ্ল্যাজেলিন ও লিপোপলিস্যাকারাইডের মাত্রা বাড়িয়ে দেয়। ফলে ইমিউন সিস্টেম বা হজম প্রক্রিয়ায় ব্যথা তৈরি করে।
গেয়ার্টজ বলেন, ‘‘বেশি খাওয়ার ফলে মুটিয়ে যাওয়ার এবং বিপাকীয় সমস্যার প্রধান কারণ এই ব্যাপারে আমাদের কোনো দ্বিমত নেই।’’
গবেষণার ফলাফলের ভিত্তিতে গবেষকরা জানান, খাবারে বাড়তি আকর্ষণ আনতে রাসায়নিক পদার্থের ব্যবহার পরীক্ষা করার জন্য পর্যাপ্ত প্রতিরোধ ব্যবস্থা হয়ত নেই। যা থেকে স্বল্পমাত্রায় পেটের ব্যথা থেকে প্রাথমিকভাবে রোগ সৃষ্টি করতে পারে।
গবেষণাটি ‘নেইচার’য়ে প্রকাশিত হয়।
ঘরোয়া উপায়ে গ্যাস্ট্রাইটিস নিরাময়
অভ্যাসে পিঠে ব্যথা
পেটের মেদ না কমার কারণ

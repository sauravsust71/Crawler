আর এর প্রতিষ্ঠা করেছিলেন খান-ই-জাহান (র) নামে এক সাধক পুরুষ। বেশ কিছু প্রাচীন স্থাপনা বিশেষ করে মসজিদ এখনও সে ইতিহাসের সাক্ষ্য বহন করে চলছে। ঐতিহাসিক ষাট গম্বুজ মসজিদসহ খানজাহান আলীর এসব প্রাচীন স্থাপনা ইউনেস্কো ঘোষিত বিশ্ব ঐতিহ্যের তালিকায় স্থান পায় ১৯৮৫ সালে।
প্রাচীন মসজিদের শহর বাগেরহাটে গিয়ে ঘুরে দেখে আসতে পারেন এসব প্রাচীন ঐতিহ্য।
হযরত খানজাহান মাজার মসজিদ
হযরত খানজাহানের (র) সমাধিসৌধ লাগোয়া ঠিক পশ্চিম পাশেই বেশ পুরানো মসজিদ। খান জাহানের সমাধিসৌধের গঠন শৈলিতে নির্মিত এই মসজিদ তাঁর সময়কালেই নির্মিত বলে ধারণা করা হয়। এক গম্বুজ বিশিষ্ট মসজিদের পশ্চিম দেয়ালে একটি মিহরাব আছে।
জানা যায় হযরত খান জাহান (র) বাগেরহাটে এসেছিলেন ১৩৯৮ খৃষ্টাব্দের পরে। তিনি প্রথমে দিল্লির সুলতানের নিকট থেকে এবং পরে বাংলার সুলতানের নিকট থেকে সুন্দরবন অঞ্চলের জায়গির লাভ করেন। এখানকার গভীর বন কেটে তিনি মুসলিম বসতি গড়ে তোলেন।
১৪৫৯ খৃষ্টাব্দের ২২ অক্টোবর খান জাহান আলীর (র) মৃত্যু হলে এ মসজিদের পাশেই তাঁকে সমাহিত করা হয়।
জিন্দাপীর মসজিদ
হযরত খানজাহান (র) মাজার মসজিদের পশ্চিম দিকে ঠাকুর দিঘির পশ্চিম পাড়ে সুন্দরঘোনা গ্রামে আছে ইট নির্মিত এক গম্বুজ বিশিষ্ট জিন্দাপীর মসজিদ। মসজিদের পাশেই হযরত খানজাহানের অনুসারী জিন্দাপীরের সমাধি। তাঁর নামেই এ মসজিদের নামকরণ। এর পূর্ব দেয়ালে তিনটি এবং উত্তর ও দক্ষিণ দেয়ালে একটি করে দরজা আছে। মসজিদের চার কোণে চারটি আট কোণাকার বুরুজ আছে।   
নয় গম্বুজ মসজিদ
খান জাহান আলীর (র) সমাধির দক্ষিণ-পূর্ব দিকে ঠাকুর দিঘীর পশ্চিম পাড়ে অবস্থিত নয় গম্বুজ মসজিদ। নামেই বোঝা যায় এর উপরে রয়েছে নয়টি গম্বুজ।
ইটের তৈরি পুরো মসজিদের গায়ে পোড়ামাটির কারুকাজ খচিত। বর্গাকারে তৈরি এই মসজিদ একেক পাশের দৈর্ঘ্য ১৫.২৪ মিটার। মসজিদের দেয়ালগুলো ২.৩৪ মিটার পুরু।
মসজিদের ছাদ নয়টি নিচু অর্ধ বৃত্তাকার গম্বুজ দিয়ে ঢাকা। গম্বজুগুলো মসজিদের ভেতরে চারটি পাথরের পিলারের উপর স্থাপিত।
মসজিদের পূর্ব, উত্তর ও পূর্ব দেয়ালে তিনটি করে প্রবেশ পথ আছে। পশ্চিম দেয়ালে আছে তিনটি অলঙ্কৃত মিহরাব। 
ষাটগম্বুজ মসজিদ


মসজিদের ছাদে ৭৭টি এবং চারকোণে মিনারে ৪টি।
তবে মসজিদের ভেতরের ষাটটি স্তম্ভ থাকার কারণে এর নাম হতে পারে ষাট গম্বুজ। স্তম্ভগুলো কালো পাথরের তৈরি। তবে অদক্ষ সংস্কারের নামে স্তম্ভগুলোর কালো পাথর পলেস্তারের প্রলেপে ঢেকে দেওয়া হয়েছে।
শুধু উত্তর দিকের একটি স্তম্ভ খালি রাখা হয়েছে দর্শনার্থীদের দেখার জন্য।
খান জাহানের সবচেয়ে উল্লেখযোগ্য স্থাপত্যকীর্তি এটি। ধারণা করা হয় ষাট গম্বুজ মসজিদটি তিনি নির্মাণ করেছিলেন ১৪৫৯ খৃষ্টাব্দের কিছুকাল আগে। 
সিংড়া মসজিদ
ষাট গম্বুজ মসজিদের প্রায় তিনশ গজ দক্ষিণ-পূর্বে সুন্দরঘোনা গ্রামের বাগেরহাট-খুলনা মহাসড়কের পাশে অবস্থিত প্রাচীন একটি স্থাপনা সিংড়া মসজিদ। বর্গাকারে তৈরি মসজিদের প্রত্যেক বাহু বাইরের দিকে ৩৯ ফুট এবং ভেতরের দিকে ২৫ ফুট লম্বা।
ইটের তৈরি মসজিদের প্রাচীরগুলি প্রায় ৭ ফুট প্রশ্বস্ত। মসজিদের পূর্ব দেয়ালে আছে তিনটি প্রবেশপথ। এর বরাবর পশ্চিম দেয়ালে রয়েছে তিনটি অলংকৃত মিহরাব।
তবে কেন্দ্রীয় মিহরাবটি অপেক্ষাকৃত বড় এবং সুসজ্জিত। উত্তর ও দক্ষিণ দিকের দেয়ালে একটি করে প্রবেশ পথ আছে। এই মসজিদের নির্মাণকাল সম্পর্কে সুস্পষ্ট কোনো তথ্য পাওয়া যায় না। তবে নির্মাণ শৈলী বিবেচনা করে ঐতিহাসিকগণ মনে করেন সিংড়া মসজিদের নির্মাণকাল আনুমানিক পঞ্চদশ শতাব্দির মাঝামাঝি।  
রণবিজয়পুর মসজিদ


ইটের তৈরি মসজিদে বর্গকারে তৈরি।
এক কক্ষ বিশিষ্ট এই মসজিদের উপরের দিকে একটি অর্ধবৃত্তাকার গম্বুজ দিয়ে ঢাকা। মসজিদের দেয়ালগুলো বেশ পুরু।
কিবলা দেয়াল ছাড়া প্রতি দেয়ালেই তিনটি করে প্রবেশপথ আছে। পূর্ব দেয়ালের তিনটি প্রবেশপথ বরাবর পশ্চিম দেয়ালে রয়েছে তিনটি মিহরাব।
কেন্দ্রীয় মিহরাবটি অন্য দুটি থেকে বড়। মসজিদের বাইরে চারকোণায় চারটি মিনার রয়েছে। যেগুলো খানজাহানি স্থাপত্যের অন্যতম নিদর্শন।
১৯৬১ সালে এটিকে সংরক্ষিত স্থাপনা হিসেবে ঘোষণা করা হয় এবং সংস্কার করা হয়।
রণবিজয়পুর গ্রামের নামেই এই মসজিদের নামকরণ হয়েছে। ধারণা করা হয় এখানে কোনো এককালে যুদ্ধ সংঘটিত হয়েছিল। সেই যুদ্ধে বিজয়ের স্মরণে এই জায়গার নাম হয় রণবিজয়পুর। বাইরের দিক থেকে এর আয়তন ৫৬ বর্গ ফুট এবং ভেতরে দিকে ৩৬ বর্গফুট।
মসজিদের প্রাচীর প্রায় ১০ ফুট চওড়া। রণবিজয়পুর মসজিদ বাংলাদেশের সবচেয়ে বড় এক গম্বুজ বিশিষ্ট মসজিদ।
চুনাখোলা মসজিদ


বর্গাকৃতি মসজিদের বাইরের দিকে লম্বায় প্রায় ৪০ ফুট এবং ভেতরের দিকে ২৫ ফুট। দেয়ালগুলো প্রায় আট ফুট চওড়া। কেন্দ্রস্থলের উপরের দিকে রয়েছে বড় একটি গম্বুজ।
মসজিদের পূর্ব দেয়ালে আছে তিনটি প্রবেশপথ। আর পশ্চিম দেয়ালে প্রবেশপথগুলো বরাবর রয়েছে তিনটি মিহরাব। মাঝের মিহরাবটি অপেক্ষাকৃত বড়। প্রতিটি মিহরাবের গায়ে রয়েছে নানারকম ফুল ও লতাপাতার কারুকাজ।
মিহরাবগুলোর নিচের অংশ মাটির ভেতরে কিছুটা দেবে গেছে। উত্তর ও দক্ষিণ দেয়ালে আছে একটি করে প্রবেশ পথ।
চুনাখোল মসজিদে এখনও নিয়মিত নামাজ আদায় করা হয়। মসজিদটির নির্মাণকাল সম্পর্কে সঠিক কোনো তথ্য পাওয়া যায়নি। 
বিবি বেগনী মসজিদ
ষাট গম্বুজ মসজিদ থেকে প্রায় এক কিলোমিটার পশ্চিমে অবস্থিত এক গম্বুজ বিশিষ্ট একটি প্রাচীন মসজিদ। যা সিংড়া মসজিদের অনুরূপ হলেও এর পশ্চিম দেয়ালে মিহরাবের সংখ্যা তিনটি। মসজিদটির সঠিক নির্মাণকাল জানা যায় না।
সাবেকডাঙ্গা পুরাকীর্তি
খান জাহানের (র) স্থাপত্যশৈলীর সঙ্গে সামঞ্জস্যপূর্ণ এই পুরাকীর্তি বাগেরহাট জেলার সাবেকডাঙ্গা গ্রামে অবস্থিত। লাল ইটের তৈরি আয়তকার এ ভবন একেক পাশে দৈর্ঘ্য ৭.৮৮ নমিটার।
ভবনটির দক্ষিণপাশে কেবল একটি প্রবেশপথ আছে। এর ভেতরে আর কোনো দরজা, জানালা কিংবা মিহরাব নেই। তাই স্থাপনাটি মসজিদ হিসেবে বিবেচনা করা হয়নি।
জনশ্রুতি আছে খান জাহান আলী (র) তাঁর বিশেষ প্রার্থণার জন্য এটি নির্মাণ করেন।
কীভাবে যাবেন


ঢাকার সয়াদবাদ থেকে মাওয়া হয়ে বাগেরহাটের পথে যাতায়াত করে মেঘনা পরিবহন, ফাল্গুনী পরিবহন, হামিম পরিবহন, আরা পরিবহন, পর্যটক পরিবহন, দোলা পরিবহন, বনফুল পরিবহন, সুন্দরবন পরিবহন ইত্যাদি।
ভাড়া আড়াইশ থেকে সাড়ে ৩শ’ টাকা।
গাবতলী থেকে পাটুরিয়া হয়ে এই পথে যাতায়াত করে ঈগল পরিবহন, সৌখিন পরিবহন, হানিফ এন্টারপ্রাইজ, সাকুরা পরিবহন ইত্যাদি। ভাড়া ৩শ’ থেকে সাড়ে ৩শ’ টাকা। 
ঢাকা থেকে বাংলাদেশ আভ্যন্তরীণ নৌ পরিবহন সংস্থার প্যাডেল স্টিমারে (৯৫৫১৮৪৬, ৯৫৫৮০০০) বাগেরহাটের মোড়েলগঞ্জ উপজেলা সদরে যাওয়া যায়। সদরঘাট থেকে শুক্রবার ছাড়া সপ্তাহের প্রতিদিনই ছেড়ে যায় একটি করে স্টিমার।
মোড়েলগঞ্জ থেকে বাসে বাগেরহাট সদরে যেতে সময় লাগে দেড় ঘণ্টার মতো। এছাড়া খুলনা থেকেও সহজে বাগেরহাটে যাওয়া যায়।
কোথায় থাকবেন
বাগেরহাট শহরে থাকার জন্য সাধারণ মানের হোটেল আছে। কেন্দ্রীয় বাস স্টেশন সংলগ্ন হোটেল আল আমিন (০৪৬৮-৬৩১৬৮, ০১৭১৮৬৯২৭৩৭, এসি দ্বৈত কক্ষ ১ হাজার টাকা, নন এসি কক্ষ ১শ’ থেকে ৪শ’ টাকা)।
কর্মকার পট্টিতে হোটেল মোহনা (০৪৬৮-৬৩০৭৫, ০১৭২২৮৫৮৩১৩, ১শ’ থেকে ৪শ’ টাকায় নন এসি কক্ষ)।
বাগেরহাটের বিশ্ব ঐতিহ্য সব ছবি দেখতে ক্লিক করুন।
নবাবগঞ্জ জাতীয় উদ্যান
প্রাচীন শহর মোহাম্মদাবাদ
ঘুরে আসুন অযোধ্যা মঠ
বিমান জাদুঘর

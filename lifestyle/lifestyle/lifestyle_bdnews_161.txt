রাস্তার ধুলাবালি, কাদা, পানি ইত্যাদি বিভিন্ন কারণে হিলের সৌন্দর্য কমে যায়। তবে শখের জুতার সঠিক যত্ন নিলে টাকা খরচ করে কেনা হিল-জুতা দীর্ঘদিন ভালো রাখা যায়।
ফ্যাশন বিষয়ক একটি সাইটে জুতার যত্ন সম্পর্কে কিছু পন্থা জানানো হয়।
- হাই হিলের জুতা বা স্যান্ডেল ব্যবহারের ফলে সব থেকে বেশি ক্ষতিগ্রস্ত হয় এর হিল। তাই হিল কেনার পর মুচির কাছে নিয়ে হিলের নিচে একটি পাতলা রাবারের সোল লাগিয়ে নেওয়া যায়। জুতার রংয়ের সঙ্গে মিলিয়ে আলাদা রাবারের সোল লাগিয়ে নিলে হিল সহজে নষ্ট হয় না।
- যে কোনো জুতাই ভালো রাখতে যত্নের প্রয়োজন। অন্যদিকে চামড়ার জুতার ক্ষেত্রে প্রয়োজন আরও বেশি যত্ন। কন্ডিশনার দিয়ে ভালোভাবেই বিশেষ যত্ন করা যায়। যা জুতার চামড়া রাখে চকচকে। আ টিকেও বেশি দিন।
প্রথমে জুতা ভালোমতো পরিষ্কার করে একটি নরম ব্রাশে কন্ডিশনার লাগিয়ে পুরো জুতায় লাগাতে হবে। ১০ মিনিট পর শুকনা কাপড় দিয়ে কন্ডিশনার মুছে ফেলতে হবে। এতে জুতা চকচকে হবে।
- জুতা বেশিদিন ফেলে রাখলে ধুলা ময়লা জমে নষ্ট হয়ে যেতে পারে। আর হাই হিল এবং দামি জুতাগুলো সবসময় ব্যবহার করা হয় না। তাই জুতাগুলো সঠিকভাবে সংরক্ষণ করা জরুরি।
একটি বক্সে রেখে ভালোভাবে আটকে দামি জুতাগুলো সংরক্ষণ করা উচিত। তাছাড়া জুতা সংরক্ষণের আগে ধুলা আটকাবে এমন কোনো ব্যাগে ভরে জুতা রাখার ব্যবস্থা করতে হবে।
তবে জুতা, বাক্সের ভিতর বা যেখানেই রাখা হোক না কেনো মাঝে মাঝে বাতাসের সংস্পর্শে আনা প্রয়োজন, নইলে জুতা নষ্ট হয়ে যেতে পারে। তাই মাঝে মাঝে জুতা বের করে কিছুক্ষণ আলোতে রেখে পরিষ্কার করে আবার সংরক্ষণ করা উচিত।

ডায়াবেটিস রোগের সাথে আমরা কমবেশি সবাই পরিচিত। মহামারীর মতো ছড়িয়ে পড়ছে এ রোগ। তবে রোগটি প্রতিরোধযোগ্যও। ডায়াবেটিস হওয়ার ঠিক আগের অবস্থাটিকে আমরা প্রি-ডায়াবেটিস বলি। মানে রক্তের সুগার স্বাভাবিকের চেয়ে বেশি কিন্তু এখনো ডায়াবেটিস হবার মতো পর্যায়ে পৌঁছায়নি। প্রি-ডায়াবেটিস থেকে ডায়াবেটিস হওয়ার সম্ভাবনা থাকে পরবর্তী দশ বছরের মধ্যে। তবে কিছু অভ্যাস এবং জীবন প্রণালীর পরিবর্তন করে ডায়াবেটিস হওয়ার ঝুঁকি অনেকাংশে কমিয়ে দেওয়া যায়। যেমন-
১. শরীরের সঠিক ওজন নিয়ন্ত্রণ- প্রতিটি মানুষের নারী ও পুরুষভেদে উচ্চতা অনুযায়ী একটি নির্দিষ্ট ওজনসীমা থাকে। যা নিয়ন্ত্রণ করা খুবই জরুরি। কম ক্যালরিসম্পন্ন সুষম খাবার গ্রহণ করে সবাই নিজের ওজনসীমা নিয়ন্ত্রণ করতে পারেন।
২. খাদ্যাভ্যাস পরিবর্তন- শর্করা, কার্বোহাইড্রেট জাতীয় খাবার যেমন ভাত, রুটি, আলু, চিনি ইত্যাদি খাবার যতটা সম্ভব কম আহার করুন। পেট ভরে সবুজ শাক-সবজি ও সালাদ আহার করলে রক্তের সুগার থাকবে নিয়ন্ত্রণে। যে কোনো মাংসের চেয়ে মাছ বেছে নিন আপনার খাদ্য তালিকায়।
৩. নিয়মিত হাঁটুন- ফিনিস একটি গবেষণায় দেখা গেছে, যেসব লোক বেশি ব্যায়াম করেছেন, সপ্তাহে ৪ ঘণ্টা পর্যন্ত বা দিনে ৩৫ মিনিট, তাদের ডায়াবেটিসের ঝুঁকি কমে গেছে স্বাভাবিকের চেয়ে প্রায় ৮০ শতাংশ পর্যন্ত। ব্যায়াম বা হাঁটা খুবই স্বাস্থ্যকর। কারণ এতে ইনসুলিনের রিসেপ্টর সংখ্যা বাড়ে। এ ইনসুলিন রক্তের সুগারকে কোষের ভেতর ঢুকতে সাহায্য করে এবং শরীরে সুগার বা গ্লুকোজ দহন করে পায় শক্তি ও পুষ্টি।
৪. ফাস্টফুড পরিহার করুন- যারা নিয়মিত ফাস্টফুড গ্রহণ করেন তাদের ডায়াবেটিস হওয়ার আশঙ্কা স্বাভাবিকের চেয়ে বেশি। কারণ ফাস্টফুডে আছে অস্বাস্থ্যকর ট্রান্সফ্যাট এবং প্রচুর সুগার (শর্করা)।
৫. মশলাযুক্ত খাবার খান- জার্মান গবেষকগণ বলেছেন, ১ গ্রাম দারুচিনি খেলে রক্তের সুগার কমে ১০ শতাংশ। এ মসলাটি রক্তের চর্বি কমাতেও সাহায্য করে।
৬. দুশ্চিন্তা ঝেড়ে ফেলুন- যে কোনো ধরনের দুশ্চিন্তা রক্তের সুগারের পরিমাণ বাড়িয়ে দেয়। তাই চেষ্টা করুন চাপহীন জীবন কাটাতে। পরিবারের সাথে সময় কাটান, ভালো থাকুন।
লেখক: চর্ম ও যৌনরোগ বিশেষজ্ঞ, বাংলাদেশ স্কিন সেন্টার
ইত্তেফাক
/মোস্তাফিজ

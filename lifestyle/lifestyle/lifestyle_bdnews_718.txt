

রেসিপি দিয়েছেন রন্ধনশিল্পী সাইমা  জাহান।
উপকরণ: গাজর ৫০০ গ্রাম। নারকেল বাটা ১ কাপ। ঘি ১ কাপ। গুঁড়াদুধ আধা কাপ। এলাচগুঁড়া সিকি চা-চামচ। দারুচিনিগুঁড়া সিকি চা–চামচ। কিসমিস ১ টেবিল-চামচ। চিনি তিন কাপ।
পদ্ধতি: গাজরের খোসা ফেলে কুচি করে নিতে হবে। চুলায় কড়াইয়ে ঘি আর নারকেল বাটা দিয়ে ভাজতে থাকুন। তারপর এতে কুচানো গাজর, চিনি, এলাচ, দুধ, দারুচিনি দিয়ে নাড়তে থাকুন। নাড়তে নাড়তে কড়াই থেকে হালুয়া যখন ছাড়া ছাড়া হয়ে আসবে তখন চুলা থেকে নামিয়ে বাদাম, কিসমিস, নারিকেল কুড়ানো দিয়ে মাখিয়ে গোল গোল করে লাড্ডু আকার করে পরিবেশন করুন।


রেসিপি দিয়েছেন রনি রহমান।
উপকরণ: গাজর কুড়ানো ১ কাপ। দুধ ৩ কাপ। বাটার ৩ টেবিল-চামচ। চিনি ১ কাপ। জাফরান এক চিমটি।
পদ্ধতি: পাতিলে বাটার আর দুধ দিয়ে ভালোভাবে জাল দিন। একটু ঘন হলে এরমধ্যে কুড়ানো গাজর দিয়ে ঘন ঘন নাড়তে থাকুন। খেয়াল রাখুন যেন সর না পড়ে।

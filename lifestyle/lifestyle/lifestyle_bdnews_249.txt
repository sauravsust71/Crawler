নারায়ণগঞ্জের রূপগঞ্জ উপজেলা স্বাস্থ্য-কমপ্লেক্সের মেডিসিন বিভাগের পরামর্শদাতা ডা. কামরুল হাসান (বিসিএস স্বাস্থ্য) বলেন, “রক্তে অণুচক্রিকার সংখ্যা স্বাভাবিকের তুলনায় কমে যাওয়াকে ডাক্তারি ভাষায় বলা হয় ‘থ্রম্বোসাইটোপেনিয়া’। অত্যন্ত জরুরি এই কণিকার আয়ু পাঁচ থেকে নয় দিন পর্যন্ত হয়ে থাকে।”
কমে যাওয়ার কারণ: অণুচক্রিকা কমে যাওয়ার কারণ সম্পর্কে ডা. কামরুল হাসান জানান, “প্রধান কারণ দুটি হল হয়ত অণুচক্রিকা ধ্বংস হয়ে যাচ্ছে, নয়ত পর্যাপ্ত পরিমাণে তৈরি হচ্ছে না।”
এর পেছনে অন্যতম কারণগুলো হতে পারে-
- ‘অ্যানিমিয়া’ বা রক্তে হিমোগ্লোবিন ও লোহিত রক্তকণিকা কমে যাওয়া, ভাইরাস সংক্রমণ, লিউকেমিয়া, কেমোথেরাপি, অতিরিক্ত মদ্যপান এবং ভিটামিন-বি টুয়েলভ’য়ের অভাবে অণুচক্রিকা তৈরি হওয়া কমে যেতে পারে। 
- তীব্র মাত্রার ক্যান্সার বা পিত্তথলির বিভিন্ন মারাত্বক রোগের কারণে।
- ইডিওপ্যাথিক থ্রম্বোসাইটোপেনিক পারপোরা (আইটিপি)’, ‘থ্রম্বোটিক থ্রম্বোসাইটোপেনিক পারপোরা (টিটিপি)’, রক্তে ব্যাকটেরিয়াজনীত প্রদাহ, ওষুধের প্রতিক্রিয়া এবং রোগ প্রতিরোধ ক্ষমতাজনীত রোগবালাইয়ের কারণে অণুচক্রিকা ভেঙে যেতে পারে।”
উপসর্গ: এই চিকিৎসক বলেন, “অবসাদ, শারীরিক দূর্বলতা, ক্ষতস্থান থেকে দীর্ঘসময় রক্তক্ষরণ, ত্বকে র‌্যাশ, পায়খানা কিংবা প্রসাবের সঙ্গে রক্ত যাওয়া ইত্যাদি অণুচক্রিকার অভাবজনীত লক্ষণ।”
জীবনযাত্রা ও খাদ্যাভ্যাসে পরিবর্তন আনার মাধ্যমে অণুচক্রিকার পরিমাণ স্বাভাবিক অবস্থায় ধরে রাখা সম্ভব। এই বিষয়ে স্বাস্থ্যবিষয়ক একটি ওয়েবসাইটের একটি প্রতিবেদন অবলম্বনে উপকারী কয়েকটি খাবার সম্পর্কে জানানো হল।
পেঁপে ও এর পাতা: ২০০৯ সালে মালয়শিয়ার ‘এশিয়ান ইনস্টিটিউট অফ সাইন্স অ্যান্ড টেকনোলজি’র করা গবেষণা অনুযায়ী, পেঁপে ও এর পাতা অণুচক্রিকার পরিমাণ বাড়াতে সাহায্য করে। এজন্য প্রতিদিন পাকাপেঁপে কিংবা পেঁপে পাতার শরবত পান করা যেতে পারে। অথবা পেঁপের শরবত বানিয়ে তাতে লেবুর রস মিশিয়েও পান করতে পারেন।
কুমড়া ও এর বীজ: শরীরে আমিষ তৈরি করতে কুমড়ায় থাকা পুষ্টি উপদানগুলো কার্যকরী। এছাড়া এতে থাকে ভিটামিন-এ, যা শরীরে অণুচক্রিকা তৈরির প্রক্রিয়ায় সহায়ক।
লেবুর সরবত: লেবু থেকে মেলে ভিটামিন-সি যা অণুচক্রিকার সংখ্যা বাড়ায়। পাশাপাশি এটি রোগ প্রাতরোধ ক্ষমতাও বাড়ায়। ফলে মুক্তমৌলজনীত ক্ষতির হাত থেকে রক্ষা পায় অণুচক্রিকা।
আমলকী: এতেও আছে ভিটামিন-সি, তাই লেবুর সব গুণাগুণ মেলে। বাড়তি উপকার হল, আমলকীতে থাকে অ্যান্টি-অক্সিডেন্ট যা অণুচক্রিকা কমে যাওয়ার জন্য দায়ী বিভিন্ন রোগ দূরে রাখে।
বিট: মুক্তমৌলজনীত ক্ষতি থেকে অণুচক্রিকাকে রক্ষা করতে বিট বেশ উপকারী। পাশাপাশি বাড়ায় এর পরিমাণ। তাই এক গ্লাস বিটের শরবত খাওয়ার অভ্যাস অণুচক্রিকার অভাব কমাতে সাহায্য করবে অনেকটাই।
অ্যালোভেরার শরবত: রক্ত বিশুদ্ধ করতে সহায়ক অ্যালোভেরা। রক্তে সংক্রমণ দূর করতেও এটি উপকারী। উভয়েরই ফলাফল রক্তে অণুচক্রিকার সংখ্যা বৃদ্ধি।

গর্ভবতী ও সন্তানকে স্তন্যদানকালেও মায়েরা এই পরিস্থিতিতে পড়তে পারেন, যার পেছনে কারণ হতে পারে পানিশূন্যতা এবং শারীরিক ও হরমোনজনীত পরিবর্তন।
স্বাস্থবিষয়ক একটি ওয়েবসাইটে শুষ্ক মুখ গহ্বরের অস্বস্তি কাটানোর প্রচলিত ঘরোয়া উপায় নিয়ে প্রকাশিত প্রতিবেদন অবলম্বনে কয়েকটি পন্থা এখানে দেওয়া হল।
তরল পান: পানিশূন্যতাই যেহেতু এর মূল কারণ, তাই তরল পানের পরিমাণ বাড়ানো এক্ষেত্রে আবশ্যক। প্রতিদিন আট থেকে ১০ গ্লাস পানি পান করার অভ্যাস গড়তে হবে। শরীরের পর্যাপ্ত পানি থাকলে তা লালারস তৈরিতে সাহায্য করবে, ফলে মুখের ভেতর থাকবে আর্দ্র। পানির পাশাপাশি ভেষজ চা, ডাবের পানি, সুপ ইত্যাদিও উপকারী।
অয়েল পুলিং


নারিকেল, সরিষা, তিল, সূর্যমুখী ইত্যাদির তেল ব্যবহার করতে পারেন এই কাজে।
কুলিকুচি শেষে কুসুম গরম পানি দিয়ে কুলি করে ব্রাশ করে নিতে হবে। এতে মুখের ভেতরটা থাকবে আর্দ্র, তবে তেল যাতে গিলে না ফেলেন সেদিকে নজর রাখতে হবে।
অ্যালোভেরা


রাতে ঘুমানোর আগে সদ্য তৈরি অ্যালোভেরার নির্যাস কটনবাডের সাহায্যে মুখের ভেতরে মাখিয়ে ১৫ থেকে ২০ মিনিট অপেক্ষা করতে হবে। পরে কুসুম গরম পানি দিয়ে ধুয়ে ফেলতে হবে। 
জিরা


প্রতিদিন দুতিনবার আধা চা-চামচ জিরা চিবাতে পারেন। আবার একমুঠ পরিমাণ জিরা একগ্লাস পানিতে সেদ্ধ করে সারাদিন ওই পানিতে চুমুক দিতে পারেন।
লেবু


এক গ্লাস পানিতে অর্ধেকটা লেবু চিপে তাতে কয়েক ফোঁটা মধু মিশিয়ে নিতে হবে। এই পানীয় সারাদিন ধরে পান করতে পারেন। আবার এক টুকরা লেবুতে এক চিমটি লবণ ছিটিয়ে চুষে খেতে পারেন দিনে তিনবার।
এলাচ


ঠাণ্ডা হলে সারাদিন এই পানি চুমুকে চুমুকে পান করতে হবে। চা তৈরিতেও এলাচ যোগ করতে পারেন।
আদা


আর আদা চা তো আছেই।
আরও পড়ুন
মুখ জীবাণু মুক্ত রাখতে
যেভাবে মুখের যত্ন নেবেন  

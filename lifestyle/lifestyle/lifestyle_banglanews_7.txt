ছুটির দিনগুলোকে আরও সুন্দর করতে চাই স্পেশাল খাবারের আইটেম। জেনে নিন খুব সহজে কীভাবে ঘরেই তৈরি করবেন চিকেন গ্রিল: 
উপকরণ মুরগির মাংস-চার পিস করে কাটা, টক দই আধা কাপ, আদা বাটা এক টেবিল চামচ, রসুন বাটা এক চা চামচ, লাল মরিচের গুঁড়া দুই চা চামচ, ভিনেগার দুই টেবিল চামচ, লবণ স্বাদমত, সরিষার তেল আধা কাপ, মধু এক টেবিল চামচ।
প্রস্তুত প্রণালী মুরগির টুকরোগুলো ভালোভাবে ধুয়ে টক দইয়ের সাথে, আদা, রসুন, মরিচের গুঁড়া, ভিনেগার, লবণ ও চিনি একসাথে মিশিয়ে ১৫ মিনিট ম্যারিনেট করুন। একটি ফ্রাই প্যানে ম্যারিনেট করা টুকরোগুলো ভালো করে ভেজে নিন। এরপর প্রতিটি টকুরা চুলার আগুনে এক মিনিট পুড়িয়ে নিন। 

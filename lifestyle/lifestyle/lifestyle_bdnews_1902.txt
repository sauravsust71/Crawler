শুষ্ক মাথার ত্বক, মাথার ত্বকে ফাঙ্গাস ও ব্যাকটেরিয়ার কারণে খুশকির সৃষ্টি হয় এবং চুলকায়। এই সমস্যা প্রাকৃতিক উপাদান দিয়ে নির্মুল করা যায়। আর এই বিষয়ে একটি প্রতিবেদন প্রকাশিত হয় রূপচর্চাবিষয়ক একটি ওয়েবসাইটে।
খুশকির কারণ
মাথার ত্বক থেকে সাদা দানাদার শুষ্ক বস্তুর উপস্থিতিই খুশকি। যদিও এটি সীমিত পরিমাণে খুব স্বাভাবিক। তবে মাত্রাতিরিক্ত উপস্থিতি ভীষণ অস্বস্তিকর। অনেকেই শুষ্ক ত্বক, তৈলাক্ত ত্বক, খাদ্যাভ্যাস, পরিষ্কার পরিচ্ছন্নতা এবং হতাশাকে খুশকির জন্য দায়ী করেন।
কিন্তু বিজ্ঞানীদের মতে, খুশকির মূল কারণ ফাঙ্গাসের সংক্রমণ। তবে সুখবর হচ্ছে প্রাকৃতিকভাবেই খুশকি নিয়ন্ত্রণ করা যায়। 
গ্রিন টি'য়ের সাহায্যে
প্রয়োজন: গ্রিন টি, পিপারমিন্ট এসেন্সিয়াল তেল, সাদা ভিনিগার।
পদ্ধতি: এক কাপ ফুটন্ত গ্রিন টিয়ের মধ্যে দুতিন ফোঁটা পিপারমিনট এসেন্সিয়াল তেল মেশান। সঙ্গে এক টেবিল-চামচ সাদা ভিনিগার মিশিয়ে ঠাণ্ডা করুন।
পানি দিয়ে চুল ভিজিয়ে গ্রিন টিয়ের মিশ্রণটি মাথায় মাথায় লাগান
পাঁচ মিনিট ধরে মাথার ত্বক মালিশ করুন। এরপর সালফেটবিহীন হালকা কোনো শ্যাম্পু দিয়ে চুল পরিষ্কার করে নিন ও কন্ডিশনার ব্যবহার করুন।
সময়: গোসল করার আগ মুহূর্তে এটা ব্যবহার করুন।
যেভাবে গ্রিনটি কাজ করে: গ্রিনটি ও পিপারমিন্ট এসেন্সিয়াল তেলে আছে অ্যান্টিঅক্সিডেন্ট এবং অ্যান্টিমাইক্রোবিয়াল উপাদান যা মাথার ত্বক সুস্থ রাখে ও চুল করে মসৃণ।
নিমপাতা দিয়ে প্রতিষেধক
গরম পানিতে দুই মুঠ নিমপাতা সারারাত ভিজিয়ে রাখুন। পরদিন সকালে নিমপাতা ভেজানো পানি দিয়ে মাথার চুল ধুয়ে ফেলুন। চাইলে নিমপাতা বেটে মাথায় লাগাতে পারেন।
নিমপাতা বাটা মাথায় লাগিয়ে এক ঘণ্টা অপেক্ষা করুন তারপর ধুয়ে ফেলুন।
সময়: সকালে গোসলের আগে এটা ব্যবহার করতে পারেন। তবে যদি সম্ভব হয় তবে সারা রাত মাথায় লাগিয়ে রাখুন ও সকালে ধুয়ে ফেলুন।
যেভাবে কাজ করে: নিমপাতা কেবল চুলকানি দূর করে না। পাশাপাশি এটা খুশকি সৃষ্টিকারী ফাঙ্গাসকেও ধ্বংস করে।
শ্যাম্পু করা
এটা কোনো ঘরোয়া প্রতিকার নয়। তবে এর মাধ্যমে আপনি মাথার ত্বককে খুশকি মুক্ত রাখতে পারবেন। শ্যাম্পু করার পর যদি ভালো ভাবে চুল না ধোয়া হয় তবে মাথার ত্বকে মৃত কোষ ও তেল জন্মে যার ফলে খুশকি সৃষ্টি হয়। তাই যদি আপনি হালকা শ্যাম্পু ব্যবহার করেন ও পরে কন্ডিশনার লাগান তাহলে খেয়াল রাখবেন যেন তা কোনোভাবেই মাথার ত্বকে না লাগে।  
নারিকেল তেল দিয়ে
প্রয়োজন- নারিকেল তেল, খাঁটি টি ট্রি তেল।
পদ্ধতি: পাঁচ টেবিল-চামচ নারিকেল তেলে পাঁচ থেকে ১০ ফোঁটা খাঁটি টি ট্রি তেল মেশান।
নিয়মিত মাথায় তেল মালিশ করার মতো করে এটি মালিশ করুন। চুলের আগা পর্যন্ত এই তেল মালিশ করার প্রয়োজন নেই।
যদি আপনার মাথার জন্য আরও বেশি তেলের প্রয়োজন হয় তবে খেয়াল রাখবেন যেন এদের অনুপাত ঠিক থাকে।
সময়: রাতে এই তেল মালিশ করুন এবং সকালে তা ধুয়ে ফেলুন। যদি সময় স্বল্পতা থাকে তাহলে গোসলের আধ ঘণ্টা আগে এই তেল ব্যবহার করতে পারেন।
যেভাবে কাজ করে- নারিকেল তেলে আছে অ্যান্টি-ফাঙ্গাল উপাদান যা ফাঙ্গাস ধ্বংস করতে ও খুশকি প্রতিরোধ করতে সাহায্য করে। টি ট্রি তেল চুলকে উজ্জ্বল করে।
লেবুর রস দিয়ে
প্রয়োজন: লেবুর রস ও পানি।
করণীয়: দুই টেবিল-চামচ তাজা লেবুর রস নিয়ে তা মাথার ত্বকে মালিশ করুন ও এক মিনিট অপেক্ষা করুন।
এরপর এক চা-চামচ লেবুর রস এক কাপ পানিতে মিশিয়ে তা দিয়ে চুল ভিজিয়ে নিন।
খুশকি চলে না যাওয়া পর্যন্ত প্রতিদিন এই পদ্ধতি অনুসরণ করুন।
সময়: ভালো ফলাফলের জন্য প্রতিদিন গোসল করার আগে এই পদ্ধতি অনুসরণ করুন।
যেভাবে কাজ করে  
তাজা লেবুর রসে থাকে অ্যাসিড যা খুশকি সৃষ্টিকারী ফাঙ্গাস দমনে সাহায্য করে। এটা সম্পূর্ণ প্রাকৃতিক হওয়ায় এর কোনো পার্শ্বপ্রতিক্রিয়া নেই। তাছাড়া এর সুগন্ধ আপনাকে সারাদিন সতেজ রাখবে।
টক দইয়ের হেয়ার মাস্ক
প্রয়োজন: টক দই ও হালকা শ্যাম্পু
পদ্ধতি: সামান্য টক দই নিয়ে তা গাজানো বা ফার্মেন্টেশনের জন্য দুএকদিন খুলে রাখুন। এরপর মাথার ত্বকে লাগিয়ে এক ঘন্টা অপেক্ষা করুন। এরপর মৃদু কোনো শ্যাম্পু দিয়ে চুল ধুয়ে নিন।
সময়: গোসল করার এক ঘন্টা আগে এটা ব্যবহার করুন।

বাটি থেকে যখন এক চামচ পাস্তা উঠিয়ে আনবেন তখন আঠার মতো চিজের আস্তর ছিঁড়ে ছিঁড়ে আসবে। পাস্তায় উদারপূর্তি করে চায়ের লোভ জাগলে রাস্তা দিয়ে হাঁটলেই, ঠেলাগাড়িতে পাওয়া যাবে বিভিন্ন স্বাদের চা বা কফি। গরমে শীতল হতে চাইলে রয়েছে বিভিন্ন ধরনের ফলের রস।
আর এ সবই মিলবে মিরপুর এক নম্বর থেকে চিড়িয়াখানা রাস্তা ধরে একটু সামনে এগিয়ে হাতের ডানে ঢুকলে।
রাজধানীর মিরপুর এলাকা কাবাবের জন্য পরিচিত পেলেও মিরপুর এক নম্বরের সেকশন এক ও দুইয়ের বিভিন্ন রাস্তায় গড়ে উঠেছে রকমারি খাওয়ার দোকান। পাশাপাশি রাস্তার ধারে পাওয়া যাবে কাচ দিয়ে ঘেরা ‘ঠেলাগাড়ি’ মানে ‘কার্ট’য়ের চা বা ঠাণ্ডা ফলের রস।
ভোজনপ্রিয় এদেশের বাঙালি রসনা তৃপ্তিতে এখন ভিড় করে এই এলাকায়।  
তাই স্থানীয়ভাবে ‘লাভরোড’ হিসেবে পরিচিত রাস্তাটি পরিণত হচ্ছে খিলগাঁও তালতলা কিংবা তিনশ ফিট রাস্তার মতো ফুডস্ট্রিটে। সিংহভাগ রেস্তোরাঁর মালিকই তরুণ এবং মিরপুরেই থাকেন।
একাধিক রেস্তোরাঁর মালিকের সঙ্গে কথা বলে জানা গেছে, এই এলাকার সবচাইতে পুরানো আকর্ষণ হল ইউনুসের চটপটি ফুচকার দোকান ‘দই ফুচকা’।
দই ফুচকা: ৩০ আসনের খাবার রেস্তোরাঁ। বাইরে থেকে দেখলে কাচঘেরা ঘরটি পাড়ার রেস্তোরাঁ মনে হবে।
‘লেমেনেইটিং করা মেন্যুতে অনেকে পদ লেখা থাকলেও পাওয়া যায়- চটপটি, ফুচকা, দইফুচকা, চুড়মুড়, পাপরি-চাট ইত্যাদি।’ প্রশ্ন করায় একগাল হেসে বললেন ম্যানেজার মো. সুমন।
ভেতরের ছোট ঘরে খাবার তৈরি করেন প্রধান কারিগর বুলবুল।


পেরি পাস্তা: আবাসিক ভবনের নিচ তলায় এই রেস্তোরা। ভেভরে ঢুকেই চোখে পড়বে দেয়ালে টাঙানো হলিউড টিভি সিরিয়াল ‘গেইম অফ থ্রোন্স’য়ের কয়েকটি চরিত্রের ছবি। আর মাঝখানে রাখা একটি তলোয়ার। ভেতরের চেয়ার-টেবিল বসানো হয়েছে কিছুটা ‘গাদাগাদি’ করে। দেয়ালে হরেক রকমের ছবি ঝুলছে, জ্বলছে হরেক রকমের বাতি।
রেস্তোরাঁর অন্যতম কর্ণধার নির্মল কুমার দে বলেন, “দুপুর ১২টা থেকে রাত ১০ পর্যন্ত খোলা রাখি আমরা। বসার ব্যবস্থা আছে ৫৫ জনের, আছে ওয়াইফাই।”


নির্মল বলেন, “নাগা মরিচ ব্যবহার করা হয় বোম-বাস্টিং পাস্তা তৈরিতে, ফলে ঝাল হয় বেশ। তবে ক্রেতাদের ঝালের মাত্রা নির্ধারণ করার সুযোগ রয়েছে।”
পাস্তা ছাড়াও এখানে পাওয়া যায় সাব-স্যান্ডউইচ, পেরি পেরি চিকেন, কফি ও বিভিন্ন ধরনের পানীয়। পেরি পেরি চিকেন (১৪৫-১৬০ টাকা)। চকলেট পানিশমেন্ট (৯৫ টাকা) নামক পানীয়ও বেশ জনপ্রিয়। প্রায় প্রতিদিন সন্ধ্যায় প্রচণ্ড ভিড় হয়, তাই এসময় গেলে লাইন ধরার মানসিক প্রস্তুতি নিয়ে যাওয়াই ভালো।
পেরি পাস্তা থেকে সামনে এগিয়ে গেলে রাস্তার দুধারে পাবেন আবেশ হোটেল, দাওয়াত ও কিং’স বার-বি-কিউ।
দাওয়াত প্রচলিত চায়নিজ রেস্তোরাঁর মতো। কিং’স বার-বি-কিউ আবাসিক ভবনের নিচ তলায় ছোট পরিসরের একটি রেস্তোরাঁ। আরও সামনে এগোলে তিন রাস্তার মোড়ের উপর মিলবে দুটি রেস্তোরাঁ- টং রিপাবলিক ও কফি টাইম।
টং রিপাবলিক: চলতি বছরের শুরুতে অত্যন্ত ছোট পরিসরে গড়ে উঠেছে এই রেস্তোরাঁ। একপাশে রান্নাঘর, অন্যপাশে ১০ জনের বসার ব্যবস্থা করা হয়েছে। তবে প্রয়োজনে বাইরে বসার ব্যবস্থা করা যায় বলে জানালেন মালিকদের একজন, শাহরিয়ার সোহান।
পাওয়া যায় বার্গার, চিকেন ফ্রাই, স্যান্ডউইচ, চাওমিন, চাপ, রাইস বোল ও বিভিন্ন পানীয়। তবে রাইস বোল (১২০-১৩০ টাকা) সব চাইতে বেশি বিক্রি হয়- তথ্য দিলেন সোহান।
তিনি আরও বলেন, “এরপরেই জনপ্রিয়তার তুঙ্গে আছে চিকেন বার্গার। দাম ১শ’ থেকে দেড়শ টাকা। পানীয়গুলোর দাম ২০ টাকা থেকে ৬৫ টাকা। তবে ৫০ টাকার মিন্ট বিক্রি হয় সব চাইতে বেশি। দাম অনুযায়ী পরিমাণ ও স্বাদ দুটোই অবিশ্বাস্য।”


বেশ ছিমছাম একটি রেস্তোরাঁ। প্রধান ফটক আর ভেতরের সজ্জায় চায়নিজ রেস্তোরাঁর ভাব স্পষ্ট। খোলা থাকে সকাল ১১টা থেকে রাত ১১টা পর্যন্ত। ৫৫ জনের বসার জায়গা আছে। ঠাণ্ডা-গরম মিলিয়ে কয়েক রকমের কফি মেলে এখানে। তবে বেশি চলে কোল্ড কফি (৫৫ টাকা) ও তিন লেয়ারের কাপুচিনো (১০০ টাকা)।
অন্যান্য খাবারের মধ্যে আছে সাব-স্যান্ডউইচ (১৪০-২২০ টাকা), নাচোস (১৬০-২১০ টাকা), পাস্তা (১৮০-২২০ টাকা)। এছাড়াও আছে সেট মেন্যু, দাম শুরু ১৬০ টাকা থেকে।
কফি ও আড্ডার ভিড় বেশি হয়- কফি ডি, টি ট্রি রেস্তোরাঁয়। প্রকৃত অর্থেই পাড়ার কফির দোকান এগুলো।
তালিকায় অসংখ্য পদ লেখা থাকলেও বিক্রি হয় কোল্ড কফি-ই, দাম ৪০ টাকা।
আবাসিক ভবনের নিচতলার সামনে দেয়াল ভেঙে দোকানে রূপান্তর করা হয়েছে। উপরের বারান্দায় বাসার কাপড় শুকানোর স্বাভাবিক চিত্র। বসার ব্যবস্থা হল প্লাস্টিকের টুল কিংবা টং দোকানের মতো বেঞ্চ।


ঠিক পাশেই গ্রিন ম্যাঙ্গো, ইন্ডিয়ান স্পাইসি, কিং কফি হাউস ও প্রিঙ্গেলস ফুডস নামে চারটি রেস্তোরাঁ আছে।
দ্য বিনস ক্যাফে: “চলতি বছরের জুন মাসে রেস্তোরাঁয় রূপ নিয়েছে”- বললেন মালিক ইমরান খান।
বসতে পারে ২৪ জন, ওয়াইফাই আছে। দুপুর ১২টা থেকে রাত ১০ পর্যন্ত খোলা থাকে। এখানেও সব চাইতে জনপ্রিয় কোল্ড কফি, দাম ৬০ টাকা। কারণ ছোট কোল্ড কফির দোকান থেকেই যাত্রা শুরু করেছিলেন ইমরান।
তবে এখন পাওয়া যায় কয়েক ধরনের সেট মেন্যু। এদের মধ্যে ৬শ’ টাকার ‘ফ্যামিলি ডিল’ অন্যতম। চারজনের হিসেবে এতে থাকে ফ্রাইড রাইস, সবজি, চারটি পপ চিকেন ও আটটি মাসালা চিকেন। ১শ’ থেকে দেড়শ টাকায় মিলবে সাব-স্যান্ডউইচ, বার্গার ইত্যাদি।
এছাড়াও পিংক ক্যাট, এগ্রি কিচেন, ক্যাফে খাওয়াসহ রয়েছে আরও অনেক রেস্তোরাঁ।


সনি সিনেমা হলের দিকে আছে কেএফসি ও পিৎজা ইন।

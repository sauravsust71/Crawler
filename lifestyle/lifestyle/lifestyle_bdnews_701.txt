রেসিপিগুলো দিয়েছেন বীথি জগলুল।
আস্ত ইলিশ


উপকরণ: ইলিশ মাছ ১টি। হলুদগুঁড়া ১ চা-চামচ। মরিচগুঁড়া ২ চা-চামচ। জিরাগুঁড়া ১ চা-চামচ। পেঁয়াজকুচি ১ কাপ। কাঁচামরিচ ৮,১০টি। তেল ১/৪ কাপ। লবণ স্বাদ মতো।
পদ্ধতি: ইলিশ মাছ আস্ত অবস্থায় ভেতরে ও বাইরে পরিষ্কার করে নিন। মাছের সঙ্গে সব উপকরণ মাখিয়ে সারারাত অথবা কমপক্ষে দুই ঘণ্টা মেরিনেশনের জন্য সাধারণ তাপমাত্রার ফ্রিজে ঢেকে রাখুন।
এবার প্রেশার কুকারে মাছ বসিয়ে এমনভাবে পানি দিন যেন মাছ ডুবে থাকে।
চুলায় পুরো আঁচে আধা ঘণ্টা রান্না করে পরের আধা ঘণ্টা একদম মৃদু আঁচে রান্না করুন।
এক ঘণ্টা পর খুব সাবধানে মাছ বের করে একটি ওভেন প্রুফ পরিবেশন পাত্রে অথবা ওভেনের ট্রেতে রাখুন। আস্ত মাছ যদি প্রেশার কুকারে না আঁটে তাহলে মাঝ বরাবর দুভাগ করে নিতে পারেন।
মাছ তুলে ঝোল কিছুটা ঘন করে রান্না করে মাছের ওপর দিয়ে দিন।
এবার ২০০ ডিগ্রি সে. তাপমাত্রায় ওভেন প্রি-হিট করে ১৫ থেকে ২০ মিনিট বেইক করে নিন।
এভাবে রান্না করলে মাছের কাঁটা একদম নরম হয়ে যাবে। খাওয়ার সময় আর বেছে খেতে হবে না ।
মনে রাখুন: প্রেশার কুকারে যে কোনো মাছ এভাবে রান্না করা যায়। বিশেষ করে যারা কাঁটা বাছার ভয়ে মাছ খেতে চান না, তারা এভাবে রান্না করে দেখতে পারেন।
ইলিশের পানি খোলা


পদ্ধতি: যে হাঁড়িতে রান্না করবেন, সেটাতে পেঁয়াজ, কাঁচামরিচ, তেল ও লবণ নিয়ে প্রথমে হাত দিয়ে চটকিয়ে নিন।
এবার পেঁয়াজের উপরে মাছ বিছিয়ে এমনভাবে পানি দিন যেন মাছ ডুবে থাকে।
হাঁড়িটি ঢাকনা দিয়ে ঢেকে চুলায় বসিয়ে আঁচ বাড়িয়ে দিন। পানি ফুটে উঠলে আঁচ মৃদু থেকে মাঝারি করে ১৫ থেকে ২০ মিনিট জ্বাল করুন।
মাঝখানে একবার মাছগুলো উল্টিয়ে দেবেন। আপনার পছন্দ মতো ঝোল ঘন হলে নামিয়ে ফেলুন।
সরিষা ইলিশ


পদ্ধতি: সব বাটা ও গুঁড়া মসলা একসঙ্গে মিশিয়ে রাখুন। প্যানে তেল গরম করে কালোজিরা ফোঁড়ন দিয়ে মসলা কষিয়ে নিন।
মসলা ভালোভাবে কষানো হলে ঝোলের জন্যে পানি দিন। পানি ফুটে উঠলে মাছ দিয়ে মাঝারি আঁচে রান্না করুন। কিছুক্ষণ পর মাছ উল্টিয়ে ঢেকে দিন।

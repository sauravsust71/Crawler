কর্মক্ষেত্রে কলিগদের সাথে বন্ধুত্ব কাজের পরিবেশকে সহজ-সাবলীল করে। কাজের প্রতি আগ্রহ এবং কাজের গতি বাড়িয়ে তোলে। ভাল সম্পর্ক থেকে অফিসে বন্ধুত্ব গড়ে উঠতেই পারে। কিন্তু অফিসে অতিরিক্ত বন্ধুর সংখ্যা অধিকাংশ ক্ষেত্রে ভাল ফলাফল বয়ে নাও আনতে পারে।
কর্তৃপক্ষের নেতিবাচক দৃষ্টিভঙ্গি: বন্ধুত্বে গল্পগুজব আর আড্ডা হওয়াটাই স্বাভাবিক। অফিসে বন্ধু বেশি হলে গল্পও বেশি হবে এবং কর্তৃপক্ষ এটিকে কাজের ব্যাঘাত বলেই ধরে নেবে। তাই আপনার অতিরিক্ত বন্ধুর সংখ্যাকে অফিসের ঊর্ধ্বতন বাঁকা নজরেই দেখবেন। 
ব্যক্তিগত সাফল্যের পথে বাঁধা: অতিরিক্ত বন্ধু প্রায়শই কাজে ফাঁকির কারণ হয় যা আপনার পারফরমেন্স গ্রাফে প্রভাব ফেলবে। আপনার কাজের মূল্যায়ন বন্ধুর মাধ্যমে হলেও তাতে সঠিক তথ্য না আসাটাই স্বাভাবিক।
অপেশাদারি আচরণ: অফিসে অধিকাংশ কলিগের সাথে বন্ধুসুলভ সম্পর্ক কোম্পানির চেইন অব কমান্ড, প্রটোকলের লঙ্ঘন করে থাকে। এই ধরনের অপেশাদারি আচরণ আপনার ক্যারিয়ারের ক্ষতিসাধন করবে।
ব্যক্তিগত জীবনের জন্য ক্ষতিকারক: বন্ধুসুলভ কলিগের সাথে স্বাভাবিকভাবেই ব্যক্তিগত খুঁটিনাটি শেয়ার করলেন, কিন্তু এই বন্ধুত্ব যদি কখনও তিক্ততায় রূপ নেয় তখন এর ফলাফল কি হতে পারে তা আগেই ভেবে নেয়া উচিত।
এতসব নেতিবাচক দিকের কথা চিন্তা করে কর্মক্ষেত্রে বন্ধুত্ব গড়ে তোলার ব্যাপারে বিমুখ হয়ে যেতে পারেন অনেকেই। তবে কর্মক্ষেত্রে যথেষ্ট বন্ধু থাকার অনেক ইতিবাচক দিকও রয়েছে। অফিসে পর্যাপ্ত বন্ধু থাকলে তা কাজের একঘেয়েমি দূর করে মনকে প্রফুল্ল রাখে। এছাড়াও চাকরিসংক্রান্ত যেকোন সমস্যা সহজে শেয়ার করা আর সমাধান পাওয়াও যায় সহজে। ঊর্ধ্বতন কারো সাথে বন্ধুত্ব আপনার কাজের সঠিক মূল্যায়ন এনে দিয়ে আপনার পদোন্নতির পথকেও সুগম করে।
কাজেই দেখা যাচ্ছে কর্মক্ষেত্রে বন্ধুত্বের ভাল দিকও কম নয়। কিছু সহজ উপায়ে কর্মক্ষেত্রে অতিরিক্ত বন্ধুর বিষয়টিকে নিজের সফলতার সিঁড়ি হিসেবে ব্যবহার করতে পারবেন।
- কর্মক্ষেত্রে বন্ধুদের সাথে সম্পর্কের/ব্যবহারের সীমানা নির্ধারণ করুন। অফিস চলাকালীন সময়ে একে অপরের সাথে কলিগের মতই আচরণ করবেন।
- কাজের সময় আড্ডায় মেতে উঠবেন না বরং নিজের কাজ নিষ্ঠার সাথে করুন।
- ‘পার্সোনাল ব্র্যান্ডিং ব্লগ’-এর একটি গবেষণা আর্টিকেলে অফিসে কোন বন্ধু কলিগের সাথে টুকটাক ঝামেলাও তাৎক্ষণিকভাবে মিটিয়ে ফেলার প্রতি জোর দেয়া হয়েছে, সব কিছুকে ব্যক্তিগতভাবে না নেয়াই ভাল।
- নিজেদের মধ্যে টিমওয়ার্ক আর প্রতিযোগিতা বজায় রেখে কোম্পানিকে সাফল্য এনে দিন। এতে করে আপনাদের বন্ধুত্বকে কোম্পানি নিজের সাফল্যের হাতিয়ার বলে ভাবতে বাধ্য হবে।
- কর্মক্ষেত্রে বন্ধুত্বের ব্যাপারে কোম্পানির কোন পলিসি আছে কিনা তা খুঁজে বের করে সেটার নিয়মকানুন অনুসরণ করলে এ বিষয়টি অনেক সহজেই নিজের আয়ত্তে রাখা যায়।
- সর্বোপরি পেশাদারি মনোভাব বজায় রাখুন ও ইতিবাচক থাকুন।
কর্মক্ষেত্রে সুস্থ-সুন্দর বন্ধুত্ব কাজের গতি বাড়িয়ে উন্নতির পথ প্রসারিত করে। তবে বন্ধুর সংখ্যা আর ব্যবহারের ধরন হতে হবে কর্মক্ষেত্রের নিয়মমাফিক। কর্মক্ষেত্রের বন্ধুত্বকে দায়বদ্ধতা নয় বরং উন্নতির হাতিয়ারে পরিণত করতে পারলেই তা আপনার কর্মজীবনকে করবে উপভোগ্য ও সাফল্যমণ্ডিত। - তানভীর জাহান।
ইত্তেফাক/এসএস
